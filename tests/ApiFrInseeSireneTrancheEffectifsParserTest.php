<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneTrancheEffectifs;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneTrancheEffectifsParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * InseeSireneTrancheEffectifsParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneTrancheEffectifsParser
 *
 * @internal
 *
 * @small
 */
class ApiFrInseeSireneTrancheEffectifsParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeSireneTrancheEffectifsParser
	 */
	protected ApiFrInseeSireneTrancheEffectifsParser $_object;
	
	public function testParseSuccess() : void
	{
		$expected = new ApiFrInseeSireneTrancheEffectifs(3, '01', 'Entre 1 et 2 personnes', 1, 2);
		
		$this->assertEquals($expected, $this->_object->parse('01'));
	}
	
	public function testParseFailed() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('XZY');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeSireneTrancheEffectifsParser();
	}
	
}
