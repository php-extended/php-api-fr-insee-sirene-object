<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneCategorieEntreprise;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneCategorieEntrepriseParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * InseeSireneCategorieEntrepriseParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneCategorieEntrepriseParser
 *
 * @internal
 *
 * @small
 */
class ApiFrInseeSireneCategorieEntrepriseParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeSireneCategorieEntrepriseParser
	 */
	protected ApiFrInseeSireneCategorieEntrepriseParser $_object;
	
	public function testParseSuccess() : void
	{
		$expected = new ApiFrInseeSireneCategorieEntreprise(2, 'ETI', 'Entreprise de Taille Intermédiaire');
		
		$this->assertEquals($expected, $this->_object->parse('ETI'));
	}
	
	public function testParseFailed() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('XYZ');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeSireneCategorieEntrepriseParser();
	}
	
}
