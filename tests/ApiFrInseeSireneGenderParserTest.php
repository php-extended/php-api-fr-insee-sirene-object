<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneGender;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneGenderParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * InseeSireneGenderParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneGenderParser
 *
 * @internal
 *
 * @small
 */
class ApiFrInseeSireneGenderParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeSireneGenderParser
	 */
	protected ApiFrInseeSireneGenderParser $_object;
	
	public function testParseSuccess() : void
	{
		$expected = new ApiFrInseeSireneGender(2, 'F', 'Féminin');
		
		$this->assertEquals($expected, $this->_object->parse('F'));
	}
	
	public function testParseFailed() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('XYZ');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeSireneGenderParser();
	}
	
}
