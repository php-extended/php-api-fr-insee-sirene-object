<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene\Test;

use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneTrancheEffectifs;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeSireneTrancheEffectifsTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneTrancheEffectifs
 * @internal
 * @small
 */
class ApiFrInseeSireneTrancheEffectifsTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeSireneTrancheEffectifs
	 */
	protected ApiFrInseeSireneTrancheEffectifs $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals(12, $this->_object->getId());
		$expected = 25;
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetCode() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getCode());
		$expected = 'qsdfghjklm';
		$this->_object->setCode($expected);
		$this->assertEquals($expected, $this->_object->getCode());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getName());
		$expected = 'qsdfghjklm';
		$this->_object->setName($expected);
		$this->assertEquals($expected, $this->_object->getName());
	}
	
	public function testGetEffectifMin() : void
	{
		$this->assertEquals(12, $this->_object->getEffectifMin());
		$expected = 25;
		$this->_object->setEffectifMin($expected);
		$this->assertEquals($expected, $this->_object->getEffectifMin());
	}
	
	public function testGetEffectifMax() : void
	{
		$this->assertEquals(12, $this->_object->getEffectifMax());
		$expected = 25;
		$this->_object->setEffectifMax($expected);
		$this->assertEquals($expected, $this->_object->getEffectifMax());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeSireneTrancheEffectifs(12, 'azertyuiop', 'azertyuiop', 12, 12);
	}
	
}
