<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoie;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtablissement;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtatAdministratif;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneNomenclatureApe;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneTrancheEffectifs;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeSireneEtablissementTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtablissement
 * @internal
 * @small
 */
class ApiFrInseeSireneEtablissementTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeSireneEtablissement
	 */
	protected ApiFrInseeSireneEtablissement $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetSiren() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getSiren());
		$expected = 'qsdfghjklm';
		$this->_object->setSiren($expected);
		$this->assertEquals($expected, $this->_object->getSiren());
	}
	
	public function testGetNic() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getNic());
		$expected = 'qsdfghjklm';
		$this->_object->setNic($expected);
		$this->assertEquals($expected, $this->_object->getNic());
	}
	
	public function testGetSiret() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getSiret());
		$expected = 'qsdfghjklm';
		$this->_object->setSiret($expected);
		$this->assertEquals($expected, $this->_object->getSiret());
	}
	
	public function testHasStatutDiffusionEtablissement() : void
	{
		$this->assertFalse($this->_object->hasStatutDiffusionEtablissement());
		$expected = true;
		$this->_object->setStatutDiffusionEtablissement($expected);
		$this->assertTrue($this->_object->hasStatutDiffusionEtablissement());
	}
	
	public function testGetDateCreationEtablissement() : void
	{
		$this->assertNull($this->_object->getDateCreationEtablissement());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateCreationEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getDateCreationEtablissement());
	}
	
	public function testGetTrancheEffectifsEtablissement() : void
	{
		$this->assertNull($this->_object->getTrancheEffectifsEtablissement());
		$expected = $this->getMockBuilder(ApiFrInseeSireneTrancheEffectifs::class)->disableOriginalConstructor()->getMock();
		$this->_object->setTrancheEffectifsEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getTrancheEffectifsEtablissement());
	}
	
	public function testGetAnneeEffectifsEtablissement() : void
	{
		$this->assertNull($this->_object->getAnneeEffectifsEtablissement());
		$expected = 25;
		$this->_object->setAnneeEffectifsEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getAnneeEffectifsEtablissement());
	}
	
	public function testGetActivitePrincipaleRegistreMetiersEtablissement() : void
	{
		$this->assertNull($this->_object->getActivitePrincipaleRegistreMetiersEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setActivitePrincipaleRegistreMetiersEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getActivitePrincipaleRegistreMetiersEtablissement());
	}
	
	public function testGetDateDernierTraitementEtablissement() : void
	{
		$this->assertNull($this->_object->getDateDernierTraitementEtablissement());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateDernierTraitementEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getDateDernierTraitementEtablissement());
	}
	
	public function testHasEtablissementSiege() : void
	{
		$this->assertFalse($this->_object->hasEtablissementSiege());
		$expected = true;
		$this->_object->setEtablissementSiege($expected);
		$this->assertTrue($this->_object->hasEtablissementSiege());
	}
	
	public function testGetNombrePeriodesEtablissement() : void
	{
		$this->assertNull($this->_object->getNombrePeriodesEtablissement());
		$expected = 25;
		$this->_object->setNombrePeriodesEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getNombrePeriodesEtablissement());
	}
	
	public function testGetComplementAdresseEtablissement() : void
	{
		$this->assertNull($this->_object->getComplementAdresseEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setComplementAdresseEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getComplementAdresseEtablissement());
	}
	
	public function testGetNumeroVoieEtablissement() : void
	{
		$this->assertNull($this->_object->getNumeroVoieEtablissement());
		$expected = 25;
		$this->_object->setNumeroVoieEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getNumeroVoieEtablissement());
	}
	
	public function testGetIndiceRepetitionEtablissement() : void
	{
		$this->assertNull($this->_object->getIndiceRepetitionEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setIndiceRepetitionEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getIndiceRepetitionEtablissement());
	}
	
	public function testGetDernierNumeroVoieEtablissement() : void
	{
		$this->assertNull($this->_object->getDernierNumeroVoieEtablissement());
		$expected = 25;
		$this->_object->setDernierNumeroVoieEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getDernierNumeroVoieEtablissement());
	}
	
	public function testGetIndiceRepetitionDernierNumeroVoieEtablissement() : void
	{
		$this->assertNull($this->_object->getIndiceRepetitionDernierNumeroVoieEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setIndiceRepetitionDernierNumeroVoieEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getIndiceRepetitionDernierNumeroVoieEtablissement());
	}
	
	public function testGetTypeVoieEtablissement() : void
	{
		$this->assertNull($this->_object->getTypeVoieEtablissement());
		$expected = $this->getMockBuilder(ApiFrInseeBanTypeVoie::class)->disableOriginalConstructor()->getMock();
		$this->_object->setTypeVoieEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getTypeVoieEtablissement());
	}
	
	public function testGetLibelleVoieEtablissement() : void
	{
		$this->assertNull($this->_object->getLibelleVoieEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelleVoieEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getLibelleVoieEtablissement());
	}
	
	public function testGetCodePostalEtablissement() : void
	{
		$this->assertNull($this->_object->getCodePostalEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setCodePostalEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getCodePostalEtablissement());
	}
	
	public function testGetLibelleCommuneEtablissement() : void
	{
		$this->assertNull($this->_object->getLibelleCommuneEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelleCommuneEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getLibelleCommuneEtablissement());
	}
	
	public function testGetLibelleCommuneEtrangerEtablissement() : void
	{
		$this->assertNull($this->_object->getLibelleCommuneEtrangerEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelleCommuneEtrangerEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getLibelleCommuneEtrangerEtablissement());
	}
	
	public function testGetDistributionSpecialeEtablissement() : void
	{
		$this->assertNull($this->_object->getDistributionSpecialeEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setDistributionSpecialeEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getDistributionSpecialeEtablissement());
	}
	
	public function testGetCodeCommuneEtablissement() : void
	{
		$this->assertNull($this->_object->getCodeCommuneEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setCodeCommuneEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getCodeCommuneEtablissement());
	}
	
	public function testGetCodeCedexEtablissement() : void
	{
		$this->assertNull($this->_object->getCodeCedexEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setCodeCedexEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getCodeCedexEtablissement());
	}
	
	public function testGetLibelleCedexEtablissement() : void
	{
		$this->assertNull($this->_object->getLibelleCedexEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelleCedexEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getLibelleCedexEtablissement());
	}
	
	public function testGetCodePaysEtrangerEtablissement() : void
	{
		$this->assertNull($this->_object->getCodePaysEtrangerEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setCodePaysEtrangerEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getCodePaysEtrangerEtablissement());
	}
	
	public function testGetLibellePaysEtrangerEtablissement() : void
	{
		$this->assertNull($this->_object->getLibellePaysEtrangerEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setLibellePaysEtrangerEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getLibellePaysEtrangerEtablissement());
	}
	
	public function testGetIdentifiantAdresseEtablissement() : void
	{
		$this->assertNull($this->_object->getIdentifiantAdresseEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setIdentifiantAdresseEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getIdentifiantAdresseEtablissement());
	}
	
	public function testGetCoordonneeLambertAbscisseEtablissement() : void
	{
		$this->assertNull($this->_object->getCoordonneeLambertAbscisseEtablissement());
		$expected = 15.2;
		$this->_object->setCoordonneeLambertAbscisseEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getCoordonneeLambertAbscisseEtablissement());
	}
	
	public function testGetCoordonneeLambertOrdonneeEtablissement() : void
	{
		$this->assertNull($this->_object->getCoordonneeLambertOrdonneeEtablissement());
		$expected = 15.2;
		$this->_object->setCoordonneeLambertOrdonneeEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getCoordonneeLambertOrdonneeEtablissement());
	}
	
	public function testGetComplementAdresse2Etablissement() : void
	{
		$this->assertNull($this->_object->getComplementAdresse2Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setComplementAdresse2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getComplementAdresse2Etablissement());
	}
	
	public function testGetNumeroVoie2Etablissement() : void
	{
		$this->assertNull($this->_object->getNumeroVoie2Etablissement());
		$expected = 25;
		$this->_object->setNumeroVoie2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getNumeroVoie2Etablissement());
	}
	
	public function testGetIndiceRepetition2Etablissement() : void
	{
		$this->assertNull($this->_object->getIndiceRepetition2Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setIndiceRepetition2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getIndiceRepetition2Etablissement());
	}
	
	public function testGetTypeVoie2Etablissement() : void
	{
		$this->assertNull($this->_object->getTypeVoie2Etablissement());
		$expected = $this->getMockBuilder(ApiFrInseeBanTypeVoie::class)->disableOriginalConstructor()->getMock();
		$this->_object->setTypeVoie2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getTypeVoie2Etablissement());
	}
	
	public function testGetLibelleVoie2Etablissement() : void
	{
		$this->assertNull($this->_object->getLibelleVoie2Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelleVoie2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getLibelleVoie2Etablissement());
	}
	
	public function testGetCodePostal2Etablissement() : void
	{
		$this->assertNull($this->_object->getCodePostal2Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setCodePostal2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getCodePostal2Etablissement());
	}
	
	public function testGetLibelleCommune2Etablissement() : void
	{
		$this->assertNull($this->_object->getLibelleCommune2Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelleCommune2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getLibelleCommune2Etablissement());
	}
	
	public function testGetLibelleCommuneEtranger2Etablissement() : void
	{
		$this->assertNull($this->_object->getLibelleCommuneEtranger2Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelleCommuneEtranger2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getLibelleCommuneEtranger2Etablissement());
	}
	
	public function testGetDistributionSpeciale2Etablissement() : void
	{
		$this->assertNull($this->_object->getDistributionSpeciale2Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setDistributionSpeciale2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getDistributionSpeciale2Etablissement());
	}
	
	public function testGetCodeCommune2Etablissement() : void
	{
		$this->assertNull($this->_object->getCodeCommune2Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setCodeCommune2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getCodeCommune2Etablissement());
	}
	
	public function testGetCodeCedex2Etablissement() : void
	{
		$this->assertNull($this->_object->getCodeCedex2Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setCodeCedex2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getCodeCedex2Etablissement());
	}
	
	public function testGetLibelleCedex2Etablissement() : void
	{
		$this->assertNull($this->_object->getLibelleCedex2Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelleCedex2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getLibelleCedex2Etablissement());
	}
	
	public function testGetCodePaysEtranger2Etablissement() : void
	{
		$this->assertNull($this->_object->getCodePaysEtranger2Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setCodePaysEtranger2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getCodePaysEtranger2Etablissement());
	}
	
	public function testGetLibellePaysEtranger2Etablissement() : void
	{
		$this->assertNull($this->_object->getLibellePaysEtranger2Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setLibellePaysEtranger2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getLibellePaysEtranger2Etablissement());
	}
	
	public function testGetDateDebut() : void
	{
		$this->assertNull($this->_object->getDateDebut());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateDebut($expected);
		$this->assertEquals($expected, $this->_object->getDateDebut());
	}
	
	public function testGetEtatAdministratifEtablissement() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrInseeSireneEtatAdministratif::class)->disableOriginalConstructor()->getMock(), $this->_object->getEtatAdministratifEtablissement());
		$expected = $this->getMockBuilder(ApiFrInseeSireneEtatAdministratif::class)->disableOriginalConstructor()->getMock();
		$this->_object->setEtatAdministratifEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getEtatAdministratifEtablissement());
	}
	
	public function testGetEnseigne1Etablissement() : void
	{
		$this->assertNull($this->_object->getEnseigne1Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setEnseigne1Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getEnseigne1Etablissement());
	}
	
	public function testGetEnseigne2Etablissement() : void
	{
		$this->assertNull($this->_object->getEnseigne2Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setEnseigne2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getEnseigne2Etablissement());
	}
	
	public function testGetEnseigne3Etablissement() : void
	{
		$this->assertNull($this->_object->getEnseigne3Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setEnseigne3Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getEnseigne3Etablissement());
	}
	
	public function testGetDenominationUsuelleEtablissement() : void
	{
		$this->assertNull($this->_object->getDenominationUsuelleEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setDenominationUsuelleEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getDenominationUsuelleEtablissement());
	}
	
	public function testGetActivitePrincipaleEtablissement() : void
	{
		$this->assertNull($this->_object->getActivitePrincipaleEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setActivitePrincipaleEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getActivitePrincipaleEtablissement());
	}
	
	public function testGetNomenclatureActivitePrincipaleEtablissement() : void
	{
		$this->assertNull($this->_object->getNomenclatureActivitePrincipaleEtablissement());
		$expected = $this->getMockBuilder(ApiFrInseeSireneNomenclatureApe::class)->disableOriginalConstructor()->getMock();
		$this->_object->setNomenclatureActivitePrincipaleEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getNomenclatureActivitePrincipaleEtablissement());
	}
	
	public function testHasCaractereEmployeurEtablissement() : void
	{
		$this->assertNull($this->_object->hasCaractereEmployeurEtablissement());
		$expected = true;
		$this->_object->setCaractereEmployeurEtablissement($expected);
		$this->assertTrue($this->_object->hasCaractereEmployeurEtablissement());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeSireneEtablissement('azertyuiop', 'azertyuiop', 'azertyuiop', false, false, $this->getMockBuilder(ApiFrInseeSireneEtatAdministratif::class)->disableOriginalConstructor()->getMock());
	}
	
}
