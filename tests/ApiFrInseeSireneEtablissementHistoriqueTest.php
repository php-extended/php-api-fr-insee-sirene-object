<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtablissementHistorique;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtatAdministratif;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneNomenclatureApe;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeSireneEtablissementHistoriqueTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtablissementHistorique
 * @internal
 * @small
 */
class ApiFrInseeSireneEtablissementHistoriqueTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeSireneEtablissementHistorique
	 */
	protected ApiFrInseeSireneEtablissementHistorique $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetSiren() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getSiren());
		$expected = 'qsdfghjklm';
		$this->_object->setSiren($expected);
		$this->assertEquals($expected, $this->_object->getSiren());
	}
	
	public function testGetNic() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getNic());
		$expected = 'qsdfghjklm';
		$this->_object->setNic($expected);
		$this->assertEquals($expected, $this->_object->getNic());
	}
	
	public function testGetSiret() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getSiret());
		$expected = 'qsdfghjklm';
		$this->_object->setSiret($expected);
		$this->assertEquals($expected, $this->_object->getSiret());
	}
	
	public function testGetDateFin() : void
	{
		$this->assertNull($this->_object->getDateFin());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateFin($expected);
		$this->assertEquals($expected, $this->_object->getDateFin());
	}
	
	public function testGetDateDebut() : void
	{
		$this->assertNull($this->_object->getDateDebut());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateDebut($expected);
		$this->assertEquals($expected, $this->_object->getDateDebut());
	}
	
	public function testGetEtatAdministratifEtablissement() : void
	{
		$this->assertNull($this->_object->getEtatAdministratifEtablissement());
		$expected = $this->getMockBuilder(ApiFrInseeSireneEtatAdministratif::class)->disableOriginalConstructor()->getMock();
		$this->_object->setEtatAdministratifEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getEtatAdministratifEtablissement());
	}
	
	public function testHasChangementEtatAdministratifEtablissement() : void
	{
		$this->assertFalse($this->_object->hasChangementEtatAdministratifEtablissement());
		$expected = true;
		$this->_object->setChangementEtatAdministratifEtablissement($expected);
		$this->assertTrue($this->_object->hasChangementEtatAdministratifEtablissement());
	}
	
	public function testGetEnseigne1Etablissement() : void
	{
		$this->assertNull($this->_object->getEnseigne1Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setEnseigne1Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getEnseigne1Etablissement());
	}
	
	public function testGetEnseigne2Etablissement() : void
	{
		$this->assertNull($this->_object->getEnseigne2Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setEnseigne2Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getEnseigne2Etablissement());
	}
	
	public function testGetEnseigne3Etablissement() : void
	{
		$this->assertNull($this->_object->getEnseigne3Etablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setEnseigne3Etablissement($expected);
		$this->assertEquals($expected, $this->_object->getEnseigne3Etablissement());
	}
	
	public function testHasChangementEnseigneEtablissement() : void
	{
		$this->assertFalse($this->_object->hasChangementEnseigneEtablissement());
		$expected = true;
		$this->_object->setChangementEnseigneEtablissement($expected);
		$this->assertTrue($this->_object->hasChangementEnseigneEtablissement());
	}
	
	public function testGetDenominationUsuelleEtablissement() : void
	{
		$this->assertNull($this->_object->getDenominationUsuelleEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setDenominationUsuelleEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getDenominationUsuelleEtablissement());
	}
	
	public function testHasChangementDenominationUsuelleEtablissement() : void
	{
		$this->assertFalse($this->_object->hasChangementDenominationUsuelleEtablissement());
		$expected = true;
		$this->_object->setChangementDenominationUsuelleEtablissement($expected);
		$this->assertTrue($this->_object->hasChangementDenominationUsuelleEtablissement());
	}
	
	public function testGetActivitePrincipaleEtablissement() : void
	{
		$this->assertNull($this->_object->getActivitePrincipaleEtablissement());
		$expected = 'qsdfghjklm';
		$this->_object->setActivitePrincipaleEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getActivitePrincipaleEtablissement());
	}
	
	public function testGetNomenclatureActivitePrincipaleEtablissement() : void
	{
		$this->assertNull($this->_object->getNomenclatureActivitePrincipaleEtablissement());
		$expected = $this->getMockBuilder(ApiFrInseeSireneNomenclatureApe::class)->disableOriginalConstructor()->getMock();
		$this->_object->setNomenclatureActivitePrincipaleEtablissement($expected);
		$this->assertEquals($expected, $this->_object->getNomenclatureActivitePrincipaleEtablissement());
	}
	
	public function testHasChangementActivitePrincipaleEtablissement() : void
	{
		$this->assertFalse($this->_object->hasChangementActivitePrincipaleEtablissement());
		$expected = true;
		$this->_object->setChangementActivitePrincipaleEtablissement($expected);
		$this->assertTrue($this->_object->hasChangementActivitePrincipaleEtablissement());
	}
	
	public function testHasCaractereEmployeurEtablissement() : void
	{
		$this->assertNull($this->_object->hasCaractereEmployeurEtablissement());
		$expected = true;
		$this->_object->setCaractereEmployeurEtablissement($expected);
		$this->assertTrue($this->_object->hasCaractereEmployeurEtablissement());
	}
	
	public function testHasChangementCaractereEmployeurEtablissement() : void
	{
		$this->assertFalse($this->_object->hasChangementCaractereEmployeurEtablissement());
		$expected = true;
		$this->_object->setChangementCaractereEmployeurEtablissement($expected);
		$this->assertTrue($this->_object->hasChangementCaractereEmployeurEtablissement());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeSireneEtablissementHistorique('azertyuiop', 'azertyuiop', 'azertyuiop', false, false, false, false, false);
	}
	
}
