<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneDoublon;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeSireneDoublonTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneDoublon
 * @internal
 * @small
 */
class ApiFrInseeSireneDoublonTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeSireneDoublon
	 */
	protected ApiFrInseeSireneDoublon $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetSiren() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getSiren());
		$expected = 'qsdfghjklm';
		$this->_object->setSiren($expected);
		$this->assertEquals($expected, $this->_object->getSiren());
	}
	
	public function testGetSirenDoublon() : void
	{
		$this->assertNull($this->_object->getSirenDoublon());
		$expected = 'qsdfghjklm';
		$this->_object->setSirenDoublon($expected);
		$this->assertEquals($expected, $this->_object->getSirenDoublon());
	}
	
	public function testGetDateDernierTraitementDoublon() : void
	{
		$this->assertNull($this->_object->getDateDernierTraitementDoublon());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateDernierTraitementDoublon($expected);
		$this->assertEquals($expected, $this->_object->getDateDernierTraitementDoublon());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeSireneDoublon('azertyuiop');
	}
	
}
