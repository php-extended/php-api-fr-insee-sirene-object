<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpoint;
use PhpExtended\HttpClient\ZipClient;
use PhpExtended\HttpMessage\FileStream;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\StringStream;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * InseeSireneEndpointTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiFrInseeSireneEndpointTest extends TestCase
{
	
	/**
	 * Gets a suitable directory to download and uncompress data.
	 *
	 * @return string
	 * @throws RuntimeException
	 */
	protected static function getTempPath() : string
	{
		$base = \is_dir('/media/anastaszor/RUNTIME/') ? '/media/anastaszor/RUNTIME/' : '/tmp/';
		$real = $base.'php-extended__php-api-fr-insee-sirene-object';
		if(!\is_dir($real))
		{
			if(!\mkdir($real))
			{
				throw new RuntimeException('Failed to make temp directory at '.$real);
			}
		}
		
		return $real;
	}
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeSireneEndpoint
	 */
	protected ApiFrInseeSireneEndpoint $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetLatestUploadDate() : void
	{
		$this->assertInstanceOf(DateTimeInterface::class, $this->_object->getLatestUploadDate());
	}
	
	public function testGetLatestStockUniteLegaleIterator() : void
	{
		if(\strpos($this->getTempPath(), '/tmp/') !== false)
		{
			$this->markTestSkipped('Not Enough Disk Space on CI');
			
			return;
		}
		
		$k = 0;
		
		/** @var PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneUniteLegale $uniteLegale */
		foreach($this->_object->getLatestStockUniteLegaleIterator() as $uniteLegale)
		{
			$this->assertNotEmpty($uniteLegale->getSiren());
			$k++;
			
			if(100000 === $k)
			{
				break;
			}
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	public function testGetLatestStockUniteLegaleHistoricIterator() : void
	{
		if(\strpos($this->getTempPath(), '/tmp/') !== false)
		{
			$this->markTestSkipped('Not Enough Disk Space on CI');
			
			return;
		}
		
		$k = 0;
		
		/** @var PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneUniteLegaleHistorique $historic */
		foreach($this->_object->getLatestStockUniteLegaleHistoricIterator() as $historic)
		{
			$this->assertNotEmpty($historic->getSiren());
			$k++;
			
			if(100000 === $k)
			{
				break;
			}
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	public function testGetLatestStockEtablissementIterator() : void
	{
		if(\strpos($this->getTempPath(), '/tmp/') !== false)
		{
			$this->markTestSkipped('Not Enough Disk Space on CI');
			
			return;
		}
		
		$k = 0;
		
		/** @var PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtablissement $etablissement */
		foreach($this->_object->getLatestStockEtablissementIterator() as $etablissement)
		{
			$this->assertNotEmpty($etablissement->getSiren());
			$this->assertNotEmpty($etablissement->getNic());
			$this->assertNotEmpty($etablissement->getSiret());
			$k++;
			
			if(100000 === $k)
			{
				break;
			}
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	public function testGetLatestStockEtablissementHistoricIterator() : void
	{
		if(\strpos($this->getTempPath(), '/tmp/') !== false)
		{
			$this->markTestSkipped('Not Enough Disk Space on CI');
			
			return;
		}
		
		$k = 0;
		
		/** @var PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtablissementHistorique $historic */
		foreach($this->_object->getLatestStockEtablissementHistoricIterator() as $historic)
		{
			$this->assertNotEmpty($historic->getSiren());
			$this->assertNotEmpty($historic->getNic());
			$this->assertNotEmpty($historic->getSiret());
			$k++;
			
			if(100000 === $k)
			{
				break;
			}
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	public function testGetLatestStockSuccessionIterator() : void
	{
		if(\strpos($this->getTempPath(), '/tmp/') !== false)
		{
			$this->markTestSkipped('Not Enough Disk Space on CI');
			
			return;
		}
		
		$k = 0;
		
		/** @var PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneSuccession $succession */
		foreach($this->_object->getLatestStockSuccessionIterator() as $succession)
		{
			$this->assertNotEmpty($succession->getSiretEtablissementPredecesseur());
			$this->assertNotEmpty($succession->getSiretEtablissementSuccesseur());
			$this->assertNotEmpty($succession->getDateDernierTraitementLienSuccession());
			$this->assertNotEmpty($succession->getDateLienSuccession());
			$k++;
			
			if(100000 === $k)
			{
				break;
			}
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	public function testGetCategorieEntrepriseIterator() : void
	{
		$k = 0;
		
		/** @var PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneCategorieEntreprise $categorieEntreprise */
		foreach($this->_object->getCategorieEntrepriseIterator() as $categorieEntreprise)
		{
			$this->assertNotEmpty($categorieEntreprise->getId());
			$this->assertNotEmpty($categorieEntreprise->getCode());
			$this->assertNotEmpty($categorieEntreprise->getName());
			$k++;
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	public function testGetEtatAdministratifIterator() : void
	{
		$k = 0;
		
		/** @var PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtatAdministratif $etatAdministratif */
		foreach($this->_object->getEtatAdministratifIterator() as $etatAdministratif)
		{
			$this->assertNotEmpty($etatAdministratif->getId());
			$this->assertNotEmpty($etatAdministratif->getCode());
			$this->assertNotEmpty($etatAdministratif->getName());
			$k++;
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	public function testGetGenderIterator() : void
	{
		$k = 0;
		
		/** @var PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneGender $gender */
		foreach($this->_object->getGenderIterator() as $gender)
		{
			$this->assertNotEmpty($gender->getId());
			$this->assertNotEmpty($gender->getCode());
			$this->assertNotEmpty($gender->getName());
			$k++;
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	public function testGetNomenclatureApeIterator() : void
	{
		$k = 0;
		
		/** @var PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneNomenclatureApe $nomenclatureApe */
		foreach($this->_object->getNomenclatureApeIterator() as $nomenclatureApe)
		{
			$this->assertNotEmpty($nomenclatureApe->getId());
			$this->assertNotEmpty($nomenclatureApe->getCode());
			$this->assertNotEmpty($nomenclatureApe->getName());
			$k++;
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	public function testGetTrancheEffectifIterator() : void
	{
		$k = 0;
		
		/** @var PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneTrancheEffectifs $trancheEffectif */
		foreach($this->_object->getTrancheEffectifIterator() as $trancheEffectif)
		{
			$this->assertNotEmpty($trancheEffectif->getId());
			$this->assertNotEmpty($trancheEffectif->getCode());
			$this->assertNotEmpty($trancheEffectif->getName());
			$k++;
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				$response = new Response();
				$response = $response->withHeader('X-Request-Uri', $request->getUri()->__toString());
				
				foreach($request->getHeaders() as $name => $value)
				{
					$response = $response->withAddedHeader('X-Request-Header-'.$name, $value);
				}
				
				// echo "\n".$request->getMethod().' '.$request->getUri()->__toString()."\n";
				$headers = [];
				
				$filePath = $request->getHeaderLine('X-Php-Download-File');
				$request = $request->withoutHeader('X-Php-Download-File');
				
				foreach($request->getHeaders() as $headerKey => $headerArr)
				{
					// echo "\t".$headerKey.': '.\implode(', ', $headerArr)."\n";
					$headers[] = $headerKey.': '.\implode(', ', $headerArr);
				}
				
				$context = \stream_context_create(['http' => [
					'method' => $request->getMethod(),
					'header' => \implode("\r\n", $headers)."\r\n",
					'content' => $request->getBody()->__toString(),
				]]);
				
				$http_response_header = [];
				
				if(!empty($filePath))
				{
					\copy($request->getUri()->__toString(), $filePath, $context);
					$stream = new FileStream($filePath);
				}
				else
				{
					$data = (string) \file_get_contents($request->getUri()->__toString(), false, $context);
					$stream = new StringStream($data);
				}
				
				foreach($http_response_header as $header)
				{
					if(\mb_strpos($header, 'HTTP/') !== false)
					{
						continue;
					}
					$dotpos = \mb_strpos($header, ':');
					$name = \mb_substr($header, 0, $dotpos);
					$value = \trim(\mb_substr($header, $dotpos + 1));
					$response = $response->withAddedHeader($name, $value);
				}
				
				return $response->withBody($stream);
			}
			
		};
		
		$this->_object = new ApiFrInseeSireneEndpoint($this->getTempPath(), new ZipClient($client, new StreamFactory()));
	}
	
}
