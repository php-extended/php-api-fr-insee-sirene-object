<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtatAdministratif;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtatAdministratifParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * InseeSireneEtatAdministratifParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtatAdministratifParser
 *
 * @internal
 *
 * @small
 */
class ApiFrInseeSireneEtatAdministratifParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeSireneEtatAdministratifParser
	 */
	protected ApiFrInseeSireneEtatAdministratifParser $_object;
	
	public function testParseSuccess() : void
	{
		$expected = new ApiFrInseeSireneEtatAdministratif(2, 'C', 'Cessée');
		
		$this->assertEquals($expected, $this->_object->parse('C'));
	}
	
	public function testParseFailed() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('XYZ');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeSireneEtatAdministratifParser();
	}
	
}
