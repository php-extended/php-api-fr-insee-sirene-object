<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtatAdministratif;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneNomenclatureApe;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneUniteLegaleHistorique;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeSireneUniteLegaleHistoriqueTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneUniteLegaleHistorique
 * @internal
 * @small
 */
class ApiFrInseeSireneUniteLegaleHistoriqueTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeSireneUniteLegaleHistorique
	 */
	protected ApiFrInseeSireneUniteLegaleHistorique $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetSiren() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getSiren());
		$this->_object->setSiren('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getSiren());
	}
	
	public function testGetDateFin() : void
	{
		$this->assertNull($this->_object->getDateFin());
		$date = new DateTimeImmutable();
		$this->_object->setDateFin($date);
		$this->assertEquals($date, $this->_object->getDateFin());
	}
	
	public function testGetDateDebut() : void
	{
		$this->assertNull($this->_object->getDateDebut());
		$date = new DateTimeImmutable();
		$this->_object->setDateDebut($date);
		$this->assertEquals($date, $this->_object->getDateDebut());
	}
	
	public function testGetEtatAdministratifUniteLegale() : void
	{
		$this->assertNull($this->_object->getEtatAdministratifUniteLegale());
		$this->_object->setEtatAdministratifUniteLegale($this->getMockBuilder(ApiFrInseeSireneEtatAdministratif::class)->disableOriginalConstructor()->getMock());
		$this->assertEquals($this->getMockBuilder(ApiFrInseeSireneEtatAdministratif::class)->disableOriginalConstructor()->getMock(), $this->_object->getEtatAdministratifUniteLegale());
	}
	
	public function testHasChangementEtatAdministratifUniteLegale() : void
	{
		$this->assertFalse($this->_object->hasChangementEtatAdministratifUniteLegale());
		$this->_object->setChangementEtatAdministratifUniteLegale(true);
		$this->assertTrue($this->_object->hasChangementEtatAdministratifUniteLegale());
	}
	
	public function testGetNomUniteLegale() : void
	{
		$this->assertNull($this->_object->getNomUniteLegale());
		$this->_object->setNomUniteLegale('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getNomUniteLegale());
	}
	
	public function testHasChangementNomUniteLegale() : void
	{
		$this->assertFalse($this->_object->hasChangementNomUniteLegale());
		$this->_object->setChangementNomUniteLegale(true);
		$this->assertTrue($this->_object->hasChangementNomUniteLegale());
	}
	
	public function testGetNomUsageUniteLegale() : void
	{
		$this->assertNull($this->_object->getNomUsageUniteLegale());
		$this->_object->setNomUsageUniteLegale('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getNomUsageUniteLegale());
	}
	
	public function testHasChangementNomUsageUniteLegale() : void
	{
		$this->assertFalse($this->_object->hasChangementNomUsageUniteLegale());
		$this->_object->setChangementNomUsageUniteLegale(true);
		$this->assertTrue($this->_object->hasChangementNomUsageUniteLegale());
	}
	
	public function testGetDenominationUniteLegale() : void
	{
		$this->assertNull($this->_object->getDenominationUniteLegale());
		$this->_object->setDenominationUniteLegale('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getDenominationUniteLegale());
	}
	
	public function testHasChangementDenominationUniteLegale() : void
	{
		$this->assertFalse($this->_object->hasChangementDenominationUniteLegale());
		$this->_object->setChangementDenominationUniteLegale(true);
		$this->assertTrue($this->_object->hasChangementDenominationUniteLegale());
	}
	
	public function testGetDenominationUsuelle1UniteLegale() : void
	{
		$this->assertNull($this->_object->getDenominationUsuelle1UniteLegale());
		$this->_object->setDenominationUsuelle1UniteLegale('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getDenominationUsuelle1UniteLegale());
	}
	
	public function testGetDenominationUsuelle2UniteLegale() : void
	{
		$this->assertNull($this->_object->getDenominationUsuelle2UniteLegale());
		$this->_object->setDenominationUsuelle2UniteLegale('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getDenominationUsuelle2UniteLegale());
	}
	
	public function testGetDenominationUsuelle3UniteLegale() : void
	{
		$this->assertNull($this->_object->getDenominationUsuelle3UniteLegale());
		$this->_object->setDenominationUsuelle3UniteLegale('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getDenominationUsuelle3UniteLegale());
	}
	
	public function testHasChangementDenominationUsuelleUniteLegale() : void
	{
		$this->assertFalse($this->_object->hasChangementDenominationUsuelleUniteLegale());
		$this->_object->setChangementDenominationUsuelleUniteLegale(true);
		$this->assertTrue($this->_object->hasChangementDenominationUsuelleUniteLegale());
	}
	
	public function testGetCategorieJuridiqueUniteLegale() : void
	{
		$this->assertNull($this->_object->getCategorieJuridiqueUniteLegale());
		$this->_object->setCategorieJuridiqueUniteLegale('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getCategorieJuridiqueUniteLegale());
	}
	
	public function testHasChangementCategorieJuridiqueUniteLegale() : void
	{
		$this->assertFalse($this->_object->hasChangementCategorieJuridiqueUniteLegale());
		$this->_object->setChangementCategorieJuridiqueUniteLegale(true);
		$this->assertTrue($this->_object->hasChangementCategorieJuridiqueUniteLegale());
	}
	
	public function testGetActivitePrincipaleUniteLegale() : void
	{
		$this->assertNull($this->_object->getActivitePrincipaleUniteLegale());
		$this->_object->setActivitePrincipaleUniteLegale('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getActivitePrincipaleUniteLegale());
	}
	
	public function testGetNomenclatureActivitePrincipaleUniteLegale() : void
	{
		$this->assertNull($this->_object->getNomenclatureActivitePrincipaleUniteLegale());
		$this->_object->setNomenclatureActivitePrincipaleUniteLegale($this->getMockBuilder(ApiFrInseeSireneNomenclatureApe::class)->disableOriginalConstructor()->getMock());
		$this->assertEquals($this->getMockBuilder(ApiFrInseeSireneNomenclatureApe::class)->disableOriginalConstructor()->getMock(), $this->_object->getNomenclatureActivitePrincipaleUniteLegale());
	}
	
	public function testHasChangementActivitePrincipaleUniteLegale() : void
	{
		$this->assertFalse($this->_object->hasChangementActivitePrincipaleUniteLegale());
		$this->_object->setChangementActivitePrincipaleUniteLegale(true);
		$this->assertTrue($this->_object->hasChangementActivitePrincipaleUniteLegale());
	}
	
	public function testGetNicSiegeUniteLegale() : void
	{
		$this->assertNull($this->_object->getNicSiegeUniteLegale());
		$this->_object->setNicSiegeUniteLegale('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getNicSiegeUniteLegale());
	}
	
	public function testHasChangementNicSiegeUniteLegale() : void
	{
		$this->assertFalse($this->_object->hasChangementNicSiegeUniteLegale());
		$this->_object->setChangementNicSiegeUniteLegale(true);
		$this->assertTrue($this->_object->hasChangementNicSiegeUniteLegale());
	}
	
	public function testHasEconomieSocialeSolidaireUniteLegale() : void
	{
		$this->assertNull($this->_object->hasEconomieSocialeSolidaireUniteLegale());
		$this->_object->setEconomieSocialeSolidaireUniteLegale(true);
		$this->assertTrue($this->_object->hasEconomieSocialeSolidaireUniteLegale());
	}
	
	public function testHasChangementEconomieSocialeSolidaireUniteLegale() : void
	{
		$this->assertFalse($this->_object->hasChangementEconomieSocialeSolidaireUniteLegale());
		$this->_object->setChangementEconomieSocialeSolidaireUniteLegale(true);
		$this->assertTrue($this->_object->hasChangementEconomieSocialeSolidaireUniteLegale());
	}
	
	public function testHasCaractereEmployeurUniteLegale() : void
	{
		$this->assertNull($this->_object->hasCaractereEmployeurUniteLegale());
		$this->_object->setCaractereEmployeurUniteLegale(true);
		$this->assertTrue($this->_object->hasCaractereEmployeurUniteLegale());
	}
	
	public function testHasChangementCaractereEmployeurUniteLegale() : void
	{
		$this->assertFalse($this->_object->hasChangementCaractereEmployeurUniteLegale());
		$this->_object->setChangementCaractereEmployeurUniteLegale(true);
		$this->assertTrue($this->_object->hasChangementCaractereEmployeurUniteLegale());
	}
	
	public function testHasSocieteMissionUniteLegale() : void
	{
		$this->assertNull($this->_object->hasSocieteMissionUniteLegale());
		$this->_object->setSocieteMissionUniteLegale(true);
		$this->assertTrue($this->_object->hasSocieteMissionUniteLegale());
	}
	
	public function testHasChangementSocieteMissionUniteLegale() : void
	{
		$this->assertFalse($this->_object->hasChangementSocieteMissionUniteLegale());
		$this->_object->setChangementSocieteMissionUniteLegale(true);
		$this->assertTrue($this->_object->hasChangementSocieteMissionUniteLegale());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeSireneUniteLegaleHistorique('azertyuiop', false, false, false, false, false, false, false, false, false, false, false);
	}
	
}
