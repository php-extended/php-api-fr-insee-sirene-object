<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneCategorieEntreprise;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEtatAdministratif;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneGender;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneNomenclatureApe;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneTrancheEffectifs;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneUniteLegale;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeSireneUniteLegaleTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneUniteLegale
 * @internal
 * @small
 */
class ApiFrInseeSireneUniteLegaleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeSireneUniteLegale
	 */
	protected ApiFrInseeSireneUniteLegale $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetSiren() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getSiren());
		$expected = 'qsdfghjklm';
		$this->_object->setSiren($expected);
		$this->assertEquals($expected, $this->_object->getSiren());
	}
	
	public function testHasStatutDiffusionUniteLegale() : void
	{
		$this->assertFalse($this->_object->hasStatutDiffusionUniteLegale());
		$expected = true;
		$this->_object->setStatutDiffusionUniteLegale($expected);
		$this->assertTrue($this->_object->hasStatutDiffusionUniteLegale());
	}
	
	public function testHasUnitePurgeeUniteLegale() : void
	{
		$this->assertFalse($this->_object->hasUnitePurgeeUniteLegale());
		$expected = true;
		$this->_object->setUnitePurgeeUniteLegale($expected);
		$this->assertTrue($this->_object->hasUnitePurgeeUniteLegale());
	}
	
	public function testGetDateCreationUniteLegale() : void
	{
		$this->assertNull($this->_object->getDateCreationUniteLegale());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateCreationUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getDateCreationUniteLegale());
	}
	
	public function testGetSigleUniteLegale() : void
	{
		$this->assertNull($this->_object->getSigleUniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setSigleUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getSigleUniteLegale());
	}
	
	public function testGetSexeUniteLegale() : void
	{
		$this->assertNull($this->_object->getSexeUniteLegale());
		$expected = $this->getMockBuilder(ApiFrInseeSireneGender::class)->disableOriginalConstructor()->getMock();
		$this->_object->setSexeUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getSexeUniteLegale());
	}
	
	public function testGetPrenom1UniteLegale() : void
	{
		$this->assertNull($this->_object->getPrenom1UniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setPrenom1UniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getPrenom1UniteLegale());
	}
	
	public function testGetPrenom2UniteLegale() : void
	{
		$this->assertNull($this->_object->getPrenom2UniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setPrenom2UniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getPrenom2UniteLegale());
	}
	
	public function testGetPrenom3UniteLegale() : void
	{
		$this->assertNull($this->_object->getPrenom3UniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setPrenom3UniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getPrenom3UniteLegale());
	}
	
	public function testGetPrenom4UniteLegale() : void
	{
		$this->assertNull($this->_object->getPrenom4UniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setPrenom4UniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getPrenom4UniteLegale());
	}
	
	public function testGetPrenomUsuelUniteLegale() : void
	{
		$this->assertNull($this->_object->getPrenomUsuelUniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setPrenomUsuelUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getPrenomUsuelUniteLegale());
	}
	
	public function testGetPseudonymeUniteLegale() : void
	{
		$this->assertNull($this->_object->getPseudonymeUniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setPseudonymeUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getPseudonymeUniteLegale());
	}
	
	public function testGetIdentifiantAssociationUniteLegale() : void
	{
		$this->assertNull($this->_object->getIdentifiantAssociationUniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setIdentifiantAssociationUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getIdentifiantAssociationUniteLegale());
	}
	
	public function testGetTrancheEffectifsUniteLegale() : void
	{
		$this->assertNull($this->_object->getTrancheEffectifsUniteLegale());
		$expected = $this->getMockBuilder(ApiFrInseeSireneTrancheEffectifs::class)->disableOriginalConstructor()->getMock();
		$this->_object->setTrancheEffectifsUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getTrancheEffectifsUniteLegale());
	}
	
	public function testGetAnneeEffectifsUniteLegale() : void
	{
		$this->assertNull($this->_object->getAnneeEffectifsUniteLegale());
		$expected = 25;
		$this->_object->setAnneeEffectifsUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getAnneeEffectifsUniteLegale());
	}
	
	public function testGetDateDernierTraitementUniteLegale() : void
	{
		$this->assertNull($this->_object->getDateDernierTraitementUniteLegale());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateDernierTraitementUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getDateDernierTraitementUniteLegale());
	}
	
	public function testGetNombrePeriodesUniteLegale() : void
	{
		$this->assertNull($this->_object->getNombrePeriodesUniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setNombrePeriodesUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getNombrePeriodesUniteLegale());
	}
	
	public function testGetCategorieEntreprise() : void
	{
		$this->assertNull($this->_object->getCategorieEntreprise());
		$expected = $this->getMockBuilder(ApiFrInseeSireneCategorieEntreprise::class)->disableOriginalConstructor()->getMock();
		$this->_object->setCategorieEntreprise($expected);
		$this->assertEquals($expected, $this->_object->getCategorieEntreprise());
	}
	
	public function testGetAnneeCategorieEntreprise() : void
	{
		$this->assertNull($this->_object->getAnneeCategorieEntreprise());
		$expected = 25;
		$this->_object->setAnneeCategorieEntreprise($expected);
		$this->assertEquals($expected, $this->_object->getAnneeCategorieEntreprise());
	}
	
	public function testGetDateDebut() : void
	{
		$this->assertNull($this->_object->getDateDebut());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateDebut($expected);
		$this->assertEquals($expected, $this->_object->getDateDebut());
	}
	
	public function testGetEtatAdministratifUniteLegale() : void
	{
		$this->assertNull($this->_object->getEtatAdministratifUniteLegale());
		$expected = $this->getMockBuilder(ApiFrInseeSireneEtatAdministratif::class)->disableOriginalConstructor()->getMock();
		$this->_object->setEtatAdministratifUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getEtatAdministratifUniteLegale());
	}
	
	public function testGetNomUniteLegale() : void
	{
		$this->assertNull($this->_object->getNomUniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setNomUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getNomUniteLegale());
	}
	
	public function testGetNomUsageUniteLegale() : void
	{
		$this->assertNull($this->_object->getNomUsageUniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setNomUsageUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getNomUsageUniteLegale());
	}
	
	public function testGetDenominationUniteLegale() : void
	{
		$this->assertNull($this->_object->getDenominationUniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setDenominationUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getDenominationUniteLegale());
	}
	
	public function testGetDenominationUsuelle1UniteLegale() : void
	{
		$this->assertNull($this->_object->getDenominationUsuelle1UniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setDenominationUsuelle1UniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getDenominationUsuelle1UniteLegale());
	}
	
	public function testGetDenominationUsuelle2UniteLegale() : void
	{
		$this->assertNull($this->_object->getDenominationUsuelle2UniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setDenominationUsuelle2UniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getDenominationUsuelle2UniteLegale());
	}
	
	public function testGetDenominationUsuelle3UniteLegale() : void
	{
		$this->assertNull($this->_object->getDenominationUsuelle3UniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setDenominationUsuelle3UniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getDenominationUsuelle3UniteLegale());
	}
	
	public function testGetCategorieJuridiqueUniteLegale() : void
	{
		$this->assertNull($this->_object->getCategorieJuridiqueUniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setCategorieJuridiqueUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getCategorieJuridiqueUniteLegale());
	}
	
	public function testGetActivitePrincipaleUniteLegale() : void
	{
		$this->assertNull($this->_object->getActivitePrincipaleUniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setActivitePrincipaleUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getActivitePrincipaleUniteLegale());
	}
	
	public function testGetNomenclatureActivitePrincipaleUniteLegale() : void
	{
		$this->assertNull($this->_object->getNomenclatureActivitePrincipaleUniteLegale());
		$expected = $this->getMockBuilder(ApiFrInseeSireneNomenclatureApe::class)->disableOriginalConstructor()->getMock();
		$this->_object->setNomenclatureActivitePrincipaleUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getNomenclatureActivitePrincipaleUniteLegale());
	}
	
	public function testGetNicSiegeUniteLegale() : void
	{
		$this->assertNull($this->_object->getNicSiegeUniteLegale());
		$expected = 'qsdfghjklm';
		$this->_object->setNicSiegeUniteLegale($expected);
		$this->assertEquals($expected, $this->_object->getNicSiegeUniteLegale());
	}
	
	public function testHasEconomieSocialeSolidaireUniteLegale() : void
	{
		$this->assertNull($this->_object->hasEconomieSocialeSolidaireUniteLegale());
		$expected = true;
		$this->_object->setEconomieSocialeSolidaireUniteLegale($expected);
		$this->assertTrue($this->_object->hasEconomieSocialeSolidaireUniteLegale());
	}
	
	public function testHasSocieteMissionUniteLegale() : void
	{
		$this->assertNull($this->_object->hasSocieteMissionUniteLegale());
		$expected = true;
		$this->_object->setSocieteMissionUniteLegale($expected);
		$this->assertTrue($this->_object->hasSocieteMissionUniteLegale());
	}
	
	public function testHasCaractereEmployeurUniteLegale() : void
	{
		$this->assertNull($this->_object->hasCaractereEmployeurUniteLegale());
		$expected = true;
		$this->_object->setCaractereEmployeurUniteLegale($expected);
		$this->assertTrue($this->_object->hasCaractereEmployeurUniteLegale());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeSireneUniteLegale('azertyuiop', false, false);
	}
	
}
