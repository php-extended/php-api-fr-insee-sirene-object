<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneNomenclatureApe;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneNomenclatureApeParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * InseeSireneNomenclatureApeParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneNomenclatureApeParser
 *
 * @internal
 *
 * @small
 */
class ApiFrInseeSireneNomenclatureApeParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeSireneNomenclatureApeParser
	 */
	protected ApiFrInseeSireneNomenclatureApeParser $_object;
	
	public function testParsedSuccess() : void
	{
		$expected = new ApiFrInseeSireneNomenclatureApe(3, 'NAFRev1', 'Norme NAF 2003');
		
		$this->assertEquals($expected, $this->_object->parse('NAFRev1'));
	}
	
	public function testParsedFailed() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('XYZ');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeSireneNomenclatureApeParser();
	}
	
}
