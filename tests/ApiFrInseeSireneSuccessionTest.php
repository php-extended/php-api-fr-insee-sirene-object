<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneSuccession;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeSireneSuccessionTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneSuccession
 * @internal
 * @small
 */
class ApiFrInseeSireneSuccessionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeSireneSuccession
	 */
	protected ApiFrInseeSireneSuccession $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetSiretEtablissementPredecesseur() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getSiretEtablissementPredecesseur());
		$expected = 'qsdfghjklm';
		$this->_object->setSiretEtablissementPredecesseur($expected);
		$this->assertEquals($expected, $this->_object->getSiretEtablissementPredecesseur());
	}
	
	public function testGetSiretEtablissementSuccesseur() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getSiretEtablissementSuccesseur());
		$expected = 'qsdfghjklm';
		$this->_object->setSiretEtablissementSuccesseur($expected);
		$this->assertEquals($expected, $this->_object->getSiretEtablissementSuccesseur());
	}
	
	public function testGetDateLienSuccession() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), $this->_object->getDateLienSuccession());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateLienSuccession($expected);
		$this->assertEquals($expected, $this->_object->getDateLienSuccession());
	}
	
	public function testHasTransfertSiege() : void
	{
		$this->assertFalse($this->_object->hasTransfertSiege());
		$expected = true;
		$this->_object->setTransfertSiege($expected);
		$this->assertTrue($this->_object->hasTransfertSiege());
	}
	
	public function testHasContinuiteEconomique() : void
	{
		$this->assertFalse($this->_object->hasContinuiteEconomique());
		$expected = true;
		$this->_object->setContinuiteEconomique($expected);
		$this->assertTrue($this->_object->hasContinuiteEconomique());
	}
	
	public function testGetDateDernierTraitementLienSuccession() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), $this->_object->getDateDernierTraitementLienSuccession());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateDernierTraitementLienSuccession($expected);
		$this->assertEquals($expected, $this->_object->getDateDernierTraitementLienSuccession());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeSireneSuccession('azertyuiop', 'azertyuiop', DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), false, false, DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'));
	}
	
}
