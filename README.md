# php-extended/php-api-fr-insee-sirene-object

A library that implements the php-extended/php-api-fr-insee-sirene-interface library.

![coverage](https://gitlab.com/php-extended/php-api-fr-insee-sirene-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-fr-insee-sirene-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-fr-insee-sirene-object ^8`


## Basic Usage

This library gives an unique endpoint : `\PhpExtended\Insee\InseeCogEndpoint`
from which all data can be retrieved.

- For most usages, you may use the following code :

```php

use PhpExtended\Insee\InseeSireneEndpoint;
use PhpExtended\Endpoint\ZipHttpEndpoint;
use PhpExtended\Endpoint\HttpEndpoint;

/** $client \Psr\Http\Client\ClientInterface */

$endpoint = new InseeSireneEndpoint(new ZipHttpEndpoint(new HttpEndpoint($client)));

$unitesLegales = $endpoint->getLatestStockUniteLegaleIterator();

foreach($unitesLegales as $uniteLegale)
{
	/** @var $sireneLine \PhpExtended\Insee\InseeSireneUniteLegaleInterface */
}


```

Be wary that the client must follow a certain number of rules regarding the
handling of files via the `X-Php-Download-File` request and response
headers for the `ZipHttpEndpoint` to be able to unzip the downloaded
file without having to use gigabytes of memory. It must also return an 
`X-Request-Uri` header to get the full uri back.


For an example of the minimal needed http client, look at the 
`InseeSireneEndpointTest` class file and find the client that is used.


If you have that much memory however, you can bypass the `ZipHttpEndpoint`
and all of the `X-Php-Download-File` and `X-Request-Uri` shenanigans
altogether.


## License

- The code is under MIT (See [license file](LICENSE)).

- The data is under Open License ([English](https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf) / [Français](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf))

- This data contains Personal Identifiable Information (PII) and should be taken care of [accordingly to the GDPR](https://gdpr.eu/).
