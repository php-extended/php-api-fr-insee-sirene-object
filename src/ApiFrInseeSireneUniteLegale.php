<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use DateTimeInterface;

/**
 * ApiFrInseeSireneUniteLegale class file.
 * 
 * This is a simple implementation of the ApiFrInseeSireneUniteLegaleInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.ExcessiveClassLength")
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.LongVariable")
 * @SuppressWarnings("PHPMD.TooManyFields")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class ApiFrInseeSireneUniteLegale implements ApiFrInseeSireneUniteLegaleInterface
{
	
	/**
	 * Un numéro d’identité de l’unité légale est attribué par
	 * l’Insee à toutes les personnes physiques ou morales inscrites au
	 * répertoire ainsi qu’à leurs établissements : le numéro Siren. Ce
	 * numéro unique est « attribué soit à l’occasion des demandes
	 * d’immatriculation au registre du commerce et des sociétés ou des
	 * déclarations effectuées au répertoire des métiers, soit à la
	 * demande d’administrations » (article R123-224 du code de commerce).
	 * Lors de sa création, une unité légale se voit attribuer un numéro
	 * Siren de 9 chiffres.
	 * 
	 * *Règles de gestion*
	 * Les entrepreneurs individuels, ou personnes physiques, conservent le
	 * même numéro Siren jusqu’à leur décès. Les sociétés, ou
	 * personnes morales, perdent la personnalité juridique au moment de la
	 * cessation de l’activité de l’entreprise. Si l’activité devait
	 * reprendre ultérieurement, un nouveau numéro Siren sera attribué. Les
	 * numéros d’identification sont uniques : lorsqu’un numéro Siren a
	 * été attribué, il ne peut pas être réutilisé et attribué à une
	 * nouvelle unité légale, même lorsque l’activité a cessé.
	 * 
	 * *Historique*
	 * Même si la mise en place du répertoire Sirene remonte à 1973, toutes
	 * les unités légales, y compris celles créées avant cette date,
	 * disposent d’un numéro Siren pour le secteur privé non agricole. En
	 * 1983, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus aux institutions et services de
	 * l’État et aux collectivités territoriales.
	 * En 1993, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus au secteur privé agricole.
	 * 
	 * Longueur : 9
	 * 
	 * @var string
	 */
	protected string $_siren;
	
	/**
	 * Seules les unités légales diffusibles sont accessibles à tout public
	 * (statutDiffusionUniteLegale=O).
	 * 
	 * @var bool
	 */
	protected bool $_statutDiffusionUniteLegale;
	
	/**
	 * Cette variable indique si l'unité légale a été purgée.
	 * Pour des raisons de capacité de stockage des données, les données
	 * concernant les entreprises cessées avant le 31/12/2002 ont été
	 * purgées.
	 * Pour ces unités cessées dites purgées :
	 *  - Seules les dernières valeurs des variables de niveau Unité Légale
	 * et de niveau Etablissement sont conservées
	 *  - En théorie, seul l'établissement siège au moment de la purge est
	 * conservé avec uniquement les dernières valeurs de cet établissement.
	 * Toutefois, pour plus de 300 unités légales purgées de la base, cette
	 * règle n’est pas respectée et ces unités ont toujours plus d'un
	 * établissement en base sans pouvoir garantir que tous les
	 * établissements ont été conservés.
	 * Plus de 4 millions d'unités légales sont purgées. Plus d’une unité
	 * purgée sur quatre a une date de création indéterminée.
	 * 
	 * *Diffusion des unités purgées*
	 * L'état administratif de l'unité purgée est : cessé.
	 * L'indicatrice unitePurgeeUniteLegale est présente et est égale à
	 * true.
	 * 
	 * *Diffusion des établissements des unités purgées*
	 * Une seule période avec dateDebut=date de début de l'état fermé si
	 * cette date est renseignée, sinon dateDebut (établissement) est à
	 * null.
	 * L'état administratif de l'établissement est : fermé.
	 * 
	 * @var bool
	 */
	protected bool $_unitePurgeeUniteLegale;
	
	/**
	 * La date de création correspond à la date qui figure dans les statuts
	 * de l’entreprise déposés au CFE compétent.
	 * Pour les unités purgées (unitePurgeeUniteLegale=true) : si la date de
	 * création est au 01/01/1900 dans Sirene, la date est forcée à null.
	 * Dans tous les autres cas, la date de création n'est jamais à null. Si
	 * elle est non renseignée, elle sera au 01/01/1900. La date de création
	 * ne correspond pas obligatoirement à dateDebut de la première période
	 * de l'unité légale.
	 * Certaines variables historisées peuvent posséder des dates de début
	 * soit au 01/01/1900, soit antérieures à la date de création.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_dateCreationUniteLegale = null;
	
	/**
	 * Un sigle est une forme réduite de la raison sociale ou de la
	 * dénomination d'une personne morale ou d'un organisme public.
	 * 
	 * Il est habituellement constitué des initiales de certains des mots de
	 * la dénomination. Afin d'en faciliter la prononciation, il arrive qu'on
	 * retienne les deux ou trois premières lettres de certains mots : il
	 * s'agit alors, au sens strict, d'un acronyme; mais l'usage a étendu à
	 * ce cas l'utilisation du terme sigle. Cette variable est à null pour les
	 * personnes physiques. Elle peut être non renseignée pour les personnes
	 * morales.
	 * 
	 * Longueur : 20
	 * 
	 * @var ?string
	 */
	protected ?string $_sigleUniteLegale = null;
	
	/**
	 * Cette variable est à null pour les personnes morales. La variable
	 * sexeUniteLegale est également non renseignée pour quelques personnes
	 * physiques.
	 * 
	 * @var ?ApiFrInseeSireneGenderInterface
	 */
	protected ?ApiFrInseeSireneGenderInterface $_sexeUniteLegale = null;
	
	/**
	 * Les variables prenom1UniteLegale à prenom4UniteLegale sont les prénoms
	 * déclarés pour une personne physique.
	 * Ces variables sont à null pour les personnes morales.
	 * Toute personne physique sera identifiée au minimum par son nom de
	 * naissance et son premier prénom. La variable prenom1UniteLegale peut
	 * être à null pour des personnes physiques (unités purgées).
	 * Les prenom1UniteLegale à prenom4UniteLegale peuvent contenir des *, qui
	 * ne sont pas significatifs.
	 * 
	 * Longueur : 20
	 * 
	 * @var ?string
	 */
	protected ?string $_prenom1UniteLegale = null;
	
	/**
	 * Deuxième prénom déclaré pour une personne physique.
	 * 
	 * @var ?string
	 */
	protected ?string $_prenom2UniteLegale = null;
	
	/**
	 * Troisième prénom déclaré pour une personne physique.
	 * 
	 * @var ?string
	 */
	protected ?string $_prenom3UniteLegale = null;
	
	/**
	 * Quatrième prénom déclaré pour une personne physique.
	 * 
	 * @var ?string
	 */
	protected ?string $_prenom4UniteLegale = null;
	
	/**
	 * Le prénom usuel est le prénom par lequel une personne choisit de se
	 * faire appeler dans la vie courante, parmi l'ensemble de ceux qui lui ont
	 * été donnés à sa naissance et qui sont inscrits à l'état civil.
	 * Il correspond généralement au prenom1UniteLegale. Cette variable est
	 * à null pour les personnes morales.
	 * 
	 * Longueur : 20
	 * 
	 * @var ?string
	 */
	protected ?string $_prenomUsuelUniteLegale = null;
	
	/**
	 * Cette variable correspond au nom qu’une personne utilise pour se
	 * désigner dans l’exercice de son activité, généralement littéraire
	 * ou artistique.
	 * Le pseudonyme est protégé, comme le nom de famille, contre
	 * l’usurpation venant d’un tiers.
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_pseudonymeUniteLegale = null;
	
	/**
	 * Lors de sa déclaration en préfecture, l’association reçoit
	 * automatiquement un numéro d’inscription au RNA. Elle doit en outre
	 * demander son immatriculation au répertoire Sirene lorsqu'elle souhaite
	 * demander des subventions auprès de l'État ou des collectivités
	 * territoriales, lorsqu'elle emploie des salariés ou lorsqu'elle exerce
	 * des activités qui conduisent au paiement de la TVA ou de l'impôt sur
	 * les sociétés. Le RNA est le fichier national, géré par le ministère
	 * de l'Intérieur, qui recense l'ensemble des informations sur les
	 * associations.
	 * 
	 * Longueur : 10
	 * 
	 * @var ?string
	 */
	protected ?string $_identifiantAssociationUniteLegale = null;
	
	/**
	 * Il s’agit d’une variable statistique, millésimée au 31/12 d’une
	 * année donnée (voir variable anneeEffectifsUniteLegale).
	 * 
	 * @var ?ApiFrInseeSireneTrancheEffectifsInterface
	 */
	protected ?ApiFrInseeSireneTrancheEffectifsInterface $_trancheEffectifsUniteLegale = null;
	
	/**
	 * Seule la dernière année de mise à jour de l’effectif salarié de
	 * l’unité légale est donnée si celle-ci est inférieure ou égale à
	 * l’année d’interrogation-3. Ainsi une interrogation en 2018 ne
	 * renverra la dernière année de mise à jour de l’effectif que si
	 * cette année est supérieure ou égale à 2015.
	 * 
	 * @var ?int
	 */
	protected ?int $_anneeEffectifsUniteLegale = null;
	
	/**
	 * Cette date peut concerner des mises à jour de données du répertoire
	 * Sirene qui ne sont pas diffusées. Cette variable peut être à null.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_dateDernierTraitementUniteLegale = null;
	
	/**
	 * Cette variable donne le nombre de périodes [dateDebut,dateFin] de
	 * l’unité légale. Chaque période correspond à l’intervalle de
	 * temps pendant lequel la totalité des variables historisées de
	 * l’unité légale n’ont pas été modifiées.
	 * 
	 * Les dates de ces périodes sont des dates d’effet (et non des dates de
	 * traitement).
	 * 
	 * @var ?string
	 */
	protected ?string $_nombrePeriodesUniteLegale = null;
	
	/**
	 * La catégorie d’entreprise est une variable statistique et calculée
	 * par l'Insee. Ce n’est pas une variable du répertoire Sirene.
	 * Définition de la catégorie d’entreprise :
	 *  - https://www.insee.fr/fr/metadonnees/definition/c1057
	 * Méthodologie de calcul et diffusion de la catégorie d’entreprise :
	 *  - https://www.insee.fr/fr/information/1730869.
	 * 
	 * @var ?ApiFrInseeSireneCategorieEntrepriseInterface
	 */
	protected ?ApiFrInseeSireneCategorieEntrepriseInterface $_categorieEntreprise = null;
	
	/**
	 * Cette variable désigne l’année de validité correspondant à la
	 * catégorie d'entreprise diffusée.
	 * 
	 * Longueur : 4
	 * 
	 * @var ?int
	 */
	protected ?int $_anneeCategorieEntreprise = null;
	
	/**
	 * Date de début de la période au cours de laquelle toutes les variables
	 * historisées de l'entreprise restent inchangées.
	 * La date 1900-01-01 signifie : date non déterminée. dateDebut
	 * peut-être vide uniquement pour les unités purgées (cf. variable
	 * unitePurgeeUniteLegale). La date de début de la période la plus
	 * ancienne ne correspond pas obligatoirement à la date de création de
	 * l'entreprise, certaines variables historisées pouvant posséder des
	 * dates de début soit au 1900-01-01, soit antérieures à la date de
	 * création.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_dateDebut = null;
	
	/**
	 * Le passage à l’état « Cessée » découle de la prise en compte
	 * d’une déclaration de cessation administrative. Pour les personnes
	 * morales, cela correspond au dépôt de la déclaration de disparition de
	 * la personne morale.
	 * Pour les personnes physiques, cela correspond soit à la prise en compte
	 * de la déclaration de cessation d’activité déposée par
	 * l’exploitant soit au décès de l’exploitant conformément à la
	 * réglementation.
	 * En dehors de ces cas, l’unité légale est toujours à l’état
	 * administratif « Active ». Pour les personnes morales, la cessation
	 * administrative est, en théorie, définitive, l’état administratif
	 * "Cessée" est irréversible.
	 * Cependant, il existe actuellement dans la base un certain nombre
	 * d’unités légales personnes morales avec un historique d'état
	 * présentant un état cessé entre deux périodes à l’état actif.
	 * Pour les personnes physiques, dans le cas où l’exploitant déclare la
	 * cessation de son activité, puis la reprend quelque temps plus tard, cet
	 * état est réversible. Il est donc normal d'avoir des périodes
	 * successives d'état actif puis cessé pour les personnes physiques. En
	 * règle générale, la première période d’historique d’une unité
	 * légale correspond à un etatAdministratifUniteLegale égal à « Active
	 * ». Toutefois, l'état administratif peut être à null (première date
	 * de début de l'état postérieure à la première date de début d'une
	 * autre variable historisée).
	 * 
	 * @var ?ApiFrInseeSireneEtatAdministratifInterface
	 */
	protected ?ApiFrInseeSireneEtatAdministratifInterface $_etatAdministratifUniteLegale = null;
	
	/**
	 * Cette variable indique le libellé le nom de naissance pour une personne
	 * physique. Cette variable est à null pour les personnes morales. Le
	 * répertoire Sirene gère des caractères majuscules non accentués et
	 * les seuls caractères spéciaux tiret (-) et apostrophe. Le nom peut
	 * être à null (cas des unités purgées, première date de début du nom
	 * postérieure à la première date de début d'une autre variable
	 * historisée).
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_nomUniteLegale = null;
	
	/**
	 * Le nom d’usage est celui que la personne physique a choisi
	 * d’utiliser.
	 * Cette variable est à null pour les personnes morales. Elle peut être
	 * également à null pour les personnes physiques.
	 * Le répertoire Sirene gère des caractères majuscules non accentués et
	 * les seuls caractères spéciaux tiret (-) et apostrophe.
	 * Cette variable est historisée.
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_nomUsageUniteLegale = null;
	
	/**
	 * Cette variable désigne la raison sociale pour les personnes morales. Il
	 * s'agit du nom sous lequel est déclarée l'unité légale. Cette
	 * variable est à null pour les personnes physiques.
	 * Le répertoire Sirene gère des caractères majuscules non accentués
	 * avec caractères spéciaux (- & + @ ! ? * ° . % : € #).
	 * La dénomination peut parfois contenir la mention de la forme de la
	 * société (SA, SAS, SARL, etc.).
	 * 
	 * *Historique*
	 * Un travail de reconstitution des historiques de dénomination a été
	 * entrepris en 2005. Toutefois, avant cette date, des historiques de
	 * dénomination peuvent s’avérer incomplets. La dénomination peut
	 * être à null (cas des unités purgées, première date de début de la
	 * dénomination postérieure à la première date de début d'une autre
	 * variable historisée).
	 * 
	 * Longueur : 120
	 * 
	 * @var ?string
	 */
	protected ?string $_denominationUniteLegale = null;
	
	/**
	 * Cette variable désigne le nom (ou les noms) sous lequel l'entreprise
	 * est connue du grand public. Cet élément d'identification de
	 * l'entreprise (sur trois champs : denominationUsuelle1UniteLegale,
	 * denominationUsuelle2UniteLegale et denominationUsuelle3UniteLegale) a
	 * été enregistré au niveau unité légale avant l'application de la
	 * norme d'échanges CFE de 2008. À partir de la norme 2008, la
	 * dénomination usuelle est enregistrée au niveau de l'établissement sur
	 * un seul champ : denominationUsuelleEtablissement.
	 * Variables historisées avec une seule indicatrice de changement pour les
	 * trois variables.
	 * 
	 * Longueur : 70
	 * 
	 * @var ?string
	 */
	protected ?string $_denominationUsuelle1UniteLegale = null;
	
	/**
	 * Second champ de la dénomination usuelle.
	 * 
	 * @var ?string
	 */
	protected ?string $_denominationUsuelle2UniteLegale = null;
	
	/**
	 * Dernier champ de la dénomination usuelle.
	 * 
	 * @var ?string
	 */
	protected ?string $_denominationUsuelle3UniteLegale = null;
	
	/**
	 * La catégorie juridique est un attribut des unités légales.
	 * Cette variable est à 1000 pour les personnes physiques.
	 * Lors de son dépôt de demande de création, le déclarant indique la
	 * forme juridique de l’unité légale qu’il crée, qui est ensuite
	 * traduite en code.
	 * Ce code est modifiable, à la marge, au cours de la vie de l’unité
	 * légale (pour les personnes morales) en fonction des déclarations de
	 * l’exploitant. Pour chaque unité légale, il existe à un instant
	 * donné un seul code Catégorie juridique. Il est attribué selon la
	 * nomenclature en vigueur.
	 * 
	 * *Historique*
	 * La catégorie juridique peut être à null (cas des unités purgées,
	 * première date de début de la catégorie juridique postérieure à la
	 * première date de début d'une autre variable historisée). En revanche,
	 * le libellé associé aux catégories juridiques n’a pas été
	 * historisé, si bien que des codes peuvent paraître « Hors nomenclature
	 * », alors qu’ils ont été valides à un instant donné.
	 * 
	 * Longueur : 4
	 * 
	 * @var ?string
	 */
	protected ?string $_categorieJuridiqueUniteLegale = null;
	
	/**
	 * Lors de son inscription au répertoire, l’Insee attribue à toute
	 * unité légale un code dit « APE » sur la base de la description de
	 * l’activité principale faite par le déclarant. Ce code est modifiable
	 * au cours de la vie de l’unité légale en fonction des déclarations
	 * de l’exploitant.
	 * Pour chaque unité légale, il existe à un instant donné un seul code
	 * « APE ». Il est attribué selon la nomenclature en vigueur. La
	 * nomenclature en vigueur est la Naf Rév2 et ce depuis le 1 er Janvier
	 * 2008.
	 * Chaque code comporte 2 chiffres, un point, 2 chiffres et une lettre.
	 * Toutes les unités légales actives au 01/01/2008 ont eu leur code APE
	 * recodé dans la nouvelle nomenclature, ainsi de très nombreuses
	 * entreprises ont une période débutant à cette date. Au moment de la
	 * déclaration de l’entreprise, il peut arriver que l’Insee ne soit
	 * pas en mesure d’attribuer le bon code APE : la modalité 00.00Z peut
	 * alors être affectée provisoirement.
	 * 
	 * *Historique*
	 * Le code APE est historisé depuis le 01/01/2005.
	 * La règle d’historisation des données d’activité est la suivante :
	 *  - Pour les entreprises cessées avant le 31/12/2004, seul le dernier
	 * code activité connu figure, dans la nomenclature en vigueur à la date
	 * de fermeture.
	 *  - Pour les entreprises actives après le 01/01/2005 et cessées avant
	 * le 31/12/2007, l’historique des codes attribués sur la période est
	 * disponible.
	 *  - Pour les entreprises actives après le 01/01/2005 et toujours actives
	 * le 1/1/2008, l’historique intègre le changement de nomenclature.
	 *  - Pour les entreprises créées après le 01/01/2008, l’historique
	 * comprend les modifications apportées au cours de la vie de
	 * l’entreprise.
	 * 
	 * L'APE peut être à null (cas des unités purgées - pour des raisons de
	 * capacité de stockage des données, les données concernant les
	 * entreprises cessées avant le 31/12/2002 ont été purgées -, première
	 * date de début de l'APE postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * Longueur : 6
	 * 
	 * @var ?string
	 */
	protected ?string $_activitePrincipaleUniteLegale = null;
	
	/**
	 * Cette variable indique la nomenclature d’activité correspondant à la
	 * variable activitePrincipaleUniteLegale (cf.
	 * activitePrincipaleUniteLegale).
	 * 
	 * @var ?ApiFrInseeSireneNomenclatureApeInterface
	 */
	protected ?ApiFrInseeSireneNomenclatureApeInterface $_nomenclatureActivitePrincipaleUniteLegale = null;
	
	/**
	 * Le siège d’une unité légale est le lieu où sont centralisées
	 * l’administration et la direction effective de l’unité légale.
	 * À un instant donné, chaque unité légale a un seul siège. Mais, au
	 * cours de la vie de l’unité légale, le siège peut être différent.
	 * Chaque siège est identifié par un numéro Nic (Numéro Interne de
	 * Classement de l'établissement) qui respecte les règles d’attribution
	 * des numéros d’établissement.
	 * Le Nic est composé de quatre chiffres et d'un cinquième qui permet de
	 * contrôler la validité du numéro Siret (concaténation du numéro
	 * Siren et du Nic).
	 * 
	 * *Historique*
	 * Le Nic du siège peut être à null sur une période mais, en règle
	 * générale, pas sur l’ensemble de l’historique (cas des unités
	 * purgées, première date de début du Nic postérieure à la première
	 * date de début d'une autre variable historisée).
	 * 
	 * Longueur : 5
	 * 
	 * @var ?string
	 */
	protected ?string $_nicSiegeUniteLegale = null;
	
	/**
	 * Cette variable indique si l'entreprise appartient au champ de
	 * l’économie sociale et solidaire. La loi n° 2014-856 du 31 juillet
	 * 2014 définit officiellement le périmètre de l’économie sociale et
	 * solidaire (ESS).
	 * Celle-ci comprend les quatre familles traditionnelles en raison de leur
	 * régime juridique (associations, fondations, coopératives et mutuelles)
	 * et inclut une nouvelle catégorie, les entreprises de l’ESS, adhérant
	 * aux mêmes principes :
	 *  - poursuivre un but social autre que le seul partage des bénéfices ;
	 *  - un caractère lucratif encadré (notamment des bénéfices
	 * majoritairement consacrés au maintien et au développement de
	 * l’activité) ;
	 *  - une gouvernance démocratique et participative.
	 * Cette variable est historisée, renseignée pour environ 1 million
	 * d'entreprises, sinon null.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_economieSocialeSolidaireUniteLegale = null;
	
	/**
	 * Si l'unité légale est une société à mission.
	 * Cette variable est historisée.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_societeMissionUniteLegale = null;
	
	/**
	 * Lors de sa formalité de création, le déclarant indique si l’unité
	 * légale aura ou non des employés. Par la suite, le déclarant peut
	 * également faire des déclarations de prise d’emploi et de fin
	 * d’emploi. La prise en compte d’une déclaration de prise d’emploi
	 * bascule immédiatement l’unité légale en « Employeuse ».
	 * Inversement, lorsqu’une déclaration de fin d’emploi est traitée,
	 * l’unité légale devient « Non employeuse ». Le caractère employeur
	 * est O si au moins l'un des établissements actifs de l'unité légale
	 * emploie des salariés.
	 * Cette variable est historisée.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_caractereEmployeurUniteLegale = null;
	
	/**
	 * Constructor for ApiFrInseeSireneUniteLegale with private members.
	 * 
	 * @param string $siren
	 * @param bool $statutDiffusionUniteLegale
	 * @param bool $unitePurgeeUniteLegale
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(string $siren, bool $statutDiffusionUniteLegale, bool $unitePurgeeUniteLegale)
	{
		$this->setSiren($siren);
		$this->setStatutDiffusionUniteLegale($statutDiffusionUniteLegale);
		$this->setUnitePurgeeUniteLegale($unitePurgeeUniteLegale);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets un numéro d’identité de l’unité légale est attribué par
	 * l’Insee à toutes les personnes physiques ou morales inscrites au
	 * répertoire ainsi qu’à leurs établissements : le numéro Siren. Ce
	 * numéro unique est « attribué soit à l’occasion des demandes
	 * d’immatriculation au registre du commerce et des sociétés ou des
	 * déclarations effectuées au répertoire des métiers, soit à la
	 * demande d’administrations » (article R123-224 du code de commerce).
	 * Lors de sa création, une unité légale se voit attribuer un numéro
	 * Siren de 9 chiffres.
	 * 
	 * *Règles de gestion*
	 * Les entrepreneurs individuels, ou personnes physiques, conservent le
	 * même numéro Siren jusqu’à leur décès. Les sociétés, ou
	 * personnes morales, perdent la personnalité juridique au moment de la
	 * cessation de l’activité de l’entreprise. Si l’activité devait
	 * reprendre ultérieurement, un nouveau numéro Siren sera attribué. Les
	 * numéros d’identification sont uniques : lorsqu’un numéro Siren a
	 * été attribué, il ne peut pas être réutilisé et attribué à une
	 * nouvelle unité légale, même lorsque l’activité a cessé.
	 * 
	 * *Historique*
	 * Même si la mise en place du répertoire Sirene remonte à 1973, toutes
	 * les unités légales, y compris celles créées avant cette date,
	 * disposent d’un numéro Siren pour le secteur privé non agricole. En
	 * 1983, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus aux institutions et services de
	 * l’État et aux collectivités territoriales.
	 * En 1993, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus au secteur privé agricole.
	 * 
	 * Longueur : 9
	 * 
	 * @param string $siren
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setSiren(string $siren) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_siren = $siren;
		
		return $this;
	}
	
	/**
	 * Gets un numéro d’identité de l’unité légale est attribué par
	 * l’Insee à toutes les personnes physiques ou morales inscrites au
	 * répertoire ainsi qu’à leurs établissements : le numéro Siren. Ce
	 * numéro unique est « attribué soit à l’occasion des demandes
	 * d’immatriculation au registre du commerce et des sociétés ou des
	 * déclarations effectuées au répertoire des métiers, soit à la
	 * demande d’administrations » (article R123-224 du code de commerce).
	 * Lors de sa création, une unité légale se voit attribuer un numéro
	 * Siren de 9 chiffres.
	 * 
	 * *Règles de gestion*
	 * Les entrepreneurs individuels, ou personnes physiques, conservent le
	 * même numéro Siren jusqu’à leur décès. Les sociétés, ou
	 * personnes morales, perdent la personnalité juridique au moment de la
	 * cessation de l’activité de l’entreprise. Si l’activité devait
	 * reprendre ultérieurement, un nouveau numéro Siren sera attribué. Les
	 * numéros d’identification sont uniques : lorsqu’un numéro Siren a
	 * été attribué, il ne peut pas être réutilisé et attribué à une
	 * nouvelle unité légale, même lorsque l’activité a cessé.
	 * 
	 * *Historique*
	 * Même si la mise en place du répertoire Sirene remonte à 1973, toutes
	 * les unités légales, y compris celles créées avant cette date,
	 * disposent d’un numéro Siren pour le secteur privé non agricole. En
	 * 1983, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus aux institutions et services de
	 * l’État et aux collectivités territoriales.
	 * En 1993, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus au secteur privé agricole.
	 * 
	 * Longueur : 9
	 * 
	 * @return string
	 */
	public function getSiren() : string
	{
		return $this->_siren;
	}
	
	/**
	 * Sets seules les unités légales diffusibles sont accessibles à tout
	 * public (statutDiffusionUniteLegale=O).
	 * 
	 * @param bool $statutDiffusionUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setStatutDiffusionUniteLegale(bool $statutDiffusionUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_statutDiffusionUniteLegale = $statutDiffusionUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets seules les unités légales diffusibles sont accessibles à tout
	 * public (statutDiffusionUniteLegale=O).
	 * 
	 * @return bool
	 */
	public function hasStatutDiffusionUniteLegale() : bool
	{
		return $this->_statutDiffusionUniteLegale;
	}
	
	/**
	 * Sets cette variable indique si l'unité légale a été purgée.
	 * Pour des raisons de capacité de stockage des données, les données
	 * concernant les entreprises cessées avant le 31/12/2002 ont été
	 * purgées.
	 * Pour ces unités cessées dites purgées :
	 *  - Seules les dernières valeurs des variables de niveau Unité Légale
	 * et de niveau Etablissement sont conservées
	 *  - En théorie, seul l'établissement siège au moment de la purge est
	 * conservé avec uniquement les dernières valeurs de cet établissement.
	 * Toutefois, pour plus de 300 unités légales purgées de la base, cette
	 * règle n’est pas respectée et ces unités ont toujours plus d'un
	 * établissement en base sans pouvoir garantir que tous les
	 * établissements ont été conservés.
	 * Plus de 4 millions d'unités légales sont purgées. Plus d’une unité
	 * purgée sur quatre a une date de création indéterminée.
	 * 
	 * *Diffusion des unités purgées*
	 * L'état administratif de l'unité purgée est : cessé.
	 * L'indicatrice unitePurgeeUniteLegale est présente et est égale à
	 * true.
	 * 
	 * *Diffusion des établissements des unités purgées*
	 * Une seule période avec dateDebut=date de début de l'état fermé si
	 * cette date est renseignée, sinon dateDebut (établissement) est à
	 * null.
	 * L'état administratif de l'établissement est : fermé.
	 * 
	 * @param bool $unitePurgeeUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setUnitePurgeeUniteLegale(bool $unitePurgeeUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_unitePurgeeUniteLegale = $unitePurgeeUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique si l'unité légale a été purgée.
	 * Pour des raisons de capacité de stockage des données, les données
	 * concernant les entreprises cessées avant le 31/12/2002 ont été
	 * purgées.
	 * Pour ces unités cessées dites purgées :
	 *  - Seules les dernières valeurs des variables de niveau Unité Légale
	 * et de niveau Etablissement sont conservées
	 *  - En théorie, seul l'établissement siège au moment de la purge est
	 * conservé avec uniquement les dernières valeurs de cet établissement.
	 * Toutefois, pour plus de 300 unités légales purgées de la base, cette
	 * règle n’est pas respectée et ces unités ont toujours plus d'un
	 * établissement en base sans pouvoir garantir que tous les
	 * établissements ont été conservés.
	 * Plus de 4 millions d'unités légales sont purgées. Plus d’une unité
	 * purgée sur quatre a une date de création indéterminée.
	 * 
	 * *Diffusion des unités purgées*
	 * L'état administratif de l'unité purgée est : cessé.
	 * L'indicatrice unitePurgeeUniteLegale est présente et est égale à
	 * true.
	 * 
	 * *Diffusion des établissements des unités purgées*
	 * Une seule période avec dateDebut=date de début de l'état fermé si
	 * cette date est renseignée, sinon dateDebut (établissement) est à
	 * null.
	 * L'état administratif de l'établissement est : fermé.
	 * 
	 * @return bool
	 */
	public function hasUnitePurgeeUniteLegale() : bool
	{
		return $this->_unitePurgeeUniteLegale;
	}
	
	/**
	 * Sets la date de création correspond à la date qui figure dans les
	 * statuts de l’entreprise déposés au CFE compétent.
	 * Pour les unités purgées (unitePurgeeUniteLegale=true) : si la date de
	 * création est au 01/01/1900 dans Sirene, la date est forcée à null.
	 * Dans tous les autres cas, la date de création n'est jamais à null. Si
	 * elle est non renseignée, elle sera au 01/01/1900. La date de création
	 * ne correspond pas obligatoirement à dateDebut de la première période
	 * de l'unité légale.
	 * Certaines variables historisées peuvent posséder des dates de début
	 * soit au 01/01/1900, soit antérieures à la date de création.
	 * 
	 * @param ?DateTimeInterface $dateCreationUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setDateCreationUniteLegale(?DateTimeInterface $dateCreationUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_dateCreationUniteLegale = $dateCreationUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets la date de création correspond à la date qui figure dans les
	 * statuts de l’entreprise déposés au CFE compétent.
	 * Pour les unités purgées (unitePurgeeUniteLegale=true) : si la date de
	 * création est au 01/01/1900 dans Sirene, la date est forcée à null.
	 * Dans tous les autres cas, la date de création n'est jamais à null. Si
	 * elle est non renseignée, elle sera au 01/01/1900. La date de création
	 * ne correspond pas obligatoirement à dateDebut de la première période
	 * de l'unité légale.
	 * Certaines variables historisées peuvent posséder des dates de début
	 * soit au 01/01/1900, soit antérieures à la date de création.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateCreationUniteLegale() : ?DateTimeInterface
	{
		return $this->_dateCreationUniteLegale;
	}
	
	/**
	 * Sets un sigle est une forme réduite de la raison sociale ou de la
	 * dénomination d'une personne morale ou d'un organisme public.
	 * 
	 * Il est habituellement constitué des initiales de certains des mots de
	 * la dénomination. Afin d'en faciliter la prononciation, il arrive qu'on
	 * retienne les deux ou trois premières lettres de certains mots : il
	 * s'agit alors, au sens strict, d'un acronyme; mais l'usage a étendu à
	 * ce cas l'utilisation du terme sigle. Cette variable est à null pour les
	 * personnes physiques. Elle peut être non renseignée pour les personnes
	 * morales.
	 * 
	 * Longueur : 20
	 * 
	 * @param ?string $sigleUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setSigleUniteLegale(?string $sigleUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_sigleUniteLegale = $sigleUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets un sigle est une forme réduite de la raison sociale ou de la
	 * dénomination d'une personne morale ou d'un organisme public.
	 * 
	 * Il est habituellement constitué des initiales de certains des mots de
	 * la dénomination. Afin d'en faciliter la prononciation, il arrive qu'on
	 * retienne les deux ou trois premières lettres de certains mots : il
	 * s'agit alors, au sens strict, d'un acronyme; mais l'usage a étendu à
	 * ce cas l'utilisation du terme sigle. Cette variable est à null pour les
	 * personnes physiques. Elle peut être non renseignée pour les personnes
	 * morales.
	 * 
	 * Longueur : 20
	 * 
	 * @return ?string
	 */
	public function getSigleUniteLegale() : ?string
	{
		return $this->_sigleUniteLegale;
	}
	
	/**
	 * Sets cette variable est à null pour les personnes morales. La variable
	 * sexeUniteLegale est également non renseignée pour quelques personnes
	 * physiques.
	 * 
	 * @param ?ApiFrInseeSireneGenderInterface $sexeUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setSexeUniteLegale(?ApiFrInseeSireneGenderInterface $sexeUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_sexeUniteLegale = $sexeUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets cette variable est à null pour les personnes morales. La variable
	 * sexeUniteLegale est également non renseignée pour quelques personnes
	 * physiques.
	 * 
	 * @return ?ApiFrInseeSireneGenderInterface
	 */
	public function getSexeUniteLegale() : ?ApiFrInseeSireneGenderInterface
	{
		return $this->_sexeUniteLegale;
	}
	
	/**
	 * Sets les variables prenom1UniteLegale à prenom4UniteLegale sont les
	 * prénoms déclarés pour une personne physique.
	 * Ces variables sont à null pour les personnes morales.
	 * Toute personne physique sera identifiée au minimum par son nom de
	 * naissance et son premier prénom. La variable prenom1UniteLegale peut
	 * être à null pour des personnes physiques (unités purgées).
	 * Les prenom1UniteLegale à prenom4UniteLegale peuvent contenir des *, qui
	 * ne sont pas significatifs.
	 * 
	 * Longueur : 20
	 * 
	 * @param ?string $prenom1UniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setPrenom1UniteLegale(?string $prenom1UniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_prenom1UniteLegale = $prenom1UniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets les variables prenom1UniteLegale à prenom4UniteLegale sont les
	 * prénoms déclarés pour une personne physique.
	 * Ces variables sont à null pour les personnes morales.
	 * Toute personne physique sera identifiée au minimum par son nom de
	 * naissance et son premier prénom. La variable prenom1UniteLegale peut
	 * être à null pour des personnes physiques (unités purgées).
	 * Les prenom1UniteLegale à prenom4UniteLegale peuvent contenir des *, qui
	 * ne sont pas significatifs.
	 * 
	 * Longueur : 20
	 * 
	 * @return ?string
	 */
	public function getPrenom1UniteLegale() : ?string
	{
		return $this->_prenom1UniteLegale;
	}
	
	/**
	 * Sets deuxième prénom déclaré pour une personne physique.
	 * 
	 * @param ?string $prenom2UniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setPrenom2UniteLegale(?string $prenom2UniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_prenom2UniteLegale = $prenom2UniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets deuxième prénom déclaré pour une personne physique.
	 * 
	 * @return ?string
	 */
	public function getPrenom2UniteLegale() : ?string
	{
		return $this->_prenom2UniteLegale;
	}
	
	/**
	 * Sets troisième prénom déclaré pour une personne physique.
	 * 
	 * @param ?string $prenom3UniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setPrenom3UniteLegale(?string $prenom3UniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_prenom3UniteLegale = $prenom3UniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets troisième prénom déclaré pour une personne physique.
	 * 
	 * @return ?string
	 */
	public function getPrenom3UniteLegale() : ?string
	{
		return $this->_prenom3UniteLegale;
	}
	
	/**
	 * Sets quatrième prénom déclaré pour une personne physique.
	 * 
	 * @param ?string $prenom4UniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setPrenom4UniteLegale(?string $prenom4UniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_prenom4UniteLegale = $prenom4UniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets quatrième prénom déclaré pour une personne physique.
	 * 
	 * @return ?string
	 */
	public function getPrenom4UniteLegale() : ?string
	{
		return $this->_prenom4UniteLegale;
	}
	
	/**
	 * Sets le prénom usuel est le prénom par lequel une personne choisit de
	 * se faire appeler dans la vie courante, parmi l'ensemble de ceux qui lui
	 * ont été donnés à sa naissance et qui sont inscrits à l'état civil.
	 * Il correspond généralement au prenom1UniteLegale. Cette variable est
	 * à null pour les personnes morales.
	 * 
	 * Longueur : 20
	 * 
	 * @param ?string $prenomUsuelUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setPrenomUsuelUniteLegale(?string $prenomUsuelUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_prenomUsuelUniteLegale = $prenomUsuelUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets le prénom usuel est le prénom par lequel une personne choisit de
	 * se faire appeler dans la vie courante, parmi l'ensemble de ceux qui lui
	 * ont été donnés à sa naissance et qui sont inscrits à l'état civil.
	 * Il correspond généralement au prenom1UniteLegale. Cette variable est
	 * à null pour les personnes morales.
	 * 
	 * Longueur : 20
	 * 
	 * @return ?string
	 */
	public function getPrenomUsuelUniteLegale() : ?string
	{
		return $this->_prenomUsuelUniteLegale;
	}
	
	/**
	 * Sets cette variable correspond au nom qu’une personne utilise pour se
	 * désigner dans l’exercice de son activité, généralement littéraire
	 * ou artistique.
	 * Le pseudonyme est protégé, comme le nom de famille, contre
	 * l’usurpation venant d’un tiers.
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $pseudonymeUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setPseudonymeUniteLegale(?string $pseudonymeUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_pseudonymeUniteLegale = $pseudonymeUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets cette variable correspond au nom qu’une personne utilise pour se
	 * désigner dans l’exercice de son activité, généralement littéraire
	 * ou artistique.
	 * Le pseudonyme est protégé, comme le nom de famille, contre
	 * l’usurpation venant d’un tiers.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getPseudonymeUniteLegale() : ?string
	{
		return $this->_pseudonymeUniteLegale;
	}
	
	/**
	 * Sets lors de sa déclaration en préfecture, l’association reçoit
	 * automatiquement un numéro d’inscription au RNA. Elle doit en outre
	 * demander son immatriculation au répertoire Sirene lorsqu'elle souhaite
	 * demander des subventions auprès de l'État ou des collectivités
	 * territoriales, lorsqu'elle emploie des salariés ou lorsqu'elle exerce
	 * des activités qui conduisent au paiement de la TVA ou de l'impôt sur
	 * les sociétés. Le RNA est le fichier national, géré par le ministère
	 * de l'Intérieur, qui recense l'ensemble des informations sur les
	 * associations.
	 * 
	 * Longueur : 10
	 * 
	 * @param ?string $identifiantAssociationUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setIdentifiantAssociationUniteLegale(?string $identifiantAssociationUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_identifiantAssociationUniteLegale = $identifiantAssociationUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets lors de sa déclaration en préfecture, l’association reçoit
	 * automatiquement un numéro d’inscription au RNA. Elle doit en outre
	 * demander son immatriculation au répertoire Sirene lorsqu'elle souhaite
	 * demander des subventions auprès de l'État ou des collectivités
	 * territoriales, lorsqu'elle emploie des salariés ou lorsqu'elle exerce
	 * des activités qui conduisent au paiement de la TVA ou de l'impôt sur
	 * les sociétés. Le RNA est le fichier national, géré par le ministère
	 * de l'Intérieur, qui recense l'ensemble des informations sur les
	 * associations.
	 * 
	 * Longueur : 10
	 * 
	 * @return ?string
	 */
	public function getIdentifiantAssociationUniteLegale() : ?string
	{
		return $this->_identifiantAssociationUniteLegale;
	}
	
	/**
	 * Sets il s’agit d’une variable statistique, millésimée au 31/12
	 * d’une année donnée (voir variable anneeEffectifsUniteLegale).
	 * 
	 * @param ?ApiFrInseeSireneTrancheEffectifsInterface $trancheEffectifsUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setTrancheEffectifsUniteLegale(?ApiFrInseeSireneTrancheEffectifsInterface $trancheEffectifsUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_trancheEffectifsUniteLegale = $trancheEffectifsUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets il s’agit d’une variable statistique, millésimée au 31/12
	 * d’une année donnée (voir variable anneeEffectifsUniteLegale).
	 * 
	 * @return ?ApiFrInseeSireneTrancheEffectifsInterface
	 */
	public function getTrancheEffectifsUniteLegale() : ?ApiFrInseeSireneTrancheEffectifsInterface
	{
		return $this->_trancheEffectifsUniteLegale;
	}
	
	/**
	 * Sets seule la dernière année de mise à jour de l’effectif salarié
	 * de l’unité légale est donnée si celle-ci est inférieure ou égale
	 * à l’année d’interrogation-3. Ainsi une interrogation en 2018 ne
	 * renverra la dernière année de mise à jour de l’effectif que si
	 * cette année est supérieure ou égale à 2015.
	 * 
	 * @param ?int $anneeEffectifsUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setAnneeEffectifsUniteLegale(?int $anneeEffectifsUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_anneeEffectifsUniteLegale = $anneeEffectifsUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets seule la dernière année de mise à jour de l’effectif salarié
	 * de l’unité légale est donnée si celle-ci est inférieure ou égale
	 * à l’année d’interrogation-3. Ainsi une interrogation en 2018 ne
	 * renverra la dernière année de mise à jour de l’effectif que si
	 * cette année est supérieure ou égale à 2015.
	 * 
	 * @return ?int
	 */
	public function getAnneeEffectifsUniteLegale() : ?int
	{
		return $this->_anneeEffectifsUniteLegale;
	}
	
	/**
	 * Sets cette date peut concerner des mises à jour de données du
	 * répertoire Sirene qui ne sont pas diffusées. Cette variable peut être
	 * à null.
	 * 
	 * @param ?DateTimeInterface $dateDernierTraitementUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setDateDernierTraitementUniteLegale(?DateTimeInterface $dateDernierTraitementUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_dateDernierTraitementUniteLegale = $dateDernierTraitementUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets cette date peut concerner des mises à jour de données du
	 * répertoire Sirene qui ne sont pas diffusées. Cette variable peut être
	 * à null.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateDernierTraitementUniteLegale() : ?DateTimeInterface
	{
		return $this->_dateDernierTraitementUniteLegale;
	}
	
	/**
	 * Sets cette variable donne le nombre de périodes [dateDebut,dateFin] de
	 * l’unité légale. Chaque période correspond à l’intervalle de
	 * temps pendant lequel la totalité des variables historisées de
	 * l’unité légale n’ont pas été modifiées.
	 * 
	 * Les dates de ces périodes sont des dates d’effet (et non des dates de
	 * traitement).
	 * 
	 * @param ?string $nombrePeriodesUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setNombrePeriodesUniteLegale(?string $nombrePeriodesUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_nombrePeriodesUniteLegale = $nombrePeriodesUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets cette variable donne le nombre de périodes [dateDebut,dateFin] de
	 * l’unité légale. Chaque période correspond à l’intervalle de
	 * temps pendant lequel la totalité des variables historisées de
	 * l’unité légale n’ont pas été modifiées.
	 * 
	 * Les dates de ces périodes sont des dates d’effet (et non des dates de
	 * traitement).
	 * 
	 * @return ?string
	 */
	public function getNombrePeriodesUniteLegale() : ?string
	{
		return $this->_nombrePeriodesUniteLegale;
	}
	
	/**
	 * Sets la catégorie d’entreprise est une variable statistique et
	 * calculée par l'Insee. Ce n’est pas une variable du répertoire
	 * Sirene.
	 * Définition de la catégorie d’entreprise :
	 *  - https://www.insee.fr/fr/metadonnees/definition/c1057
	 * Méthodologie de calcul et diffusion de la catégorie d’entreprise :
	 *  - https://www.insee.fr/fr/information/1730869.
	 * 
	 * @param ?ApiFrInseeSireneCategorieEntrepriseInterface $categorieEntreprise
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setCategorieEntreprise(?ApiFrInseeSireneCategorieEntrepriseInterface $categorieEntreprise) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_categorieEntreprise = $categorieEntreprise;
		
		return $this;
	}
	
	/**
	 * Gets la catégorie d’entreprise est une variable statistique et
	 * calculée par l'Insee. Ce n’est pas une variable du répertoire
	 * Sirene.
	 * Définition de la catégorie d’entreprise :
	 *  - https://www.insee.fr/fr/metadonnees/definition/c1057
	 * Méthodologie de calcul et diffusion de la catégorie d’entreprise :
	 *  - https://www.insee.fr/fr/information/1730869.
	 * 
	 * @return ?ApiFrInseeSireneCategorieEntrepriseInterface
	 */
	public function getCategorieEntreprise() : ?ApiFrInseeSireneCategorieEntrepriseInterface
	{
		return $this->_categorieEntreprise;
	}
	
	/**
	 * Sets cette variable désigne l’année de validité correspondant à la
	 * catégorie d'entreprise diffusée.
	 * 
	 * Longueur : 4
	 * 
	 * @param ?int $anneeCategorieEntreprise
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setAnneeCategorieEntreprise(?int $anneeCategorieEntreprise) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_anneeCategorieEntreprise = $anneeCategorieEntreprise;
		
		return $this;
	}
	
	/**
	 * Gets cette variable désigne l’année de validité correspondant à la
	 * catégorie d'entreprise diffusée.
	 * 
	 * Longueur : 4
	 * 
	 * @return ?int
	 */
	public function getAnneeCategorieEntreprise() : ?int
	{
		return $this->_anneeCategorieEntreprise;
	}
	
	/**
	 * Sets date de début de la période au cours de laquelle toutes les
	 * variables historisées de l'entreprise restent inchangées.
	 * La date 1900-01-01 signifie : date non déterminée. dateDebut
	 * peut-être vide uniquement pour les unités purgées (cf. variable
	 * unitePurgeeUniteLegale). La date de début de la période la plus
	 * ancienne ne correspond pas obligatoirement à la date de création de
	 * l'entreprise, certaines variables historisées pouvant posséder des
	 * dates de début soit au 1900-01-01, soit antérieures à la date de
	 * création.
	 * 
	 * @param ?DateTimeInterface $dateDebut
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setDateDebut(?DateTimeInterface $dateDebut) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_dateDebut = $dateDebut;
		
		return $this;
	}
	
	/**
	 * Gets date de début de la période au cours de laquelle toutes les
	 * variables historisées de l'entreprise restent inchangées.
	 * La date 1900-01-01 signifie : date non déterminée. dateDebut
	 * peut-être vide uniquement pour les unités purgées (cf. variable
	 * unitePurgeeUniteLegale). La date de début de la période la plus
	 * ancienne ne correspond pas obligatoirement à la date de création de
	 * l'entreprise, certaines variables historisées pouvant posséder des
	 * dates de début soit au 1900-01-01, soit antérieures à la date de
	 * création.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateDebut() : ?DateTimeInterface
	{
		return $this->_dateDebut;
	}
	
	/**
	 * Sets le passage à l’état « Cessée » découle de la prise en
	 * compte d’une déclaration de cessation administrative. Pour les
	 * personnes morales, cela correspond au dépôt de la déclaration de
	 * disparition de la personne morale.
	 * Pour les personnes physiques, cela correspond soit à la prise en compte
	 * de la déclaration de cessation d’activité déposée par
	 * l’exploitant soit au décès de l’exploitant conformément à la
	 * réglementation.
	 * En dehors de ces cas, l’unité légale est toujours à l’état
	 * administratif « Active ». Pour les personnes morales, la cessation
	 * administrative est, en théorie, définitive, l’état administratif
	 * "Cessée" est irréversible.
	 * Cependant, il existe actuellement dans la base un certain nombre
	 * d’unités légales personnes morales avec un historique d'état
	 * présentant un état cessé entre deux périodes à l’état actif.
	 * Pour les personnes physiques, dans le cas où l’exploitant déclare la
	 * cessation de son activité, puis la reprend quelque temps plus tard, cet
	 * état est réversible. Il est donc normal d'avoir des périodes
	 * successives d'état actif puis cessé pour les personnes physiques. En
	 * règle générale, la première période d’historique d’une unité
	 * légale correspond à un etatAdministratifUniteLegale égal à « Active
	 * ». Toutefois, l'état administratif peut être à null (première date
	 * de début de l'état postérieure à la première date de début d'une
	 * autre variable historisée).
	 * 
	 * @param ?ApiFrInseeSireneEtatAdministratifInterface $etatAdministratifUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setEtatAdministratifUniteLegale(?ApiFrInseeSireneEtatAdministratifInterface $etatAdministratifUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_etatAdministratifUniteLegale = $etatAdministratifUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets le passage à l’état « Cessée » découle de la prise en
	 * compte d’une déclaration de cessation administrative. Pour les
	 * personnes morales, cela correspond au dépôt de la déclaration de
	 * disparition de la personne morale.
	 * Pour les personnes physiques, cela correspond soit à la prise en compte
	 * de la déclaration de cessation d’activité déposée par
	 * l’exploitant soit au décès de l’exploitant conformément à la
	 * réglementation.
	 * En dehors de ces cas, l’unité légale est toujours à l’état
	 * administratif « Active ». Pour les personnes morales, la cessation
	 * administrative est, en théorie, définitive, l’état administratif
	 * "Cessée" est irréversible.
	 * Cependant, il existe actuellement dans la base un certain nombre
	 * d’unités légales personnes morales avec un historique d'état
	 * présentant un état cessé entre deux périodes à l’état actif.
	 * Pour les personnes physiques, dans le cas où l’exploitant déclare la
	 * cessation de son activité, puis la reprend quelque temps plus tard, cet
	 * état est réversible. Il est donc normal d'avoir des périodes
	 * successives d'état actif puis cessé pour les personnes physiques. En
	 * règle générale, la première période d’historique d’une unité
	 * légale correspond à un etatAdministratifUniteLegale égal à « Active
	 * ». Toutefois, l'état administratif peut être à null (première date
	 * de début de l'état postérieure à la première date de début d'une
	 * autre variable historisée).
	 * 
	 * @return ?ApiFrInseeSireneEtatAdministratifInterface
	 */
	public function getEtatAdministratifUniteLegale() : ?ApiFrInseeSireneEtatAdministratifInterface
	{
		return $this->_etatAdministratifUniteLegale;
	}
	
	/**
	 * Sets cette variable indique le libellé le nom de naissance pour une
	 * personne physique. Cette variable est à null pour les personnes
	 * morales. Le répertoire Sirene gère des caractères majuscules non
	 * accentués et les seuls caractères spéciaux tiret (-) et apostrophe.
	 * Le nom peut être à null (cas des unités purgées, première date de
	 * début du nom postérieure à la première date de début d'une autre
	 * variable historisée).
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $nomUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setNomUniteLegale(?string $nomUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_nomUniteLegale = $nomUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique le libellé le nom de naissance pour une
	 * personne physique. Cette variable est à null pour les personnes
	 * morales. Le répertoire Sirene gère des caractères majuscules non
	 * accentués et les seuls caractères spéciaux tiret (-) et apostrophe.
	 * Le nom peut être à null (cas des unités purgées, première date de
	 * début du nom postérieure à la première date de début d'une autre
	 * variable historisée).
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getNomUniteLegale() : ?string
	{
		return $this->_nomUniteLegale;
	}
	
	/**
	 * Sets le nom d’usage est celui que la personne physique a choisi
	 * d’utiliser.
	 * Cette variable est à null pour les personnes morales. Elle peut être
	 * également à null pour les personnes physiques.
	 * Le répertoire Sirene gère des caractères majuscules non accentués et
	 * les seuls caractères spéciaux tiret (-) et apostrophe.
	 * Cette variable est historisée.
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $nomUsageUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setNomUsageUniteLegale(?string $nomUsageUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_nomUsageUniteLegale = $nomUsageUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets le nom d’usage est celui que la personne physique a choisi
	 * d’utiliser.
	 * Cette variable est à null pour les personnes morales. Elle peut être
	 * également à null pour les personnes physiques.
	 * Le répertoire Sirene gère des caractères majuscules non accentués et
	 * les seuls caractères spéciaux tiret (-) et apostrophe.
	 * Cette variable est historisée.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getNomUsageUniteLegale() : ?string
	{
		return $this->_nomUsageUniteLegale;
	}
	
	/**
	 * Sets cette variable désigne la raison sociale pour les personnes
	 * morales. Il s'agit du nom sous lequel est déclarée l'unité légale.
	 * Cette variable est à null pour les personnes physiques.
	 * Le répertoire Sirene gère des caractères majuscules non accentués
	 * avec caractères spéciaux (- & + @ ! ? * ° . % : € #).
	 * La dénomination peut parfois contenir la mention de la forme de la
	 * société (SA, SAS, SARL, etc.).
	 * 
	 * *Historique*
	 * Un travail de reconstitution des historiques de dénomination a été
	 * entrepris en 2005. Toutefois, avant cette date, des historiques de
	 * dénomination peuvent s’avérer incomplets. La dénomination peut
	 * être à null (cas des unités purgées, première date de début de la
	 * dénomination postérieure à la première date de début d'une autre
	 * variable historisée).
	 * 
	 * Longueur : 120
	 * 
	 * @param ?string $denominationUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setDenominationUniteLegale(?string $denominationUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_denominationUniteLegale = $denominationUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets cette variable désigne la raison sociale pour les personnes
	 * morales. Il s'agit du nom sous lequel est déclarée l'unité légale.
	 * Cette variable est à null pour les personnes physiques.
	 * Le répertoire Sirene gère des caractères majuscules non accentués
	 * avec caractères spéciaux (- & + @ ! ? * ° . % : € #).
	 * La dénomination peut parfois contenir la mention de la forme de la
	 * société (SA, SAS, SARL, etc.).
	 * 
	 * *Historique*
	 * Un travail de reconstitution des historiques de dénomination a été
	 * entrepris en 2005. Toutefois, avant cette date, des historiques de
	 * dénomination peuvent s’avérer incomplets. La dénomination peut
	 * être à null (cas des unités purgées, première date de début de la
	 * dénomination postérieure à la première date de début d'une autre
	 * variable historisée).
	 * 
	 * Longueur : 120
	 * 
	 * @return ?string
	 */
	public function getDenominationUniteLegale() : ?string
	{
		return $this->_denominationUniteLegale;
	}
	
	/**
	 * Sets cette variable désigne le nom (ou les noms) sous lequel
	 * l'entreprise est connue du grand public. Cet élément d'identification
	 * de l'entreprise (sur trois champs : denominationUsuelle1UniteLegale,
	 * denominationUsuelle2UniteLegale et denominationUsuelle3UniteLegale) a
	 * été enregistré au niveau unité légale avant l'application de la
	 * norme d'échanges CFE de 2008. À partir de la norme 2008, la
	 * dénomination usuelle est enregistrée au niveau de l'établissement sur
	 * un seul champ : denominationUsuelleEtablissement.
	 * Variables historisées avec une seule indicatrice de changement pour les
	 * trois variables.
	 * 
	 * Longueur : 70
	 * 
	 * @param ?string $denominationUsuelle1UniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setDenominationUsuelle1UniteLegale(?string $denominationUsuelle1UniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_denominationUsuelle1UniteLegale = $denominationUsuelle1UniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets cette variable désigne le nom (ou les noms) sous lequel
	 * l'entreprise est connue du grand public. Cet élément d'identification
	 * de l'entreprise (sur trois champs : denominationUsuelle1UniteLegale,
	 * denominationUsuelle2UniteLegale et denominationUsuelle3UniteLegale) a
	 * été enregistré au niveau unité légale avant l'application de la
	 * norme d'échanges CFE de 2008. À partir de la norme 2008, la
	 * dénomination usuelle est enregistrée au niveau de l'établissement sur
	 * un seul champ : denominationUsuelleEtablissement.
	 * Variables historisées avec une seule indicatrice de changement pour les
	 * trois variables.
	 * 
	 * Longueur : 70
	 * 
	 * @return ?string
	 */
	public function getDenominationUsuelle1UniteLegale() : ?string
	{
		return $this->_denominationUsuelle1UniteLegale;
	}
	
	/**
	 * Sets second champ de la dénomination usuelle.
	 * 
	 * @param ?string $denominationUsuelle2UniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setDenominationUsuelle2UniteLegale(?string $denominationUsuelle2UniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_denominationUsuelle2UniteLegale = $denominationUsuelle2UniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets second champ de la dénomination usuelle.
	 * 
	 * @return ?string
	 */
	public function getDenominationUsuelle2UniteLegale() : ?string
	{
		return $this->_denominationUsuelle2UniteLegale;
	}
	
	/**
	 * Sets dernier champ de la dénomination usuelle.
	 * 
	 * @param ?string $denominationUsuelle3UniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setDenominationUsuelle3UniteLegale(?string $denominationUsuelle3UniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_denominationUsuelle3UniteLegale = $denominationUsuelle3UniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets dernier champ de la dénomination usuelle.
	 * 
	 * @return ?string
	 */
	public function getDenominationUsuelle3UniteLegale() : ?string
	{
		return $this->_denominationUsuelle3UniteLegale;
	}
	
	/**
	 * Sets la catégorie juridique est un attribut des unités légales.
	 * Cette variable est à 1000 pour les personnes physiques.
	 * Lors de son dépôt de demande de création, le déclarant indique la
	 * forme juridique de l’unité légale qu’il crée, qui est ensuite
	 * traduite en code.
	 * Ce code est modifiable, à la marge, au cours de la vie de l’unité
	 * légale (pour les personnes morales) en fonction des déclarations de
	 * l’exploitant. Pour chaque unité légale, il existe à un instant
	 * donné un seul code Catégorie juridique. Il est attribué selon la
	 * nomenclature en vigueur.
	 * 
	 * *Historique*
	 * La catégorie juridique peut être à null (cas des unités purgées,
	 * première date de début de la catégorie juridique postérieure à la
	 * première date de début d'une autre variable historisée). En revanche,
	 * le libellé associé aux catégories juridiques n’a pas été
	 * historisé, si bien que des codes peuvent paraître « Hors nomenclature
	 * », alors qu’ils ont été valides à un instant donné.
	 * 
	 * Longueur : 4
	 * 
	 * @param ?string $categorieJuridiqueUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setCategorieJuridiqueUniteLegale(?string $categorieJuridiqueUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_categorieJuridiqueUniteLegale = $categorieJuridiqueUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets la catégorie juridique est un attribut des unités légales.
	 * Cette variable est à 1000 pour les personnes physiques.
	 * Lors de son dépôt de demande de création, le déclarant indique la
	 * forme juridique de l’unité légale qu’il crée, qui est ensuite
	 * traduite en code.
	 * Ce code est modifiable, à la marge, au cours de la vie de l’unité
	 * légale (pour les personnes morales) en fonction des déclarations de
	 * l’exploitant. Pour chaque unité légale, il existe à un instant
	 * donné un seul code Catégorie juridique. Il est attribué selon la
	 * nomenclature en vigueur.
	 * 
	 * *Historique*
	 * La catégorie juridique peut être à null (cas des unités purgées,
	 * première date de début de la catégorie juridique postérieure à la
	 * première date de début d'une autre variable historisée). En revanche,
	 * le libellé associé aux catégories juridiques n’a pas été
	 * historisé, si bien que des codes peuvent paraître « Hors nomenclature
	 * », alors qu’ils ont été valides à un instant donné.
	 * 
	 * Longueur : 4
	 * 
	 * @return ?string
	 */
	public function getCategorieJuridiqueUniteLegale() : ?string
	{
		return $this->_categorieJuridiqueUniteLegale;
	}
	
	/**
	 * Sets lors de son inscription au répertoire, l’Insee attribue à toute
	 * unité légale un code dit « APE » sur la base de la description de
	 * l’activité principale faite par le déclarant. Ce code est modifiable
	 * au cours de la vie de l’unité légale en fonction des déclarations
	 * de l’exploitant.
	 * Pour chaque unité légale, il existe à un instant donné un seul code
	 * « APE ». Il est attribué selon la nomenclature en vigueur. La
	 * nomenclature en vigueur est la Naf Rév2 et ce depuis le 1 er Janvier
	 * 2008.
	 * Chaque code comporte 2 chiffres, un point, 2 chiffres et une lettre.
	 * Toutes les unités légales actives au 01/01/2008 ont eu leur code APE
	 * recodé dans la nouvelle nomenclature, ainsi de très nombreuses
	 * entreprises ont une période débutant à cette date. Au moment de la
	 * déclaration de l’entreprise, il peut arriver que l’Insee ne soit
	 * pas en mesure d’attribuer le bon code APE : la modalité 00.00Z peut
	 * alors être affectée provisoirement.
	 * 
	 * *Historique*
	 * Le code APE est historisé depuis le 01/01/2005.
	 * La règle d’historisation des données d’activité est la suivante :
	 *  - Pour les entreprises cessées avant le 31/12/2004, seul le dernier
	 * code activité connu figure, dans la nomenclature en vigueur à la date
	 * de fermeture.
	 *  - Pour les entreprises actives après le 01/01/2005 et cessées avant
	 * le 31/12/2007, l’historique des codes attribués sur la période est
	 * disponible.
	 *  - Pour les entreprises actives après le 01/01/2005 et toujours actives
	 * le 1/1/2008, l’historique intègre le changement de nomenclature.
	 *  - Pour les entreprises créées après le 01/01/2008, l’historique
	 * comprend les modifications apportées au cours de la vie de
	 * l’entreprise.
	 * 
	 * L'APE peut être à null (cas des unités purgées - pour des raisons de
	 * capacité de stockage des données, les données concernant les
	 * entreprises cessées avant le 31/12/2002 ont été purgées -, première
	 * date de début de l'APE postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * Longueur : 6
	 * 
	 * @param ?string $activitePrincipaleUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setActivitePrincipaleUniteLegale(?string $activitePrincipaleUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_activitePrincipaleUniteLegale = $activitePrincipaleUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets lors de son inscription au répertoire, l’Insee attribue à toute
	 * unité légale un code dit « APE » sur la base de la description de
	 * l’activité principale faite par le déclarant. Ce code est modifiable
	 * au cours de la vie de l’unité légale en fonction des déclarations
	 * de l’exploitant.
	 * Pour chaque unité légale, il existe à un instant donné un seul code
	 * « APE ». Il est attribué selon la nomenclature en vigueur. La
	 * nomenclature en vigueur est la Naf Rév2 et ce depuis le 1 er Janvier
	 * 2008.
	 * Chaque code comporte 2 chiffres, un point, 2 chiffres et une lettre.
	 * Toutes les unités légales actives au 01/01/2008 ont eu leur code APE
	 * recodé dans la nouvelle nomenclature, ainsi de très nombreuses
	 * entreprises ont une période débutant à cette date. Au moment de la
	 * déclaration de l’entreprise, il peut arriver que l’Insee ne soit
	 * pas en mesure d’attribuer le bon code APE : la modalité 00.00Z peut
	 * alors être affectée provisoirement.
	 * 
	 * *Historique*
	 * Le code APE est historisé depuis le 01/01/2005.
	 * La règle d’historisation des données d’activité est la suivante :
	 *  - Pour les entreprises cessées avant le 31/12/2004, seul le dernier
	 * code activité connu figure, dans la nomenclature en vigueur à la date
	 * de fermeture.
	 *  - Pour les entreprises actives après le 01/01/2005 et cessées avant
	 * le 31/12/2007, l’historique des codes attribués sur la période est
	 * disponible.
	 *  - Pour les entreprises actives après le 01/01/2005 et toujours actives
	 * le 1/1/2008, l’historique intègre le changement de nomenclature.
	 *  - Pour les entreprises créées après le 01/01/2008, l’historique
	 * comprend les modifications apportées au cours de la vie de
	 * l’entreprise.
	 * 
	 * L'APE peut être à null (cas des unités purgées - pour des raisons de
	 * capacité de stockage des données, les données concernant les
	 * entreprises cessées avant le 31/12/2002 ont été purgées -, première
	 * date de début de l'APE postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * Longueur : 6
	 * 
	 * @return ?string
	 */
	public function getActivitePrincipaleUniteLegale() : ?string
	{
		return $this->_activitePrincipaleUniteLegale;
	}
	
	/**
	 * Sets cette variable indique la nomenclature d’activité correspondant
	 * à la variable activitePrincipaleUniteLegale (cf.
	 * activitePrincipaleUniteLegale).
	 * 
	 * @param ?ApiFrInseeSireneNomenclatureApeInterface $nomenclatureActivitePrincipaleUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setNomenclatureActivitePrincipaleUniteLegale(?ApiFrInseeSireneNomenclatureApeInterface $nomenclatureActivitePrincipaleUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_nomenclatureActivitePrincipaleUniteLegale = $nomenclatureActivitePrincipaleUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique la nomenclature d’activité correspondant
	 * à la variable activitePrincipaleUniteLegale (cf.
	 * activitePrincipaleUniteLegale).
	 * 
	 * @return ?ApiFrInseeSireneNomenclatureApeInterface
	 */
	public function getNomenclatureActivitePrincipaleUniteLegale() : ?ApiFrInseeSireneNomenclatureApeInterface
	{
		return $this->_nomenclatureActivitePrincipaleUniteLegale;
	}
	
	/**
	 * Sets le siège d’une unité légale est le lieu où sont centralisées
	 * l’administration et la direction effective de l’unité légale.
	 * À un instant donné, chaque unité légale a un seul siège. Mais, au
	 * cours de la vie de l’unité légale, le siège peut être différent.
	 * Chaque siège est identifié par un numéro Nic (Numéro Interne de
	 * Classement de l'établissement) qui respecte les règles d’attribution
	 * des numéros d’établissement.
	 * Le Nic est composé de quatre chiffres et d'un cinquième qui permet de
	 * contrôler la validité du numéro Siret (concaténation du numéro
	 * Siren et du Nic).
	 * 
	 * *Historique*
	 * Le Nic du siège peut être à null sur une période mais, en règle
	 * générale, pas sur l’ensemble de l’historique (cas des unités
	 * purgées, première date de début du Nic postérieure à la première
	 * date de début d'une autre variable historisée).
	 * 
	 * Longueur : 5
	 * 
	 * @param ?string $nicSiegeUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setNicSiegeUniteLegale(?string $nicSiegeUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_nicSiegeUniteLegale = $nicSiegeUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets le siège d’une unité légale est le lieu où sont centralisées
	 * l’administration et la direction effective de l’unité légale.
	 * À un instant donné, chaque unité légale a un seul siège. Mais, au
	 * cours de la vie de l’unité légale, le siège peut être différent.
	 * Chaque siège est identifié par un numéro Nic (Numéro Interne de
	 * Classement de l'établissement) qui respecte les règles d’attribution
	 * des numéros d’établissement.
	 * Le Nic est composé de quatre chiffres et d'un cinquième qui permet de
	 * contrôler la validité du numéro Siret (concaténation du numéro
	 * Siren et du Nic).
	 * 
	 * *Historique*
	 * Le Nic du siège peut être à null sur une période mais, en règle
	 * générale, pas sur l’ensemble de l’historique (cas des unités
	 * purgées, première date de début du Nic postérieure à la première
	 * date de début d'une autre variable historisée).
	 * 
	 * Longueur : 5
	 * 
	 * @return ?string
	 */
	public function getNicSiegeUniteLegale() : ?string
	{
		return $this->_nicSiegeUniteLegale;
	}
	
	/**
	 * Sets cette variable indique si l'entreprise appartient au champ de
	 * l’économie sociale et solidaire. La loi n° 2014-856 du 31 juillet
	 * 2014 définit officiellement le périmètre de l’économie sociale et
	 * solidaire (ESS).
	 * Celle-ci comprend les quatre familles traditionnelles en raison de leur
	 * régime juridique (associations, fondations, coopératives et mutuelles)
	 * et inclut une nouvelle catégorie, les entreprises de l’ESS, adhérant
	 * aux mêmes principes :
	 *  - poursuivre un but social autre que le seul partage des bénéfices ;
	 *  - un caractère lucratif encadré (notamment des bénéfices
	 * majoritairement consacrés au maintien et au développement de
	 * l’activité) ;
	 *  - une gouvernance démocratique et participative.
	 * Cette variable est historisée, renseignée pour environ 1 million
	 * d'entreprises, sinon null.
	 * 
	 * @param ?bool $economieSocialeSolidaireUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setEconomieSocialeSolidaireUniteLegale(?bool $economieSocialeSolidaireUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_economieSocialeSolidaireUniteLegale = $economieSocialeSolidaireUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique si l'entreprise appartient au champ de
	 * l’économie sociale et solidaire. La loi n° 2014-856 du 31 juillet
	 * 2014 définit officiellement le périmètre de l’économie sociale et
	 * solidaire (ESS).
	 * Celle-ci comprend les quatre familles traditionnelles en raison de leur
	 * régime juridique (associations, fondations, coopératives et mutuelles)
	 * et inclut une nouvelle catégorie, les entreprises de l’ESS, adhérant
	 * aux mêmes principes :
	 *  - poursuivre un but social autre que le seul partage des bénéfices ;
	 *  - un caractère lucratif encadré (notamment des bénéfices
	 * majoritairement consacrés au maintien et au développement de
	 * l’activité) ;
	 *  - une gouvernance démocratique et participative.
	 * Cette variable est historisée, renseignée pour environ 1 million
	 * d'entreprises, sinon null.
	 * 
	 * @return ?bool
	 */
	public function hasEconomieSocialeSolidaireUniteLegale() : ?bool
	{
		return $this->_economieSocialeSolidaireUniteLegale;
	}
	
	/**
	 * Sets si l'unité légale est une société à mission.
	 * Cette variable est historisée.
	 * 
	 * @param ?bool $societeMissionUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setSocieteMissionUniteLegale(?bool $societeMissionUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_societeMissionUniteLegale = $societeMissionUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets si l'unité légale est une société à mission.
	 * Cette variable est historisée.
	 * 
	 * @return ?bool
	 */
	public function hasSocieteMissionUniteLegale() : ?bool
	{
		return $this->_societeMissionUniteLegale;
	}
	
	/**
	 * Sets lors de sa formalité de création, le déclarant indique si
	 * l’unité légale aura ou non des employés. Par la suite, le
	 * déclarant peut également faire des déclarations de prise d’emploi
	 * et de fin d’emploi. La prise en compte d’une déclaration de prise
	 * d’emploi bascule immédiatement l’unité légale en « Employeuse
	 * ». Inversement, lorsqu’une déclaration de fin d’emploi est
	 * traitée, l’unité légale devient « Non employeuse ». Le caractère
	 * employeur est O si au moins l'un des établissements actifs de l'unité
	 * légale emploie des salariés.
	 * Cette variable est historisée.
	 * 
	 * @param ?bool $caractereEmployeurUniteLegale
	 * @return ApiFrInseeSireneUniteLegaleInterface
	 */
	public function setCaractereEmployeurUniteLegale(?bool $caractereEmployeurUniteLegale) : ApiFrInseeSireneUniteLegaleInterface
	{
		$this->_caractereEmployeurUniteLegale = $caractereEmployeurUniteLegale;
		
		return $this;
	}
	
	/**
	 * Gets lors de sa formalité de création, le déclarant indique si
	 * l’unité légale aura ou non des employés. Par la suite, le
	 * déclarant peut également faire des déclarations de prise d’emploi
	 * et de fin d’emploi. La prise en compte d’une déclaration de prise
	 * d’emploi bascule immédiatement l’unité légale en « Employeuse
	 * ». Inversement, lorsqu’une déclaration de fin d’emploi est
	 * traitée, l’unité légale devient « Non employeuse ». Le caractère
	 * employeur est O si au moins l'un des établissements actifs de l'unité
	 * légale emploie des salariés.
	 * Cette variable est historisée.
	 * 
	 * @return ?bool
	 */
	public function hasCaractereEmployeurUniteLegale() : ?bool
	{
		return $this->_caractereEmployeurUniteLegale;
	}
	
}
