<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use DateTimeInterface;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoieInterface;

/**
 * ApiFrInseeSireneEtablissement class file.
 * 
 * This is a simple implementation of the
 * ApiFrInseeSireneEtablissementInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.ExcessiveClassLength")
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.LongVariable")
 * @SuppressWarnings("PHPMD.TooManyFields")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class ApiFrInseeSireneEtablissement implements ApiFrInseeSireneEtablissementInterface
{
	
	/**
	 * Un numéro d’identité de l’unité légale est attribué par
	 * l’Insee à toutes les personnes physiques ou morales inscrites au
	 * répertoire ainsi qu’à leurs établissements : le numéro Siren. Ce
	 * numéro unique est « attribué soit à l’occasion des demandes
	 * d’immatriculation au registre du commerce et des sociétés ou des
	 * déclarations effectuées au répertoire des métiers, soit à la
	 * demande d’administrations » (article R123-224 du code de  commerce).
	 * Lors de sa création, une unité légale se voit attribuer un numéro
	 * Siren de 9 chiffres.
	 * 
	 * *Règles de gestion*
	 * Les entrepreneurs individuels, ou personnes physiques, conservent le
	 * même numéro Siren jusqu’à leur décès. Les sociétés, ou
	 * personnes morales, perdent la personnalité juridique au moment de la
	 * cessation de l’activité de l’entreprise. Si l’activité devait
	 * reprendre ultérieurement, un nouveau numéro Siren sera attribué. Les
	 * numéros d’identification sont uniques : lorsqu’un numéro Siren a
	 * été attribué, il ne peut pas être réutilisé et attribué à une
	 * nouvelle unité légale, même lorsque l’activité a cessé.
	 * 
	 * *Historique*
	 * Même si la mise en place du répertoire Sirene remonte à 1973, toutes
	 * les unités légales, y compris celles créées avant cette date,
	 * disposent d’un numéro Siren pour le secteur privé non agricole.
	 * En 1983, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus aux institutions et services de
	 * l’État et aux collectivités territoriales.
	 * En 1993, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus au secteur privé agricole.
	 * 
	 * Longueur : 9
	 * 
	 * @var string
	 */
	protected string $_siren;
	
	/**
	 * Le numéro interne de classement permet de distinguer les
	 * établissements d'une même entreprise. Il est composé de 5 chiffres.
	 * Associé au Siren, il forme le Siret de l'établissement. Il est
	 * composé de quatre chiffres et d'un cinquième qui permet de contrôler
	 * la validité du numéro Siret.
	 * Le Nic est attribué une seule fois au sein de l'entreprise. Si
	 * l'établissement ferme, son Nic ne peut être réattribué à un nouvel
	 * établissement.
	 * 
	 * Longueur : 5
	 * 
	 * @var string
	 */
	protected string $_nic;
	
	/**
	 * Le numéro Siret est le numéro unique d’identification attribué à
	 * chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * Une entreprise est constituée d’autant d’établissements qu’il y
	 * a de lieux différents où elle exerce son activité. L’établissement
	 * est fermé quand l’activité cesse dans l’établissement concerné
	 * ou lorsque l’établissement change d’adresse.
	 * 
	 * Longueur : 14
	 * 
	 * @var string
	 */
	protected string $_siret;
	
	/**
	 * Seuls les établissements diffusibles sont accessibles à tout public
	 * (statutDiffusionEtablissement=O).
	 * 
	 * @var bool
	 */
	protected bool $_statutDiffusionEtablissement;
	
	/**
	 * La date de création correspond à la date qui figure dans les statuts
	 * de l’entreprise déposés au CFE compétent.
	 * Pour les établissements des unités purgées
	 * (unitePurgeeUniteLegale=true): si la date de création est au 01/01/1900
	 * dans Sirene, la date est forcée à null.
	 * Dans tous les autres cas, la date de création n'est jamais à null. Si
	 * elle est non renseignée, elle sera au 01/01/1900.
	 * La date de création ne correspond pas obligatoirement à dateDebut de
	 * la première période de l'établissement, certaines variables
	 * historisées pouvant posséder des dates de début soit au 1900-01-01,
	 * soit antérieures à la date de création.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_dateCreationEtablissement = null;
	
	/**
	 * Il s’agit d’une variable statistique, millésimée au 31/12 d’une
	 * année donnée (voir variable anneeEffectifsEtablissement).
	 * 
	 * @var ?ApiFrInseeSireneTrancheEffectifsInterface
	 */
	protected ?ApiFrInseeSireneTrancheEffectifsInterface $_trancheEffectifsEtablissement = null;
	
	/**
	 * Seule la dernière année de mise à jour de l’effectif salarié de
	 * l’établissement est donnée si celle-ci est inférieure ou égale à
	 * l’année d’interrogation-3. Ainsi une interrogation en 2018 ne
	 * renverra la dernière année de mise à jour de l’effectif que si
	 * cette année est supérieure ou égale à 2015.
	 * 
	 * @var ?int
	 */
	protected ?int $_anneeEffectifsEtablissement = null;
	
	/**
	 * Cette variable désigne le code de l'activité exercée par l'artisan
	 * inscrit au registre des métiers selon la Nomenclature d'Activités
	 * Française de l'Artisanat (NAFA).
	 * 
	 * Longueur : 6
	 * 
	 * @var ?string
	 */
	protected ?string $_activitePrincipaleRegistreMetiersEtablissement = null;
	
	/**
	 * Cette date peut concerner des mises à jour de données du répertoire
	 * Sirene qui ne sont pas diffusées par API Sirene.
	 * Cette variable peut être à null.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_dateDernierTraitementEtablissement = null;
	
	/**
	 * C’est une variable booléenne qui indique si l'établissement est le
	 * siège ou non de l'unité légale. Variable calculée toujours
	 * renseignée.
	 * 
	 * @var bool
	 */
	protected bool $_etablissementSiege;
	
	/**
	 * Cette variable donne le nombre de périodes [dateDebut,dateFin] de
	 * l’établissement. Chaque période correspond à l’intervalle de
	 * temps pendant lequel la totalité des variables historisées de
	 * l’établissement n’ont pas été modifiées.
	 * Les dates de ces périodes sont des dates d’effet (et non des dates de
	 * traitement).
	 * 
	 * @var ?int
	 */
	protected ?int $_nombrePeriodesEtablissement = null;
	
	/**
	 * Cette variable est un élément constitutif de l’adresse.
	 * C'est une variable facultative qui précise l'adresse avec :
	 *  - une indication d'étage, d'appartement, de porte, de N° de boîte à
	 * lettres,
	 *  - la désignation d'un bâtiment, d'un escalier, d'une entrée, d'un
	 * bloc,
	 *  - le nom d'une résidence, d'un ensemble….
	 * 
	 * Longueur : 38
	 * 
	 * @var ?string
	 */
	protected ?string $_complementAdresseEtablissement = null;
	
	/**
	 * C'est un élément constitutif de l’adresse.
	 * Cette variable est facultative.
	 * Si plusieurs numéros de voie sont indiqués (5-7, 5 à 7…),
	 * l'information complète (5-7) ou (5 à 7) figure en complément
	 * d'adresse et le premier des numéros (5 dans l'exemple) est porté dans
	 * la variable numeroVoieEtablissement.
	 * 
	 * @var ?int
	 */
	protected ?int $_numeroVoieEtablissement = null;
	
	/**
	 * Cette variable un élément constitutif de l’adresse.
	 * Cette variable est facultative.
	 * Elle doit être associée à la variable numeroVoieEtablissement.
	 * 
	 * Longueur : 1
	 * 
	 * @var ?string
	 */
	protected ?string $_indiceRepetitionEtablissement = null;
	
	/**
	 * Le numéro de voie de l'adresse précédente, s'il y en a une..
	 * 
	 * @var ?int
	 */
	protected ?int $_dernierNumeroVoieEtablissement = null;
	
	/**
	 * L'indice de répétition de l'adresse précédente, s'il y en a une.
	 * 
	 * @var ?string
	 */
	protected ?string $_indiceRepetitionDernierNumeroVoieEtablissement = null;
	
	/**
	 * C'est un élément constitutif de l’adresse.
	 * 
	 * Si le type de voie est d'une longueur inférieure ou égale à 4
	 * caractères, le type de voie n'est pas abrégé. Ainsi, RUE ou QUAI sont
	 * écrits tels quels, alors que AVENUE est abrégée en AV.
	 * 
	 * @var ?ApiFrInseeBanTypeVoieInterface
	 */
	protected ?ApiFrInseeBanTypeVoieInterface $_typeVoieEtablissement = null;
	
	/**
	 * Cette variable indique le libellé de voie de la commune de localisation
	 * de l'établissement. 
	 * C’est un élément constitutif de l’adresse.
	 * Cette variable est facultative : elle n'est pas toujours renseignée, en
	 * particulier dans les petites communes.
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_libelleVoieEtablissement = null;
	
	/**
	 * Cette variable désigne le code postal de l’adresse de
	 * l’établissement.
	 * Pour les adresses à l’étranger (codeCommuneEtablissement commence
	 * par 99), cette variable est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @var ?string
	 */
	protected ?string $_codePostalEtablissement = null;
	
	/**
	 * Cette variable indique le libellé de la commune de localisation de
	 * l'établissement si celui-ci n’est pas à l’étranger. C’est un
	 * élément constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_libelleCommuneEtablissement = null;
	
	/**
	 * Cette variable est un élément constitutif de l’adresse pour les
	 * établissements situés sur le territoire étranger (la variable
	 * codeCommuneEtablissement est vide).
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_libelleCommuneEtrangerEtablissement = null;
	
	/**
	 * La distribution spéciale reprend les éléments particuliers qui
	 * accompagnent une adresse de distribution spéciale. C’est un élément
	 * constitutif de l’adresse.
	 * Cette variable est facultative.
	 * 
	 * Longueur 26
	 * 
	 * *Exemples de début de modalités attendues*
	 * BP : Boîte postale
	 * TSA : Tri par service à l'arrivée
	 * LP : Local postal
	 * RP : Référence postale
	 * SP : Secteur postal
	 * CP : Case postale
	 * CE : Case entreprise
	 * CS : Course spéciale
	 * POSTE RESTANTE
	 * CEDEX : Courrier d'entreprise à distribution exceptionnelle
	 * CIDEX : Courrier individuel à distribution exceptionnelle
	 * CASE, NIVEAU : Mots utilisés pour la distribution interne du courrier
	 * à LA DEFENSE
	 * CASIER : Utilisé pour le centre commercial des Quatre Temps
	 * SILIC, SENIA, MAREE, FLEURS… : Utilisés sur le site de Rungis
	 * 
	 * @var ?string
	 */
	protected ?string $_distributionSpecialeEtablissement = null;
	
	/**
	 * Cette variable désigne le code de la commune de localisation de
	 * l'établissement, hors adresse à l'étranger.
	 * Le code commune correspond au code commune existant à la date de la
	 * mise à disposition : toute modification du code officiel géographique
	 * est répercutée sur la totalité des établissements (même ceux
	 * fermés) correspondant à ce code commune.
	 * 
	 * Pour les établissements localisés à l'étranger, la variable
	 * codeCommuneEtablissement est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @var ?string
	 */
	protected ?string $_codeCommuneEtablissement = null;
	
	/**
	 * Cette variable est un élément constitutif de l’adresse.
	 * Elle est facultative.
	 * 
	 * Longueur : 9
	 * 
	 * @var ?string
	 */
	protected ?string $_codeCedexEtablissement = null;
	
	/**
	 * Cette variable indique le libellé correspondant au code cedex de
	 * l'établissement si celui-ci est non null. Ce libellé est le libellé
	 * utilisé dans la ligne 6 d’adresse pour l’acheminement postal.
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_libelleCedexEtablissement = null;
	
	/**
	 * Cette variable désigne le code du pays de localisation de
	 * l'établissement pour les adresses à l'étranger. La variable
	 * codePaysEtrangerEtablissement commence toujours par 99 si elle est
	 * renseignée. Les 3 caractères suivants sont le code du pays étranger.
	 * 
	 * Longueur : 5
	 * 
	 * @var ?string
	 */
	protected ?string $_codePaysEtrangerEtablissement = null;
	
	/**
	 * Cette variable indique le libellé du pays de localisation de
	 * l'établissement si celui-ci est à l’étranger. C’est un élément
	 * constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_libellePaysEtrangerEtablissement = null;
	
	/**
	 * Identifiant de l'adresse
	 * Longueur : 15.
	 * 
	 * @var ?string
	 */
	protected ?string $_identifiantAdresseEtablissement = null;
	
	/**
	 * Abscisse des coordonnees Lambert de l'adresse.
	 * 
	 * @var ?float
	 */
	protected ?float $_coordonneeLambertAbscisseEtablissement = null;
	
	/**
	 * Ordonnée des coordonnees Lambert de l'adresse.
	 * 
	 * @var ?float
	 */
	protected ?float $_coordonneeLambertOrdonneeEtablissement = null;
	
	/**
	 * Dans le cas où l’établissement dispose d’une entrée secondaire,
	 * cette variable est un élément constitutif de l’adresse secondaire.
	 * C'est une variable facultative qui précise l'adresse secondaire avec :
	 *  - une indication d'étage, d'appartement, de porte, de N° de boîte à
	 * lettres,
	 *  - la désignation d'un bâtiment, d'un escalier, d'une entrée, d'un
	 * bloc,
	 *  - le nom d'une résidence, d'un ensemble….
	 * 
	 * Longueur : 38
	 * 
	 * @var ?string
	 */
	protected ?string $_complementAdresse2Etablissement = null;
	
	/**
	 * C'est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative.
	 * Si plusieurs numéros de voie sont indiqués (5-7, 5 à 7…),
	 * l'information complète (5-7) ou (5 à 7) figure en complément
	 * d'adresse secondaire et le premier des numéros (5 dans l'exemple) est
	 * porté dans la variable numeroVoie2Etablissement.
	 * 
	 * @var ?int
	 */
	protected ?int $_numeroVoie2Etablissement = null;
	
	/**
	 * Cette variable un élément constitutif de l’adresse secondaire. Cette
	 * variable est facultative. Elle doit être associée à la variable
	 * numeroVoie2Etablissement.
	 * 
	 * Longueur : 1
	 * 
	 * @var ?string
	 */
	protected ?string $_indiceRepetition2Etablissement = null;
	
	/**
	 * Cette variable est un élément constitutif de l’adresse secondaire.
	 * 
	 * Si le type de voie est d'une longueur inférieure ou égale à 4
	 * caractères, le type de voie n'est pas abrégé. Ainsi, RUE ou QUAI sont
	 * écrits tels quels, alors que AVENUE est abrégée en AV.
	 * 
	 * @var ?ApiFrInseeBanTypeVoieInterface
	 */
	protected ?ApiFrInseeBanTypeVoieInterface $_typeVoie2Etablissement = null;
	
	/**
	 * Cette variable indique le libellé de la voie de l’adresse secondaire
	 * de l'établissement.
	 * C’est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative : elle n'est pas toujours renseignée, en
	 * particulier dans les petites communes.
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_libelleVoie2Etablissement = null;
	
	/**
	 * Cette variable désigne le code postal de l’adresse de
	 * l’établissement.
	 * Pour les adresses à l’étranger (codeCommuneEtablissement commence
	 * par 99), cette variable est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @var ?string
	 */
	protected ?string $_codePostal2Etablissement = null;
	
	/**
	 * Cette variable indique le libellé de la commune de l’adresse
	 * secondaire de l'établissement si celui-ci n’est pas à l’étranger.
	 * C’est un élément constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_libelleCommune2Etablissement = null;
	
	/**
	 * Dans le cas où l’établissement dispose d’une entrée secondaire,
	 * cette variable est un élément constitutif de l’adresse secondaire
	 * pour les établissements situés sur le territoire étranger (la
	 * variable codeCommune2Etablissement est vide).
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_libelleCommuneEtranger2Etablissement = null;
	
	/**
	 * Dans le cas où l’établissement dispose d’une entrée secondaire,
	 * la distribution spéciale reprend les éléments particuliers qui
	 * accompagnent l’adresse secondaire de distribution spéciale. C’est
	 * un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative.
	 * 
	 * Longueur : 26
	 * 
	 * *Exemples de début de modalités attendues*
	 * BP : Boîte postale
	 * TSA : Tri par service à l'arrivée
	 * LP : Local postal
	 * RP : Référence postale
	 * SP : Secteur postal
	 * CP : Case postale
	 * CE : Case entreprise
	 * CS : Course spéciale
	 * POSTE RESTANTE
	 * CEDEX : Courrier d'entreprise à distribution exceptionnelle
	 * CIDEX : Courrier individuel à distribution exceptionnelle
	 * CASE, NIVEAU : Mots utilisés pour la distribution interne du courrier
	 * à LA DEFENSE
	 * CASIER : Utilisé pour le centre commercial des Quatre Temps
	 * SILIC, SENIA, MAREE, FLEURS… : Utilisés sur le site de Rungis
	 * 
	 * @var ?string
	 */
	protected ?string $_distributionSpeciale2Etablissement = null;
	
	/**
	 * Dans le cas où l’établissement dispose d’une entrée secondaire,
	 * cette variable désigne le code de la commune de l’adresse secondaire
	 * de l’établissement, hors adresse à l'étranger. Le code commune
	 * correspond au code commune existant à la date de la mise à disposition
	 * : toute modification du code officiel géographique est répercutée sur
	 * la totalité des établissements (même ceux fermés) correspondant à
	 * ce code commune.
	 * Pour les établissements localisés à l'étranger, la variable
	 * codeCommune2Etablissement est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @var ?string
	 */
	protected ?string $_codeCommune2Etablissement = null;
	
	/**
	 * Dans le cas où l’établissement dispose d’une entrée secondaire,
	 * c’est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative.
	 * 
	 * Longueur : 9
	 * 
	 * @var ?string
	 */
	protected ?string $_codeCedex2Etablissement = null;
	
	/**
	 * Cette variable indique le libellé correspondant au code cedex de
	 * l’adresse secondaire de l'établissement si celui-ci est non null. Ce
	 * libellé est le libellé utilisé dans la ligne 6 d’adresse pour
	 * l’acheminement postal.
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_libelleCedex2Etablissement = null;
	
	/**
	 * Dans le cas où l’établissement dispose d’une entrée secondaire,
	 * cette variable désigne le code du pays de localisation de l'adresse
	 * secondaire de l'établissement pour les adresses à l'étranger. La
	 * variable codePaysEtranger2Etablissement commence toujours par 99 si elle
	 * est renseignée. Les 3 caractères suivants sont le code du pays
	 * étranger.
	 * 
	 * Longueur : 5
	 * 
	 * @var ?string
	 */
	protected ?string $_codePaysEtranger2Etablissement = null;
	
	/**
	 * Cette variable indique le libellé du pays de localisation de
	 * l’adresse secondaire de l'établissement si celui-ci est à
	 * l’étranger.
	 * C’est un élément constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_libellePaysEtranger2Etablissement = null;
	
	/**
	 * Date de début de la période au cours de laquelle toutes les variables
	 * historisées de l'établissement restent inchangées.
	 * La date 1900-01-01 signifie : date non déterminée.
	 * dateDebut peut-être vide uniquement pour les établissements des
	 * unités purgées (cf. variable unitePurgeeUniteLegale dans le descriptif
	 * des variables du fichier StockUniteLegale).
	 * La date de début de la période la plus ancienne ne correspond pas
	 * obligatoirement à la date de création de l'établissement, certaines
	 * variables historisées pouvant posséder des dates de début soit au
	 * 1900-01-01 soit antérieures à la date de création.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_dateDebut = null;
	
	/**
	 * Lors de son inscription au répertoire, un établissement est, sauf
	 * exception, à l’état « Actif ». Le passage à l’état « Fermé
	 * » découle de la prise en compte d’une déclaration de fermeture.
	 * À noter qu’un établissement fermé peut être rouvert.
	 * En règle générale, la première période d’historique d’un
	 * établissement correspond à un etatAdministratifUniteLegale égal à «
	 * Actif ». Toutefois, l'état administratif peut être à null (première
	 * date de début de l'état postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * @var ApiFrInseeSireneEtatAdministratifInterface
	 */
	protected ApiFrInseeSireneEtatAdministratifInterface $_etatAdministratifEtablissement;
	
	/**
	 * Les trois variables enseigne1Etablissement, enseigne2Etablissement et
	 * enseigne3Etablissement contiennent la ou les enseignes de
	 * l'établissement.
	 * L'enseigne identifie l'emplacement ou le local dans lequel est exercée
	 * l'activité. Un établissement peut posséder une enseigne, plusieurs
	 * enseignes ou aucune.
	 * 
	 * L'analyse des enseignes et de son découpage en trois variables dans
	 * Sirene montre deux cas possibles : soit les 3 champs concernent 3
	 * enseignes bien distinctes, soit ces trois champs correspondent au
	 * découpage de l'enseigne qui est déclarée dans la liasse (sur un seul
	 * champ) avec une continuité des trois champs.
	 * 
	 * *Exemples*
	 * SIRET=53053581400178
	 * "enseigne1Etablissement": "LES SERRURIERS DES YVELINES LES VITRIERS DES
	 * YVELI",
	 * "enseigne2Etablissement": "NES LES CHAUFFAGISTES DES YVELINES LES
	 * PLATRIERS D",
	 * "enseigne3Etablissement": "ES YVELINES LES ELECTRICIENS DES
	 * YVELINES.…"
	 * 
	 * SIRET=05439181800033
	 * "enseigne1Etablissement": "HALTE OCCASIONS",
	 * "enseigne2Etablissement": "OUTRE-MER LOCATION",
	 * "enseigne3Etablissement": "OUTRE-MER TRANSIT".
	 * 
	 * Cette variable est historisée.
	 * 
	 * Longueur : 50
	 * 
	 * @var ?string
	 */
	protected ?string $_enseigne1Etablissement = null;
	
	/**
	 * 2e ligne de l'enseigne.
	 * 
	 * @var ?string
	 */
	protected ?string $_enseigne2Etablissement = null;
	
	/**
	 * 3e ligne de l'enseigne.
	 * 
	 * @var ?string
	 */
	protected ?string $_enseigne3Etablissement = null;
	
	/**
	 * Cette variable désigne le nom sous lequel l'établissement est connu du
	 * grand public. Cet élément d'identification de l'établissement a été
	 * enregistré au niveau établissement depuis l'application de la norme
	 * d'échanges CFE de 2008. Avant la norme 2008, la dénomination usuelle
	 * était enregistrée au niveau de l'unité légale sur trois champs (cf.
	 * variables denominationUsuelle1UniteLegale à
	 * denominationUsuelle3UniteLegale dans le descriptif des variables du
	 * fichier StockUniteLegale).
	 * Cette variable est historisée.
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_denominationUsuelleEtablissement = null;
	
	/**
	 * Lors de son inscription au répertoire, l’Insee attribue à tout
	 * établissement un code dit « APE » sur la base de la description de
	 * l’activité principale faite par le déclarant. Ce code est modifiable
	 * au cours de la vie de l’établissement en fonction des déclarations
	 * de l’exploitant.
	 * Pour chaque établissement, il existe à un instant donné un seul code
	 * « APE ». Il est attribué selon la nomenclature en vigueur. La
	 * nomenclature en vigueur est la Naf Rév2 et ce depuis le 1 er Janvier
	 * 2008.
	 * Chaque code comporte 2 chiffres, un point, 2 chiffres et une lettre.
	 * Tous les établissements actifs au 01/01/2008 ont eu leur code APE
	 * recodé dans la nouvelle nomenclature, ainsi de très nombreux
	 * établissements ont une période débutant à cette date.
	 * Au moment de la déclaration de l’entreprise, il peut arriver que
	 * l’Insee ne soit pas en mesure d’attribuer le bon code APE : la
	 * modalité 00.00Z peut alors être affectée provisoirement.
	 * 
	 * *Historique*
	 * Le code APE est historisé depuis le 01/01/2005.
	 * La règle d’historisation des données d’activité est la suivante :
	 *  - Pour les établissements fermés avant le 31/12/2004, seul le dernier
	 * code activité connu figure, dans la nomenclature en vigueur à la date
	 * de fermeture.
	 *  - Pour les établissements ouverts après le 01/01/2005 et fermés
	 * avant le 31/12/2007, l’historique des codes attribués sur la période
	 * est disponible.
	 *  - Pour les établissements ouverts après le 01/01/2005 et toujours
	 * ouverts le 01/01/2008, l’historique intègre le changement de
	 * nomenclature.
	 *  - Pour les établissements ouverts après le 01/01/2008, l’historique
	 * comprend les modifications apportées au cours de la vie de
	 * l’établissement.
	 * L'APE peut être à null (cas des unités purgées - pour des raisons de
	 * capacité de stockage des données, les données concernant les
	 * entreprises cessées avant le 31/12/2002 ont été purgées -, première
	 * date de début de l'APE postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * Longueur : 6
	 * 
	 * @var ?string
	 */
	protected ?string $_activitePrincipaleEtablissement = null;
	
	/**
	 * Cette variable indique la nomenclature d’activité correspondant à la
	 * variable activitePrincipaleEtablissement (cf.
	 * activitePrincipaleEtablissement).
	 * 
	 * @var ?ApiFrInseeSireneNomenclatureApeInterface
	 */
	protected ?ApiFrInseeSireneNomenclatureApeInterface $_nomenclatureActivitePrincipaleEtablissement = null;
	
	/**
	 * Lors de sa formalité d’ouverture, le déclarant indique si
	 * l’établissement aura ou non des employés. Par la suite, le
	 * déclarant peut également faire des déclarations de prise d’emploi
	 * et de fin d’emploi. La prise en compte d’une déclaration de prise
	 * d’emploi bascule immédiatement l’établissement en « employeur ».
	 * Inversement, lorsqu’une déclaration de fin d’emploi est traitée,
	 * l’établissement devient « non employeur ». Pour chaque
	 * établissement, il existe à un instant donné un seul code « employeur
	 * ».
	 * Cette variable est historisée pour les établissements qui étaient
	 * ouverts en 2005 et pour ceux ouverts ultérieurement.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_caractereEmployeurEtablissement = null;
	
	/**
	 * Constructor for ApiFrInseeSireneEtablissement with private members.
	 * 
	 * @param string $siren
	 * @param string $nic
	 * @param string $siret
	 * @param bool $statutDiffusionEtablissement
	 * @param bool $etablissementSiege
	 * @param ApiFrInseeSireneEtatAdministratifInterface $etatAdministratifEtablissement
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(string $siren, string $nic, string $siret, bool $statutDiffusionEtablissement, bool $etablissementSiege, ApiFrInseeSireneEtatAdministratifInterface $etatAdministratifEtablissement)
	{
		$this->setSiren($siren);
		$this->setNic($nic);
		$this->setSiret($siret);
		$this->setStatutDiffusionEtablissement($statutDiffusionEtablissement);
		$this->setEtablissementSiege($etablissementSiege);
		$this->setEtatAdministratifEtablissement($etatAdministratifEtablissement);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets un numéro d’identité de l’unité légale est attribué par
	 * l’Insee à toutes les personnes physiques ou morales inscrites au
	 * répertoire ainsi qu’à leurs établissements : le numéro Siren. Ce
	 * numéro unique est « attribué soit à l’occasion des demandes
	 * d’immatriculation au registre du commerce et des sociétés ou des
	 * déclarations effectuées au répertoire des métiers, soit à la
	 * demande d’administrations » (article R123-224 du code de  commerce).
	 * Lors de sa création, une unité légale se voit attribuer un numéro
	 * Siren de 9 chiffres.
	 * 
	 * *Règles de gestion*
	 * Les entrepreneurs individuels, ou personnes physiques, conservent le
	 * même numéro Siren jusqu’à leur décès. Les sociétés, ou
	 * personnes morales, perdent la personnalité juridique au moment de la
	 * cessation de l’activité de l’entreprise. Si l’activité devait
	 * reprendre ultérieurement, un nouveau numéro Siren sera attribué. Les
	 * numéros d’identification sont uniques : lorsqu’un numéro Siren a
	 * été attribué, il ne peut pas être réutilisé et attribué à une
	 * nouvelle unité légale, même lorsque l’activité a cessé.
	 * 
	 * *Historique*
	 * Même si la mise en place du répertoire Sirene remonte à 1973, toutes
	 * les unités légales, y compris celles créées avant cette date,
	 * disposent d’un numéro Siren pour le secteur privé non agricole.
	 * En 1983, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus aux institutions et services de
	 * l’État et aux collectivités territoriales.
	 * En 1993, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus au secteur privé agricole.
	 * 
	 * Longueur : 9
	 * 
	 * @param string $siren
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setSiren(string $siren) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_siren = $siren;
		
		return $this;
	}
	
	/**
	 * Gets un numéro d’identité de l’unité légale est attribué par
	 * l’Insee à toutes les personnes physiques ou morales inscrites au
	 * répertoire ainsi qu’à leurs établissements : le numéro Siren. Ce
	 * numéro unique est « attribué soit à l’occasion des demandes
	 * d’immatriculation au registre du commerce et des sociétés ou des
	 * déclarations effectuées au répertoire des métiers, soit à la
	 * demande d’administrations » (article R123-224 du code de  commerce).
	 * Lors de sa création, une unité légale se voit attribuer un numéro
	 * Siren de 9 chiffres.
	 * 
	 * *Règles de gestion*
	 * Les entrepreneurs individuels, ou personnes physiques, conservent le
	 * même numéro Siren jusqu’à leur décès. Les sociétés, ou
	 * personnes morales, perdent la personnalité juridique au moment de la
	 * cessation de l’activité de l’entreprise. Si l’activité devait
	 * reprendre ultérieurement, un nouveau numéro Siren sera attribué. Les
	 * numéros d’identification sont uniques : lorsqu’un numéro Siren a
	 * été attribué, il ne peut pas être réutilisé et attribué à une
	 * nouvelle unité légale, même lorsque l’activité a cessé.
	 * 
	 * *Historique*
	 * Même si la mise en place du répertoire Sirene remonte à 1973, toutes
	 * les unités légales, y compris celles créées avant cette date,
	 * disposent d’un numéro Siren pour le secteur privé non agricole.
	 * En 1983, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus aux institutions et services de
	 * l’État et aux collectivités territoriales.
	 * En 1993, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus au secteur privé agricole.
	 * 
	 * Longueur : 9
	 * 
	 * @return string
	 */
	public function getSiren() : string
	{
		return $this->_siren;
	}
	
	/**
	 * Sets le numéro interne de classement permet de distinguer les
	 * établissements d'une même entreprise. Il est composé de 5 chiffres.
	 * Associé au Siren, il forme le Siret de l'établissement. Il est
	 * composé de quatre chiffres et d'un cinquième qui permet de contrôler
	 * la validité du numéro Siret.
	 * Le Nic est attribué une seule fois au sein de l'entreprise. Si
	 * l'établissement ferme, son Nic ne peut être réattribué à un nouvel
	 * établissement.
	 * 
	 * Longueur : 5
	 * 
	 * @param string $nic
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setNic(string $nic) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_nic = $nic;
		
		return $this;
	}
	
	/**
	 * Gets le numéro interne de classement permet de distinguer les
	 * établissements d'une même entreprise. Il est composé de 5 chiffres.
	 * Associé au Siren, il forme le Siret de l'établissement. Il est
	 * composé de quatre chiffres et d'un cinquième qui permet de contrôler
	 * la validité du numéro Siret.
	 * Le Nic est attribué une seule fois au sein de l'entreprise. Si
	 * l'établissement ferme, son Nic ne peut être réattribué à un nouvel
	 * établissement.
	 * 
	 * Longueur : 5
	 * 
	 * @return string
	 */
	public function getNic() : string
	{
		return $this->_nic;
	}
	
	/**
	 * Sets le numéro Siret est le numéro unique d’identification attribué
	 * à chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * Une entreprise est constituée d’autant d’établissements qu’il y
	 * a de lieux différents où elle exerce son activité. L’établissement
	 * est fermé quand l’activité cesse dans l’établissement concerné
	 * ou lorsque l’établissement change d’adresse.
	 * 
	 * Longueur : 14
	 * 
	 * @param string $siret
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setSiret(string $siret) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_siret = $siret;
		
		return $this;
	}
	
	/**
	 * Gets le numéro Siret est le numéro unique d’identification attribué
	 * à chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * Une entreprise est constituée d’autant d’établissements qu’il y
	 * a de lieux différents où elle exerce son activité. L’établissement
	 * est fermé quand l’activité cesse dans l’établissement concerné
	 * ou lorsque l’établissement change d’adresse.
	 * 
	 * Longueur : 14
	 * 
	 * @return string
	 */
	public function getSiret() : string
	{
		return $this->_siret;
	}
	
	/**
	 * Sets seuls les établissements diffusibles sont accessibles à tout
	 * public (statutDiffusionEtablissement=O).
	 * 
	 * @param bool $statutDiffusionEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setStatutDiffusionEtablissement(bool $statutDiffusionEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_statutDiffusionEtablissement = $statutDiffusionEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets seuls les établissements diffusibles sont accessibles à tout
	 * public (statutDiffusionEtablissement=O).
	 * 
	 * @return bool
	 */
	public function hasStatutDiffusionEtablissement() : bool
	{
		return $this->_statutDiffusionEtablissement;
	}
	
	/**
	 * Sets la date de création correspond à la date qui figure dans les
	 * statuts de l’entreprise déposés au CFE compétent.
	 * Pour les établissements des unités purgées
	 * (unitePurgeeUniteLegale=true): si la date de création est au 01/01/1900
	 * dans Sirene, la date est forcée à null.
	 * Dans tous les autres cas, la date de création n'est jamais à null. Si
	 * elle est non renseignée, elle sera au 01/01/1900.
	 * La date de création ne correspond pas obligatoirement à dateDebut de
	 * la première période de l'établissement, certaines variables
	 * historisées pouvant posséder des dates de début soit au 1900-01-01,
	 * soit antérieures à la date de création.
	 * 
	 * @param ?DateTimeInterface $dateCreationEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setDateCreationEtablissement(?DateTimeInterface $dateCreationEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_dateCreationEtablissement = $dateCreationEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets la date de création correspond à la date qui figure dans les
	 * statuts de l’entreprise déposés au CFE compétent.
	 * Pour les établissements des unités purgées
	 * (unitePurgeeUniteLegale=true): si la date de création est au 01/01/1900
	 * dans Sirene, la date est forcée à null.
	 * Dans tous les autres cas, la date de création n'est jamais à null. Si
	 * elle est non renseignée, elle sera au 01/01/1900.
	 * La date de création ne correspond pas obligatoirement à dateDebut de
	 * la première période de l'établissement, certaines variables
	 * historisées pouvant posséder des dates de début soit au 1900-01-01,
	 * soit antérieures à la date de création.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateCreationEtablissement() : ?DateTimeInterface
	{
		return $this->_dateCreationEtablissement;
	}
	
	/**
	 * Sets il s’agit d’une variable statistique, millésimée au 31/12
	 * d’une année donnée (voir variable anneeEffectifsEtablissement).
	 * 
	 * @param ?ApiFrInseeSireneTrancheEffectifsInterface $trancheEffectifsEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setTrancheEffectifsEtablissement(?ApiFrInseeSireneTrancheEffectifsInterface $trancheEffectifsEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_trancheEffectifsEtablissement = $trancheEffectifsEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets il s’agit d’une variable statistique, millésimée au 31/12
	 * d’une année donnée (voir variable anneeEffectifsEtablissement).
	 * 
	 * @return ?ApiFrInseeSireneTrancheEffectifsInterface
	 */
	public function getTrancheEffectifsEtablissement() : ?ApiFrInseeSireneTrancheEffectifsInterface
	{
		return $this->_trancheEffectifsEtablissement;
	}
	
	/**
	 * Sets seule la dernière année de mise à jour de l’effectif salarié
	 * de l’établissement est donnée si celle-ci est inférieure ou égale
	 * à l’année d’interrogation-3. Ainsi une interrogation en 2018 ne
	 * renverra la dernière année de mise à jour de l’effectif que si
	 * cette année est supérieure ou égale à 2015.
	 * 
	 * @param ?int $anneeEffectifsEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setAnneeEffectifsEtablissement(?int $anneeEffectifsEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_anneeEffectifsEtablissement = $anneeEffectifsEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets seule la dernière année de mise à jour de l’effectif salarié
	 * de l’établissement est donnée si celle-ci est inférieure ou égale
	 * à l’année d’interrogation-3. Ainsi une interrogation en 2018 ne
	 * renverra la dernière année de mise à jour de l’effectif que si
	 * cette année est supérieure ou égale à 2015.
	 * 
	 * @return ?int
	 */
	public function getAnneeEffectifsEtablissement() : ?int
	{
		return $this->_anneeEffectifsEtablissement;
	}
	
	/**
	 * Sets cette variable désigne le code de l'activité exercée par
	 * l'artisan inscrit au registre des métiers selon la Nomenclature
	 * d'Activités Française de l'Artisanat (NAFA).
	 * 
	 * Longueur : 6
	 * 
	 * @param ?string $activitePrincipaleRegistreMetiersEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setActivitePrincipaleRegistreMetiersEtablissement(?string $activitePrincipaleRegistreMetiersEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_activitePrincipaleRegistreMetiersEtablissement = $activitePrincipaleRegistreMetiersEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable désigne le code de l'activité exercée par
	 * l'artisan inscrit au registre des métiers selon la Nomenclature
	 * d'Activités Française de l'Artisanat (NAFA).
	 * 
	 * Longueur : 6
	 * 
	 * @return ?string
	 */
	public function getActivitePrincipaleRegistreMetiersEtablissement() : ?string
	{
		return $this->_activitePrincipaleRegistreMetiersEtablissement;
	}
	
	/**
	 * Sets cette date peut concerner des mises à jour de données du
	 * répertoire Sirene qui ne sont pas diffusées par API Sirene.
	 * Cette variable peut être à null.
	 * 
	 * @param ?DateTimeInterface $dateDernierTraitementEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setDateDernierTraitementEtablissement(?DateTimeInterface $dateDernierTraitementEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_dateDernierTraitementEtablissement = $dateDernierTraitementEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette date peut concerner des mises à jour de données du
	 * répertoire Sirene qui ne sont pas diffusées par API Sirene.
	 * Cette variable peut être à null.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateDernierTraitementEtablissement() : ?DateTimeInterface
	{
		return $this->_dateDernierTraitementEtablissement;
	}
	
	/**
	 * Sets c’est une variable booléenne qui indique si l'établissement est
	 * le siège ou non de l'unité légale. Variable calculée toujours
	 * renseignée.
	 * 
	 * @param bool $etablissementSiege
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setEtablissementSiege(bool $etablissementSiege) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_etablissementSiege = $etablissementSiege;
		
		return $this;
	}
	
	/**
	 * Gets c’est une variable booléenne qui indique si l'établissement est
	 * le siège ou non de l'unité légale. Variable calculée toujours
	 * renseignée.
	 * 
	 * @return bool
	 */
	public function hasEtablissementSiege() : bool
	{
		return $this->_etablissementSiege;
	}
	
	/**
	 * Sets cette variable donne le nombre de périodes [dateDebut,dateFin] de
	 * l’établissement. Chaque période correspond à l’intervalle de
	 * temps pendant lequel la totalité des variables historisées de
	 * l’établissement n’ont pas été modifiées.
	 * Les dates de ces périodes sont des dates d’effet (et non des dates de
	 * traitement).
	 * 
	 * @param ?int $nombrePeriodesEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setNombrePeriodesEtablissement(?int $nombrePeriodesEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_nombrePeriodesEtablissement = $nombrePeriodesEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable donne le nombre de périodes [dateDebut,dateFin] de
	 * l’établissement. Chaque période correspond à l’intervalle de
	 * temps pendant lequel la totalité des variables historisées de
	 * l’établissement n’ont pas été modifiées.
	 * Les dates de ces périodes sont des dates d’effet (et non des dates de
	 * traitement).
	 * 
	 * @return ?int
	 */
	public function getNombrePeriodesEtablissement() : ?int
	{
		return $this->_nombrePeriodesEtablissement;
	}
	
	/**
	 * Sets cette variable est un élément constitutif de l’adresse.
	 * C'est une variable facultative qui précise l'adresse avec :
	 *  - une indication d'étage, d'appartement, de porte, de N° de boîte à
	 * lettres,
	 *  - la désignation d'un bâtiment, d'un escalier, d'une entrée, d'un
	 * bloc,
	 *  - le nom d'une résidence, d'un ensemble….
	 * 
	 * Longueur : 38
	 * 
	 * @param ?string $complementAdresseEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setComplementAdresseEtablissement(?string $complementAdresseEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_complementAdresseEtablissement = $complementAdresseEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable est un élément constitutif de l’adresse.
	 * C'est une variable facultative qui précise l'adresse avec :
	 *  - une indication d'étage, d'appartement, de porte, de N° de boîte à
	 * lettres,
	 *  - la désignation d'un bâtiment, d'un escalier, d'une entrée, d'un
	 * bloc,
	 *  - le nom d'une résidence, d'un ensemble….
	 * 
	 * Longueur : 38
	 * 
	 * @return ?string
	 */
	public function getComplementAdresseEtablissement() : ?string
	{
		return $this->_complementAdresseEtablissement;
	}
	
	/**
	 * Sets c'est un élément constitutif de l’adresse.
	 * Cette variable est facultative.
	 * Si plusieurs numéros de voie sont indiqués (5-7, 5 à 7…),
	 * l'information complète (5-7) ou (5 à 7) figure en complément
	 * d'adresse et le premier des numéros (5 dans l'exemple) est porté dans
	 * la variable numeroVoieEtablissement.
	 * 
	 * @param ?int $numeroVoieEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setNumeroVoieEtablissement(?int $numeroVoieEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_numeroVoieEtablissement = $numeroVoieEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets c'est un élément constitutif de l’adresse.
	 * Cette variable est facultative.
	 * Si plusieurs numéros de voie sont indiqués (5-7, 5 à 7…),
	 * l'information complète (5-7) ou (5 à 7) figure en complément
	 * d'adresse et le premier des numéros (5 dans l'exemple) est porté dans
	 * la variable numeroVoieEtablissement.
	 * 
	 * @return ?int
	 */
	public function getNumeroVoieEtablissement() : ?int
	{
		return $this->_numeroVoieEtablissement;
	}
	
	/**
	 * Sets cette variable un élément constitutif de l’adresse.
	 * Cette variable est facultative.
	 * Elle doit être associée à la variable numeroVoieEtablissement.
	 * 
	 * Longueur : 1
	 * 
	 * @param ?string $indiceRepetitionEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setIndiceRepetitionEtablissement(?string $indiceRepetitionEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_indiceRepetitionEtablissement = $indiceRepetitionEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable un élément constitutif de l’adresse.
	 * Cette variable est facultative.
	 * Elle doit être associée à la variable numeroVoieEtablissement.
	 * 
	 * Longueur : 1
	 * 
	 * @return ?string
	 */
	public function getIndiceRepetitionEtablissement() : ?string
	{
		return $this->_indiceRepetitionEtablissement;
	}
	
	/**
	 * Sets le numéro de voie de l'adresse précédente, s'il y en a une..
	 * 
	 * @param ?int $dernierNumeroVoieEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setDernierNumeroVoieEtablissement(?int $dernierNumeroVoieEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_dernierNumeroVoieEtablissement = $dernierNumeroVoieEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets le numéro de voie de l'adresse précédente, s'il y en a une..
	 * 
	 * @return ?int
	 */
	public function getDernierNumeroVoieEtablissement() : ?int
	{
		return $this->_dernierNumeroVoieEtablissement;
	}
	
	/**
	 * Sets l'indice de répétition de l'adresse précédente, s'il y en a
	 * une.
	 * 
	 * @param ?string $indiceRepetitionDernierNumeroVoieEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setIndiceRepetitionDernierNumeroVoieEtablissement(?string $indiceRepetitionDernierNumeroVoieEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_indiceRepetitionDernierNumeroVoieEtablissement = $indiceRepetitionDernierNumeroVoieEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets l'indice de répétition de l'adresse précédente, s'il y en a
	 * une.
	 * 
	 * @return ?string
	 */
	public function getIndiceRepetitionDernierNumeroVoieEtablissement() : ?string
	{
		return $this->_indiceRepetitionDernierNumeroVoieEtablissement;
	}
	
	/**
	 * Sets c'est un élément constitutif de l’adresse.
	 * 
	 * Si le type de voie est d'une longueur inférieure ou égale à 4
	 * caractères, le type de voie n'est pas abrégé. Ainsi, RUE ou QUAI sont
	 * écrits tels quels, alors que AVENUE est abrégée en AV.
	 * 
	 * @param ?ApiFrInseeBanTypeVoieInterface $typeVoieEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setTypeVoieEtablissement(?ApiFrInseeBanTypeVoieInterface $typeVoieEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_typeVoieEtablissement = $typeVoieEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets c'est un élément constitutif de l’adresse.
	 * 
	 * Si le type de voie est d'une longueur inférieure ou égale à 4
	 * caractères, le type de voie n'est pas abrégé. Ainsi, RUE ou QUAI sont
	 * écrits tels quels, alors que AVENUE est abrégée en AV.
	 * 
	 * @return ?ApiFrInseeBanTypeVoieInterface
	 */
	public function getTypeVoieEtablissement() : ?ApiFrInseeBanTypeVoieInterface
	{
		return $this->_typeVoieEtablissement;
	}
	
	/**
	 * Sets cette variable indique le libellé de voie de la commune de
	 * localisation de l'établissement. 
	 * C’est un élément constitutif de l’adresse.
	 * Cette variable est facultative : elle n'est pas toujours renseignée, en
	 * particulier dans les petites communes.
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $libelleVoieEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setLibelleVoieEtablissement(?string $libelleVoieEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_libelleVoieEtablissement = $libelleVoieEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique le libellé de voie de la commune de
	 * localisation de l'établissement. 
	 * C’est un élément constitutif de l’adresse.
	 * Cette variable est facultative : elle n'est pas toujours renseignée, en
	 * particulier dans les petites communes.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleVoieEtablissement() : ?string
	{
		return $this->_libelleVoieEtablissement;
	}
	
	/**
	 * Sets cette variable désigne le code postal de l’adresse de
	 * l’établissement.
	 * Pour les adresses à l’étranger (codeCommuneEtablissement commence
	 * par 99), cette variable est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @param ?string $codePostalEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setCodePostalEtablissement(?string $codePostalEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_codePostalEtablissement = $codePostalEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable désigne le code postal de l’adresse de
	 * l’établissement.
	 * Pour les adresses à l’étranger (codeCommuneEtablissement commence
	 * par 99), cette variable est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @return ?string
	 */
	public function getCodePostalEtablissement() : ?string
	{
		return $this->_codePostalEtablissement;
	}
	
	/**
	 * Sets cette variable indique le libellé de la commune de localisation de
	 * l'établissement si celui-ci n’est pas à l’étranger. C’est un
	 * élément constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $libelleCommuneEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setLibelleCommuneEtablissement(?string $libelleCommuneEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_libelleCommuneEtablissement = $libelleCommuneEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique le libellé de la commune de localisation de
	 * l'établissement si celui-ci n’est pas à l’étranger. C’est un
	 * élément constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleCommuneEtablissement() : ?string
	{
		return $this->_libelleCommuneEtablissement;
	}
	
	/**
	 * Sets cette variable est un élément constitutif de l’adresse pour les
	 * établissements situés sur le territoire étranger (la variable
	 * codeCommuneEtablissement est vide).
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $libelleCommuneEtrangerEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setLibelleCommuneEtrangerEtablissement(?string $libelleCommuneEtrangerEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_libelleCommuneEtrangerEtablissement = $libelleCommuneEtrangerEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable est un élément constitutif de l’adresse pour les
	 * établissements situés sur le territoire étranger (la variable
	 * codeCommuneEtablissement est vide).
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleCommuneEtrangerEtablissement() : ?string
	{
		return $this->_libelleCommuneEtrangerEtablissement;
	}
	
	/**
	 * Sets la distribution spéciale reprend les éléments particuliers qui
	 * accompagnent une adresse de distribution spéciale. C’est un élément
	 * constitutif de l’adresse.
	 * Cette variable est facultative.
	 * 
	 * Longueur 26
	 * 
	 * *Exemples de début de modalités attendues*
	 * BP : Boîte postale
	 * TSA : Tri par service à l'arrivée
	 * LP : Local postal
	 * RP : Référence postale
	 * SP : Secteur postal
	 * CP : Case postale
	 * CE : Case entreprise
	 * CS : Course spéciale
	 * POSTE RESTANTE
	 * CEDEX : Courrier d'entreprise à distribution exceptionnelle
	 * CIDEX : Courrier individuel à distribution exceptionnelle
	 * CASE, NIVEAU : Mots utilisés pour la distribution interne du courrier
	 * à LA DEFENSE
	 * CASIER : Utilisé pour le centre commercial des Quatre Temps
	 * SILIC, SENIA, MAREE, FLEURS… : Utilisés sur le site de Rungis
	 * 
	 * @param ?string $distributionSpecialeEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setDistributionSpecialeEtablissement(?string $distributionSpecialeEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_distributionSpecialeEtablissement = $distributionSpecialeEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets la distribution spéciale reprend les éléments particuliers qui
	 * accompagnent une adresse de distribution spéciale. C’est un élément
	 * constitutif de l’adresse.
	 * Cette variable est facultative.
	 * 
	 * Longueur 26
	 * 
	 * *Exemples de début de modalités attendues*
	 * BP : Boîte postale
	 * TSA : Tri par service à l'arrivée
	 * LP : Local postal
	 * RP : Référence postale
	 * SP : Secteur postal
	 * CP : Case postale
	 * CE : Case entreprise
	 * CS : Course spéciale
	 * POSTE RESTANTE
	 * CEDEX : Courrier d'entreprise à distribution exceptionnelle
	 * CIDEX : Courrier individuel à distribution exceptionnelle
	 * CASE, NIVEAU : Mots utilisés pour la distribution interne du courrier
	 * à LA DEFENSE
	 * CASIER : Utilisé pour le centre commercial des Quatre Temps
	 * SILIC, SENIA, MAREE, FLEURS… : Utilisés sur le site de Rungis
	 * 
	 * @return ?string
	 */
	public function getDistributionSpecialeEtablissement() : ?string
	{
		return $this->_distributionSpecialeEtablissement;
	}
	
	/**
	 * Sets cette variable désigne le code de la commune de localisation de
	 * l'établissement, hors adresse à l'étranger.
	 * Le code commune correspond au code commune existant à la date de la
	 * mise à disposition : toute modification du code officiel géographique
	 * est répercutée sur la totalité des établissements (même ceux
	 * fermés) correspondant à ce code commune.
	 * 
	 * Pour les établissements localisés à l'étranger, la variable
	 * codeCommuneEtablissement est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @param ?string $codeCommuneEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setCodeCommuneEtablissement(?string $codeCommuneEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_codeCommuneEtablissement = $codeCommuneEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable désigne le code de la commune de localisation de
	 * l'établissement, hors adresse à l'étranger.
	 * Le code commune correspond au code commune existant à la date de la
	 * mise à disposition : toute modification du code officiel géographique
	 * est répercutée sur la totalité des établissements (même ceux
	 * fermés) correspondant à ce code commune.
	 * 
	 * Pour les établissements localisés à l'étranger, la variable
	 * codeCommuneEtablissement est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @return ?string
	 */
	public function getCodeCommuneEtablissement() : ?string
	{
		return $this->_codeCommuneEtablissement;
	}
	
	/**
	 * Sets cette variable est un élément constitutif de l’adresse.
	 * Elle est facultative.
	 * 
	 * Longueur : 9
	 * 
	 * @param ?string $codeCedexEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setCodeCedexEtablissement(?string $codeCedexEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_codeCedexEtablissement = $codeCedexEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable est un élément constitutif de l’adresse.
	 * Elle est facultative.
	 * 
	 * Longueur : 9
	 * 
	 * @return ?string
	 */
	public function getCodeCedexEtablissement() : ?string
	{
		return $this->_codeCedexEtablissement;
	}
	
	/**
	 * Sets cette variable indique le libellé correspondant au code cedex de
	 * l'établissement si celui-ci est non null. Ce libellé est le libellé
	 * utilisé dans la ligne 6 d’adresse pour l’acheminement postal.
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $libelleCedexEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setLibelleCedexEtablissement(?string $libelleCedexEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_libelleCedexEtablissement = $libelleCedexEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique le libellé correspondant au code cedex de
	 * l'établissement si celui-ci est non null. Ce libellé est le libellé
	 * utilisé dans la ligne 6 d’adresse pour l’acheminement postal.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleCedexEtablissement() : ?string
	{
		return $this->_libelleCedexEtablissement;
	}
	
	/**
	 * Sets cette variable désigne le code du pays de localisation de
	 * l'établissement pour les adresses à l'étranger. La variable
	 * codePaysEtrangerEtablissement commence toujours par 99 si elle est
	 * renseignée. Les 3 caractères suivants sont le code du pays étranger.
	 * 
	 * Longueur : 5
	 * 
	 * @param ?string $codePaysEtrangerEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setCodePaysEtrangerEtablissement(?string $codePaysEtrangerEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_codePaysEtrangerEtablissement = $codePaysEtrangerEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable désigne le code du pays de localisation de
	 * l'établissement pour les adresses à l'étranger. La variable
	 * codePaysEtrangerEtablissement commence toujours par 99 si elle est
	 * renseignée. Les 3 caractères suivants sont le code du pays étranger.
	 * 
	 * Longueur : 5
	 * 
	 * @return ?string
	 */
	public function getCodePaysEtrangerEtablissement() : ?string
	{
		return $this->_codePaysEtrangerEtablissement;
	}
	
	/**
	 * Sets cette variable indique le libellé du pays de localisation de
	 * l'établissement si celui-ci est à l’étranger. C’est un élément
	 * constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $libellePaysEtrangerEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setLibellePaysEtrangerEtablissement(?string $libellePaysEtrangerEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_libellePaysEtrangerEtablissement = $libellePaysEtrangerEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique le libellé du pays de localisation de
	 * l'établissement si celui-ci est à l’étranger. C’est un élément
	 * constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibellePaysEtrangerEtablissement() : ?string
	{
		return $this->_libellePaysEtrangerEtablissement;
	}
	
	/**
	 * Sets identifiant de l'adresse
	 * Longueur : 15.
	 * 
	 * @param ?string $identifiantAdresseEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setIdentifiantAdresseEtablissement(?string $identifiantAdresseEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_identifiantAdresseEtablissement = $identifiantAdresseEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets identifiant de l'adresse
	 * Longueur : 15.
	 * 
	 * @return ?string
	 */
	public function getIdentifiantAdresseEtablissement() : ?string
	{
		return $this->_identifiantAdresseEtablissement;
	}
	
	/**
	 * Sets abscisse des coordonnees Lambert de l'adresse.
	 * 
	 * @param ?float $coordonneeLambertAbscisseEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setCoordonneeLambertAbscisseEtablissement(?float $coordonneeLambertAbscisseEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_coordonneeLambertAbscisseEtablissement = $coordonneeLambertAbscisseEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets abscisse des coordonnees Lambert de l'adresse.
	 * 
	 * @return ?float
	 */
	public function getCoordonneeLambertAbscisseEtablissement() : ?float
	{
		return $this->_coordonneeLambertAbscisseEtablissement;
	}
	
	/**
	 * Sets ordonnée des coordonnees Lambert de l'adresse.
	 * 
	 * @param ?float $coordonneeLambertOrdonneeEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setCoordonneeLambertOrdonneeEtablissement(?float $coordonneeLambertOrdonneeEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_coordonneeLambertOrdonneeEtablissement = $coordonneeLambertOrdonneeEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets ordonnée des coordonnees Lambert de l'adresse.
	 * 
	 * @return ?float
	 */
	public function getCoordonneeLambertOrdonneeEtablissement() : ?float
	{
		return $this->_coordonneeLambertOrdonneeEtablissement;
	}
	
	/**
	 * Sets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, cette variable est un élément constitutif de l’adresse
	 * secondaire.
	 * C'est une variable facultative qui précise l'adresse secondaire avec :
	 *  - une indication d'étage, d'appartement, de porte, de N° de boîte à
	 * lettres,
	 *  - la désignation d'un bâtiment, d'un escalier, d'une entrée, d'un
	 * bloc,
	 *  - le nom d'une résidence, d'un ensemble….
	 * 
	 * Longueur : 38
	 * 
	 * @param ?string $complementAdresse2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setComplementAdresse2Etablissement(?string $complementAdresse2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_complementAdresse2Etablissement = $complementAdresse2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, cette variable est un élément constitutif de l’adresse
	 * secondaire.
	 * C'est une variable facultative qui précise l'adresse secondaire avec :
	 *  - une indication d'étage, d'appartement, de porte, de N° de boîte à
	 * lettres,
	 *  - la désignation d'un bâtiment, d'un escalier, d'une entrée, d'un
	 * bloc,
	 *  - le nom d'une résidence, d'un ensemble….
	 * 
	 * Longueur : 38
	 * 
	 * @return ?string
	 */
	public function getComplementAdresse2Etablissement() : ?string
	{
		return $this->_complementAdresse2Etablissement;
	}
	
	/**
	 * Sets c'est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative.
	 * Si plusieurs numéros de voie sont indiqués (5-7, 5 à 7…),
	 * l'information complète (5-7) ou (5 à 7) figure en complément
	 * d'adresse secondaire et le premier des numéros (5 dans l'exemple) est
	 * porté dans la variable numeroVoie2Etablissement.
	 * 
	 * @param ?int $numeroVoie2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setNumeroVoie2Etablissement(?int $numeroVoie2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_numeroVoie2Etablissement = $numeroVoie2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets c'est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative.
	 * Si plusieurs numéros de voie sont indiqués (5-7, 5 à 7…),
	 * l'information complète (5-7) ou (5 à 7) figure en complément
	 * d'adresse secondaire et le premier des numéros (5 dans l'exemple) est
	 * porté dans la variable numeroVoie2Etablissement.
	 * 
	 * @return ?int
	 */
	public function getNumeroVoie2Etablissement() : ?int
	{
		return $this->_numeroVoie2Etablissement;
	}
	
	/**
	 * Sets cette variable un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative. Elle doit être associée à la variable
	 * numeroVoie2Etablissement.
	 * 
	 * Longueur : 1
	 * 
	 * @param ?string $indiceRepetition2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setIndiceRepetition2Etablissement(?string $indiceRepetition2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_indiceRepetition2Etablissement = $indiceRepetition2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative. Elle doit être associée à la variable
	 * numeroVoie2Etablissement.
	 * 
	 * Longueur : 1
	 * 
	 * @return ?string
	 */
	public function getIndiceRepetition2Etablissement() : ?string
	{
		return $this->_indiceRepetition2Etablissement;
	}
	
	/**
	 * Sets cette variable est un élément constitutif de l’adresse
	 * secondaire.
	 * 
	 * Si le type de voie est d'une longueur inférieure ou égale à 4
	 * caractères, le type de voie n'est pas abrégé. Ainsi, RUE ou QUAI sont
	 * écrits tels quels, alors que AVENUE est abrégée en AV.
	 * 
	 * @param ?ApiFrInseeBanTypeVoieInterface $typeVoie2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setTypeVoie2Etablissement(?ApiFrInseeBanTypeVoieInterface $typeVoie2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_typeVoie2Etablissement = $typeVoie2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable est un élément constitutif de l’adresse
	 * secondaire.
	 * 
	 * Si le type de voie est d'une longueur inférieure ou égale à 4
	 * caractères, le type de voie n'est pas abrégé. Ainsi, RUE ou QUAI sont
	 * écrits tels quels, alors que AVENUE est abrégée en AV.
	 * 
	 * @return ?ApiFrInseeBanTypeVoieInterface
	 */
	public function getTypeVoie2Etablissement() : ?ApiFrInseeBanTypeVoieInterface
	{
		return $this->_typeVoie2Etablissement;
	}
	
	/**
	 * Sets cette variable indique le libellé de la voie de l’adresse
	 * secondaire de l'établissement.
	 * C’est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative : elle n'est pas toujours renseignée, en
	 * particulier dans les petites communes.
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $libelleVoie2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setLibelleVoie2Etablissement(?string $libelleVoie2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_libelleVoie2Etablissement = $libelleVoie2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique le libellé de la voie de l’adresse
	 * secondaire de l'établissement.
	 * C’est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative : elle n'est pas toujours renseignée, en
	 * particulier dans les petites communes.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleVoie2Etablissement() : ?string
	{
		return $this->_libelleVoie2Etablissement;
	}
	
	/**
	 * Sets cette variable désigne le code postal de l’adresse de
	 * l’établissement.
	 * Pour les adresses à l’étranger (codeCommuneEtablissement commence
	 * par 99), cette variable est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @param ?string $codePostal2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setCodePostal2Etablissement(?string $codePostal2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_codePostal2Etablissement = $codePostal2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable désigne le code postal de l’adresse de
	 * l’établissement.
	 * Pour les adresses à l’étranger (codeCommuneEtablissement commence
	 * par 99), cette variable est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @return ?string
	 */
	public function getCodePostal2Etablissement() : ?string
	{
		return $this->_codePostal2Etablissement;
	}
	
	/**
	 * Sets cette variable indique le libellé de la commune de l’adresse
	 * secondaire de l'établissement si celui-ci n’est pas à l’étranger.
	 * C’est un élément constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $libelleCommune2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setLibelleCommune2Etablissement(?string $libelleCommune2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_libelleCommune2Etablissement = $libelleCommune2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique le libellé de la commune de l’adresse
	 * secondaire de l'établissement si celui-ci n’est pas à l’étranger.
	 * C’est un élément constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleCommune2Etablissement() : ?string
	{
		return $this->_libelleCommune2Etablissement;
	}
	
	/**
	 * Sets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, cette variable est un élément constitutif de l’adresse
	 * secondaire pour les établissements situés sur le territoire étranger
	 * (la variable codeCommune2Etablissement est vide).
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $libelleCommuneEtranger2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setLibelleCommuneEtranger2Etablissement(?string $libelleCommuneEtranger2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_libelleCommuneEtranger2Etablissement = $libelleCommuneEtranger2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, cette variable est un élément constitutif de l’adresse
	 * secondaire pour les établissements situés sur le territoire étranger
	 * (la variable codeCommune2Etablissement est vide).
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleCommuneEtranger2Etablissement() : ?string
	{
		return $this->_libelleCommuneEtranger2Etablissement;
	}
	
	/**
	 * Sets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, la distribution spéciale reprend les éléments
	 * particuliers qui accompagnent l’adresse secondaire de distribution
	 * spéciale. C’est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative.
	 * 
	 * Longueur : 26
	 * 
	 * *Exemples de début de modalités attendues*
	 * BP : Boîte postale
	 * TSA : Tri par service à l'arrivée
	 * LP : Local postal
	 * RP : Référence postale
	 * SP : Secteur postal
	 * CP : Case postale
	 * CE : Case entreprise
	 * CS : Course spéciale
	 * POSTE RESTANTE
	 * CEDEX : Courrier d'entreprise à distribution exceptionnelle
	 * CIDEX : Courrier individuel à distribution exceptionnelle
	 * CASE, NIVEAU : Mots utilisés pour la distribution interne du courrier
	 * à LA DEFENSE
	 * CASIER : Utilisé pour le centre commercial des Quatre Temps
	 * SILIC, SENIA, MAREE, FLEURS… : Utilisés sur le site de Rungis
	 * 
	 * @param ?string $distributionSpeciale2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setDistributionSpeciale2Etablissement(?string $distributionSpeciale2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_distributionSpeciale2Etablissement = $distributionSpeciale2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, la distribution spéciale reprend les éléments
	 * particuliers qui accompagnent l’adresse secondaire de distribution
	 * spéciale. C’est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative.
	 * 
	 * Longueur : 26
	 * 
	 * *Exemples de début de modalités attendues*
	 * BP : Boîte postale
	 * TSA : Tri par service à l'arrivée
	 * LP : Local postal
	 * RP : Référence postale
	 * SP : Secteur postal
	 * CP : Case postale
	 * CE : Case entreprise
	 * CS : Course spéciale
	 * POSTE RESTANTE
	 * CEDEX : Courrier d'entreprise à distribution exceptionnelle
	 * CIDEX : Courrier individuel à distribution exceptionnelle
	 * CASE, NIVEAU : Mots utilisés pour la distribution interne du courrier
	 * à LA DEFENSE
	 * CASIER : Utilisé pour le centre commercial des Quatre Temps
	 * SILIC, SENIA, MAREE, FLEURS… : Utilisés sur le site de Rungis
	 * 
	 * @return ?string
	 */
	public function getDistributionSpeciale2Etablissement() : ?string
	{
		return $this->_distributionSpeciale2Etablissement;
	}
	
	/**
	 * Sets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, cette variable désigne le code de la commune de l’adresse
	 * secondaire de l’établissement, hors adresse à l'étranger. Le code
	 * commune correspond au code commune existant à la date de la mise à
	 * disposition : toute modification du code officiel géographique est
	 * répercutée sur la totalité des établissements (même ceux fermés)
	 * correspondant à ce code commune.
	 * Pour les établissements localisés à l'étranger, la variable
	 * codeCommune2Etablissement est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @param ?string $codeCommune2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setCodeCommune2Etablissement(?string $codeCommune2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_codeCommune2Etablissement = $codeCommune2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, cette variable désigne le code de la commune de l’adresse
	 * secondaire de l’établissement, hors adresse à l'étranger. Le code
	 * commune correspond au code commune existant à la date de la mise à
	 * disposition : toute modification du code officiel géographique est
	 * répercutée sur la totalité des établissements (même ceux fermés)
	 * correspondant à ce code commune.
	 * Pour les établissements localisés à l'étranger, la variable
	 * codeCommune2Etablissement est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @return ?string
	 */
	public function getCodeCommune2Etablissement() : ?string
	{
		return $this->_codeCommune2Etablissement;
	}
	
	/**
	 * Sets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, c’est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative.
	 * 
	 * Longueur : 9
	 * 
	 * @param ?string $codeCedex2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setCodeCedex2Etablissement(?string $codeCedex2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_codeCedex2Etablissement = $codeCedex2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, c’est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative.
	 * 
	 * Longueur : 9
	 * 
	 * @return ?string
	 */
	public function getCodeCedex2Etablissement() : ?string
	{
		return $this->_codeCedex2Etablissement;
	}
	
	/**
	 * Sets cette variable indique le libellé correspondant au code cedex de
	 * l’adresse secondaire de l'établissement si celui-ci est non null. Ce
	 * libellé est le libellé utilisé dans la ligne 6 d’adresse pour
	 * l’acheminement postal.
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $libelleCedex2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setLibelleCedex2Etablissement(?string $libelleCedex2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_libelleCedex2Etablissement = $libelleCedex2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique le libellé correspondant au code cedex de
	 * l’adresse secondaire de l'établissement si celui-ci est non null. Ce
	 * libellé est le libellé utilisé dans la ligne 6 d’adresse pour
	 * l’acheminement postal.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleCedex2Etablissement() : ?string
	{
		return $this->_libelleCedex2Etablissement;
	}
	
	/**
	 * Sets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, cette variable désigne le code du pays de localisation de
	 * l'adresse secondaire de l'établissement pour les adresses à
	 * l'étranger. La variable codePaysEtranger2Etablissement commence
	 * toujours par 99 si elle est renseignée. Les 3 caractères suivants sont
	 * le code du pays étranger.
	 * 
	 * Longueur : 5
	 * 
	 * @param ?string $codePaysEtranger2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setCodePaysEtranger2Etablissement(?string $codePaysEtranger2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_codePaysEtranger2Etablissement = $codePaysEtranger2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, cette variable désigne le code du pays de localisation de
	 * l'adresse secondaire de l'établissement pour les adresses à
	 * l'étranger. La variable codePaysEtranger2Etablissement commence
	 * toujours par 99 si elle est renseignée. Les 3 caractères suivants sont
	 * le code du pays étranger.
	 * 
	 * Longueur : 5
	 * 
	 * @return ?string
	 */
	public function getCodePaysEtranger2Etablissement() : ?string
	{
		return $this->_codePaysEtranger2Etablissement;
	}
	
	/**
	 * Sets cette variable indique le libellé du pays de localisation de
	 * l’adresse secondaire de l'établissement si celui-ci est à
	 * l’étranger.
	 * C’est un élément constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $libellePaysEtranger2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setLibellePaysEtranger2Etablissement(?string $libellePaysEtranger2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_libellePaysEtranger2Etablissement = $libellePaysEtranger2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique le libellé du pays de localisation de
	 * l’adresse secondaire de l'établissement si celui-ci est à
	 * l’étranger.
	 * C’est un élément constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibellePaysEtranger2Etablissement() : ?string
	{
		return $this->_libellePaysEtranger2Etablissement;
	}
	
	/**
	 * Sets date de début de la période au cours de laquelle toutes les
	 * variables historisées de l'établissement restent inchangées.
	 * La date 1900-01-01 signifie : date non déterminée.
	 * dateDebut peut-être vide uniquement pour les établissements des
	 * unités purgées (cf. variable unitePurgeeUniteLegale dans le descriptif
	 * des variables du fichier StockUniteLegale).
	 * La date de début de la période la plus ancienne ne correspond pas
	 * obligatoirement à la date de création de l'établissement, certaines
	 * variables historisées pouvant posséder des dates de début soit au
	 * 1900-01-01 soit antérieures à la date de création.
	 * 
	 * @param ?DateTimeInterface $dateDebut
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setDateDebut(?DateTimeInterface $dateDebut) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_dateDebut = $dateDebut;
		
		return $this;
	}
	
	/**
	 * Gets date de début de la période au cours de laquelle toutes les
	 * variables historisées de l'établissement restent inchangées.
	 * La date 1900-01-01 signifie : date non déterminée.
	 * dateDebut peut-être vide uniquement pour les établissements des
	 * unités purgées (cf. variable unitePurgeeUniteLegale dans le descriptif
	 * des variables du fichier StockUniteLegale).
	 * La date de début de la période la plus ancienne ne correspond pas
	 * obligatoirement à la date de création de l'établissement, certaines
	 * variables historisées pouvant posséder des dates de début soit au
	 * 1900-01-01 soit antérieures à la date de création.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateDebut() : ?DateTimeInterface
	{
		return $this->_dateDebut;
	}
	
	/**
	 * Sets lors de son inscription au répertoire, un établissement est, sauf
	 * exception, à l’état « Actif ». Le passage à l’état « Fermé
	 * » découle de la prise en compte d’une déclaration de fermeture.
	 * À noter qu’un établissement fermé peut être rouvert.
	 * En règle générale, la première période d’historique d’un
	 * établissement correspond à un etatAdministratifUniteLegale égal à «
	 * Actif ». Toutefois, l'état administratif peut être à null (première
	 * date de début de l'état postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * @param ApiFrInseeSireneEtatAdministratifInterface $etatAdministratifEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setEtatAdministratifEtablissement(ApiFrInseeSireneEtatAdministratifInterface $etatAdministratifEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_etatAdministratifEtablissement = $etatAdministratifEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets lors de son inscription au répertoire, un établissement est, sauf
	 * exception, à l’état « Actif ». Le passage à l’état « Fermé
	 * » découle de la prise en compte d’une déclaration de fermeture.
	 * À noter qu’un établissement fermé peut être rouvert.
	 * En règle générale, la première période d’historique d’un
	 * établissement correspond à un etatAdministratifUniteLegale égal à «
	 * Actif ». Toutefois, l'état administratif peut être à null (première
	 * date de début de l'état postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * @return ApiFrInseeSireneEtatAdministratifInterface
	 */
	public function getEtatAdministratifEtablissement() : ApiFrInseeSireneEtatAdministratifInterface
	{
		return $this->_etatAdministratifEtablissement;
	}
	
	/**
	 * Sets les trois variables enseigne1Etablissement, enseigne2Etablissement
	 * et enseigne3Etablissement contiennent la ou les enseignes de
	 * l'établissement.
	 * L'enseigne identifie l'emplacement ou le local dans lequel est exercée
	 * l'activité. Un établissement peut posséder une enseigne, plusieurs
	 * enseignes ou aucune.
	 * 
	 * L'analyse des enseignes et de son découpage en trois variables dans
	 * Sirene montre deux cas possibles : soit les 3 champs concernent 3
	 * enseignes bien distinctes, soit ces trois champs correspondent au
	 * découpage de l'enseigne qui est déclarée dans la liasse (sur un seul
	 * champ) avec une continuité des trois champs.
	 * 
	 * *Exemples*
	 * SIRET=53053581400178
	 * "enseigne1Etablissement": "LES SERRURIERS DES YVELINES LES VITRIERS DES
	 * YVELI",
	 * "enseigne2Etablissement": "NES LES CHAUFFAGISTES DES YVELINES LES
	 * PLATRIERS D",
	 * "enseigne3Etablissement": "ES YVELINES LES ELECTRICIENS DES
	 * YVELINES.…"
	 * 
	 * SIRET=05439181800033
	 * "enseigne1Etablissement": "HALTE OCCASIONS",
	 * "enseigne2Etablissement": "OUTRE-MER LOCATION",
	 * "enseigne3Etablissement": "OUTRE-MER TRANSIT".
	 * 
	 * Cette variable est historisée.
	 * 
	 * Longueur : 50
	 * 
	 * @param ?string $enseigne1Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setEnseigne1Etablissement(?string $enseigne1Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_enseigne1Etablissement = $enseigne1Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets les trois variables enseigne1Etablissement, enseigne2Etablissement
	 * et enseigne3Etablissement contiennent la ou les enseignes de
	 * l'établissement.
	 * L'enseigne identifie l'emplacement ou le local dans lequel est exercée
	 * l'activité. Un établissement peut posséder une enseigne, plusieurs
	 * enseignes ou aucune.
	 * 
	 * L'analyse des enseignes et de son découpage en trois variables dans
	 * Sirene montre deux cas possibles : soit les 3 champs concernent 3
	 * enseignes bien distinctes, soit ces trois champs correspondent au
	 * découpage de l'enseigne qui est déclarée dans la liasse (sur un seul
	 * champ) avec une continuité des trois champs.
	 * 
	 * *Exemples*
	 * SIRET=53053581400178
	 * "enseigne1Etablissement": "LES SERRURIERS DES YVELINES LES VITRIERS DES
	 * YVELI",
	 * "enseigne2Etablissement": "NES LES CHAUFFAGISTES DES YVELINES LES
	 * PLATRIERS D",
	 * "enseigne3Etablissement": "ES YVELINES LES ELECTRICIENS DES
	 * YVELINES.…"
	 * 
	 * SIRET=05439181800033
	 * "enseigne1Etablissement": "HALTE OCCASIONS",
	 * "enseigne2Etablissement": "OUTRE-MER LOCATION",
	 * "enseigne3Etablissement": "OUTRE-MER TRANSIT".
	 * 
	 * Cette variable est historisée.
	 * 
	 * Longueur : 50
	 * 
	 * @return ?string
	 */
	public function getEnseigne1Etablissement() : ?string
	{
		return $this->_enseigne1Etablissement;
	}
	
	/**
	 * Sets 2e ligne de l'enseigne.
	 * 
	 * @param ?string $enseigne2Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setEnseigne2Etablissement(?string $enseigne2Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_enseigne2Etablissement = $enseigne2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets 2e ligne de l'enseigne.
	 * 
	 * @return ?string
	 */
	public function getEnseigne2Etablissement() : ?string
	{
		return $this->_enseigne2Etablissement;
	}
	
	/**
	 * Sets 3e ligne de l'enseigne.
	 * 
	 * @param ?string $enseigne3Etablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setEnseigne3Etablissement(?string $enseigne3Etablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_enseigne3Etablissement = $enseigne3Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets 3e ligne de l'enseigne.
	 * 
	 * @return ?string
	 */
	public function getEnseigne3Etablissement() : ?string
	{
		return $this->_enseigne3Etablissement;
	}
	
	/**
	 * Sets cette variable désigne le nom sous lequel l'établissement est
	 * connu du grand public. Cet élément d'identification de
	 * l'établissement a été enregistré au niveau établissement depuis
	 * l'application de la norme d'échanges CFE de 2008. Avant la norme 2008,
	 * la dénomination usuelle était enregistrée au niveau de l'unité
	 * légale sur trois champs (cf. variables denominationUsuelle1UniteLegale
	 * à denominationUsuelle3UniteLegale dans le descriptif des variables du
	 * fichier StockUniteLegale).
	 * Cette variable est historisée.
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $denominationUsuelleEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setDenominationUsuelleEtablissement(?string $denominationUsuelleEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_denominationUsuelleEtablissement = $denominationUsuelleEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable désigne le nom sous lequel l'établissement est
	 * connu du grand public. Cet élément d'identification de
	 * l'établissement a été enregistré au niveau établissement depuis
	 * l'application de la norme d'échanges CFE de 2008. Avant la norme 2008,
	 * la dénomination usuelle était enregistrée au niveau de l'unité
	 * légale sur trois champs (cf. variables denominationUsuelle1UniteLegale
	 * à denominationUsuelle3UniteLegale dans le descriptif des variables du
	 * fichier StockUniteLegale).
	 * Cette variable est historisée.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getDenominationUsuelleEtablissement() : ?string
	{
		return $this->_denominationUsuelleEtablissement;
	}
	
	/**
	 * Sets lors de son inscription au répertoire, l’Insee attribue à tout
	 * établissement un code dit « APE » sur la base de la description de
	 * l’activité principale faite par le déclarant. Ce code est modifiable
	 * au cours de la vie de l’établissement en fonction des déclarations
	 * de l’exploitant.
	 * Pour chaque établissement, il existe à un instant donné un seul code
	 * « APE ». Il est attribué selon la nomenclature en vigueur. La
	 * nomenclature en vigueur est la Naf Rév2 et ce depuis le 1 er Janvier
	 * 2008.
	 * Chaque code comporte 2 chiffres, un point, 2 chiffres et une lettre.
	 * Tous les établissements actifs au 01/01/2008 ont eu leur code APE
	 * recodé dans la nouvelle nomenclature, ainsi de très nombreux
	 * établissements ont une période débutant à cette date.
	 * Au moment de la déclaration de l’entreprise, il peut arriver que
	 * l’Insee ne soit pas en mesure d’attribuer le bon code APE : la
	 * modalité 00.00Z peut alors être affectée provisoirement.
	 * 
	 * *Historique*
	 * Le code APE est historisé depuis le 01/01/2005.
	 * La règle d’historisation des données d’activité est la suivante :
	 *  - Pour les établissements fermés avant le 31/12/2004, seul le dernier
	 * code activité connu figure, dans la nomenclature en vigueur à la date
	 * de fermeture.
	 *  - Pour les établissements ouverts après le 01/01/2005 et fermés
	 * avant le 31/12/2007, l’historique des codes attribués sur la période
	 * est disponible.
	 *  - Pour les établissements ouverts après le 01/01/2005 et toujours
	 * ouverts le 01/01/2008, l’historique intègre le changement de
	 * nomenclature.
	 *  - Pour les établissements ouverts après le 01/01/2008, l’historique
	 * comprend les modifications apportées au cours de la vie de
	 * l’établissement.
	 * L'APE peut être à null (cas des unités purgées - pour des raisons de
	 * capacité de stockage des données, les données concernant les
	 * entreprises cessées avant le 31/12/2002 ont été purgées -, première
	 * date de début de l'APE postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * Longueur : 6
	 * 
	 * @param ?string $activitePrincipaleEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setActivitePrincipaleEtablissement(?string $activitePrincipaleEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_activitePrincipaleEtablissement = $activitePrincipaleEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets lors de son inscription au répertoire, l’Insee attribue à tout
	 * établissement un code dit « APE » sur la base de la description de
	 * l’activité principale faite par le déclarant. Ce code est modifiable
	 * au cours de la vie de l’établissement en fonction des déclarations
	 * de l’exploitant.
	 * Pour chaque établissement, il existe à un instant donné un seul code
	 * « APE ». Il est attribué selon la nomenclature en vigueur. La
	 * nomenclature en vigueur est la Naf Rév2 et ce depuis le 1 er Janvier
	 * 2008.
	 * Chaque code comporte 2 chiffres, un point, 2 chiffres et une lettre.
	 * Tous les établissements actifs au 01/01/2008 ont eu leur code APE
	 * recodé dans la nouvelle nomenclature, ainsi de très nombreux
	 * établissements ont une période débutant à cette date.
	 * Au moment de la déclaration de l’entreprise, il peut arriver que
	 * l’Insee ne soit pas en mesure d’attribuer le bon code APE : la
	 * modalité 00.00Z peut alors être affectée provisoirement.
	 * 
	 * *Historique*
	 * Le code APE est historisé depuis le 01/01/2005.
	 * La règle d’historisation des données d’activité est la suivante :
	 *  - Pour les établissements fermés avant le 31/12/2004, seul le dernier
	 * code activité connu figure, dans la nomenclature en vigueur à la date
	 * de fermeture.
	 *  - Pour les établissements ouverts après le 01/01/2005 et fermés
	 * avant le 31/12/2007, l’historique des codes attribués sur la période
	 * est disponible.
	 *  - Pour les établissements ouverts après le 01/01/2005 et toujours
	 * ouverts le 01/01/2008, l’historique intègre le changement de
	 * nomenclature.
	 *  - Pour les établissements ouverts après le 01/01/2008, l’historique
	 * comprend les modifications apportées au cours de la vie de
	 * l’établissement.
	 * L'APE peut être à null (cas des unités purgées - pour des raisons de
	 * capacité de stockage des données, les données concernant les
	 * entreprises cessées avant le 31/12/2002 ont été purgées -, première
	 * date de début de l'APE postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * Longueur : 6
	 * 
	 * @return ?string
	 */
	public function getActivitePrincipaleEtablissement() : ?string
	{
		return $this->_activitePrincipaleEtablissement;
	}
	
	/**
	 * Sets cette variable indique la nomenclature d’activité correspondant
	 * à la variable activitePrincipaleEtablissement (cf.
	 * activitePrincipaleEtablissement).
	 * 
	 * @param ?ApiFrInseeSireneNomenclatureApeInterface $nomenclatureActivitePrincipaleEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setNomenclatureActivitePrincipaleEtablissement(?ApiFrInseeSireneNomenclatureApeInterface $nomenclatureActivitePrincipaleEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_nomenclatureActivitePrincipaleEtablissement = $nomenclatureActivitePrincipaleEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique la nomenclature d’activité correspondant
	 * à la variable activitePrincipaleEtablissement (cf.
	 * activitePrincipaleEtablissement).
	 * 
	 * @return ?ApiFrInseeSireneNomenclatureApeInterface
	 */
	public function getNomenclatureActivitePrincipaleEtablissement() : ?ApiFrInseeSireneNomenclatureApeInterface
	{
		return $this->_nomenclatureActivitePrincipaleEtablissement;
	}
	
	/**
	 * Sets lors de sa formalité d’ouverture, le déclarant indique si
	 * l’établissement aura ou non des employés. Par la suite, le
	 * déclarant peut également faire des déclarations de prise d’emploi
	 * et de fin d’emploi. La prise en compte d’une déclaration de prise
	 * d’emploi bascule immédiatement l’établissement en « employeur ».
	 * Inversement, lorsqu’une déclaration de fin d’emploi est traitée,
	 * l’établissement devient « non employeur ». Pour chaque
	 * établissement, il existe à un instant donné un seul code « employeur
	 * ».
	 * Cette variable est historisée pour les établissements qui étaient
	 * ouverts en 2005 et pour ceux ouverts ultérieurement.
	 * 
	 * @param ?bool $caractereEmployeurEtablissement
	 * @return ApiFrInseeSireneEtablissementInterface
	 */
	public function setCaractereEmployeurEtablissement(?bool $caractereEmployeurEtablissement) : ApiFrInseeSireneEtablissementInterface
	{
		$this->_caractereEmployeurEtablissement = $caractereEmployeurEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets lors de sa formalité d’ouverture, le déclarant indique si
	 * l’établissement aura ou non des employés. Par la suite, le
	 * déclarant peut également faire des déclarations de prise d’emploi
	 * et de fin d’emploi. La prise en compte d’une déclaration de prise
	 * d’emploi bascule immédiatement l’établissement en « employeur ».
	 * Inversement, lorsqu’une déclaration de fin d’emploi est traitée,
	 * l’établissement devient « non employeur ». Pour chaque
	 * établissement, il existe à un instant donné un seul code « employeur
	 * ».
	 * Cette variable est historisée pour les établissements qui étaient
	 * ouverts en 2005 et pour ceux ouverts ultérieurement.
	 * 
	 * @return ?bool
	 */
	public function hasCaractereEmployeurEtablissement() : ?bool
	{
		return $this->_caractereEmployeurEtablissement;
	}
	
}
