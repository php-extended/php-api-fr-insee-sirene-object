<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;

/**
 * InseeSireneEtatAdministratifParser class file.
 * 
 * This class parses insee sirene etat administratif data into source objects.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiFrInseeSireneEtatAdministratif>
 */
class ApiFrInseeSireneEtatAdministratifParser extends AbstractParser
{
	
	/**
	 * The etats administratifs.
	 * 
	 * @var array<string, ApiFrInseeSireneEtatAdministratif>
	 */
	protected array $_etatsAdministratifs = [];
	
	/**
	 * Builds a new InseeSireneEtatAdministratifParser with the given data.
	 * 
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct()
	{
		$reifier = new Reifier();
		$filePath = \dirname(__DIR__).'/data/etat_administratif.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		$iterator = $reifier->reifyIterator(ApiFrInseeSireneEtatAdministratif::class, $iterator);
		
		/** @var ApiFrInseeSireneEtatAdministratif $etatAdministratif */
		foreach($iterator as $etatAdministratif)
		{
			$this->_etatsAdministratifs[(string) $etatAdministratif->getCode()] = $etatAdministratif;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiFrInseeSireneEtatAdministratif
	{
		$data = (string) $data;
		if(isset($this->_etatsAdministratifs[$data]))
		{
			return $this->_etatsAdministratifs[$data];
		}
		
		throw new ParseException(ApiFrInseeSireneEtatAdministratif::class, $data, 0);
	}
	
}
