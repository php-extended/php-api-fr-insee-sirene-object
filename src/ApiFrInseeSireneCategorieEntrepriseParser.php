<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;

/**
 * InseeSireneCategorieEntrepriseParser class file.
 * 
 * This class parses insee categorie entreprise data into source objects.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiFrInseeSireneCategorieEntreprise>
 * @SuppressWarnings("PHPMD.LongClassName")
 */
class ApiFrInseeSireneCategorieEntrepriseParser extends AbstractParser
{
	
	/**
	 * The categories entreprises.
	 * 
	 * @var array<string, ApiFrInseeSireneCategorieEntreprise>
	 */
	protected array $_categoriesEntreprises = [];
	
	/**
	 * Builds a new InseeSireneCategorieEntrepriseParser with the given data.
	 * 
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct()
	{
		$reifier = new Reifier();
		$filePath = \dirname(__DIR__).'/data/categorie_entreprise.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		$iterator = $reifier->reifyIterator(ApiFrInseeSireneCategorieEntreprise::class, $iterator);
		
		/** @var ApiFrInseeSireneCategorieEntreprise $categorieEntreprise */
		foreach($iterator as $categorieEntreprise)
		{
			$this->_categoriesEntreprises[(string) $categorieEntreprise->getCode()] = $categorieEntreprise;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiFrInseeSireneCategorieEntreprise
	{
		$data = (string) $data;
		if(isset($this->_categoriesEntreprises[$data]))
		{
			return $this->_categoriesEntreprises[$data];
		}
		
		throw new ParseException(ApiFrInseeSireneCategorieEntreprise::class, $data, 0);
	}
	
}
