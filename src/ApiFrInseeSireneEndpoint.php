<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use Iterator;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoie;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoieParser;
use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\CsvStringDataProvider;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;
use Throwable;

/**
 * ApiFrInseeSireneEndpoint class file.
 * 
 * This class represents the driver to get bulk files from the insee (which
 * are deposed on data.gouv.fr).
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiFrInseeSireneEndpoint implements ApiFrInseeSireneEndpointInterface
{
	
	public const HOST = 'https://files.data.gouv.fr/';
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The reifier.
	 *
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * The temp directory path.
	 *
	 * @var string
	 */
	protected string $_tempDirectoryPath;
	
	/**
	 * Builds a new InseeBanEndpoint with the given http endpoint.
	 *
	 * @param string $tempDirectoryPath
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?ReifierInterface $reifier
	 * @throws InvalidArgumentException
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct(
		string $tempDirectoryPath,
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?ReifierInterface $reifier = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_reifier = $reifier ?? new Reifier();
		
		$realpath = \realpath($tempDirectoryPath);
		if(false === $realpath)
		{
			$message = 'The given temp directory path "{dir}" does not exist on the filesystem';
			$context = ['{dir}' => $tempDirectoryPath];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		if(!\is_dir($realpath))
		{
			$message = 'The given directory "{dir}" is not a real directory';
			$context = ['{dir}' => $realpath];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		if(!\is_writable($realpath))
		{
			$message = 'The given directory "{dir}" is not writeable';
			$context = ['{dir}' => $realpath];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		$this->_tempDirectoryPath = $realpath;
		
		$configuration = $this->_reifier->getConfiguration();
		$configuration->setParser(ApiFrInseeSireneCategorieEntreprise::class, new ApiFrInseeSireneCategorieEntrepriseParser());
		$configuration->setParser(ApiFrInseeSireneEtatAdministratif::class, new ApiFrInseeSireneEtatAdministratifParser());
		$configuration->setParser(ApiFrInseeSireneGender::class, new ApiFrInseeSireneGenderParser());
		$configuration->setParser(ApiFrInseeSireneNomenclatureApe::class, new ApiFrInseeSireneNomenclatureApeParser());
		$configuration->setParser(ApiFrInseeSireneTrancheEffectifs::class, new ApiFrInseeSireneTrancheEffectifsParser());
		$configuration->setParser(ApiFrInseeBanTypeVoie::class, new ApiFrInseeBanTypeVoieParser());
		
		$configuration->addDateTimeFormat(ApiFrInseeSireneUniteLegale::class, 'dateCreationUniteLegale', ['Y-m-d']);
		$configuration->addDateTimeFormat(ApiFrInseeSireneUniteLegale::class, 'dateDebut', ['Y-m-d']);
		$configuration->addDateTimeFormat(ApiFrInseeSireneUniteLegale::class, 'dateDernierTraitementUniteLegale', ['Y-m-d\\TH:i:s']);
		$configuration->addDateTimeFormat(ApiFrInseeSireneUniteLegaleHistorique::class, 'dateDebut', ['Y-m-d']);
		$configuration->addDateTimeFormat(ApiFrInseeSireneUniteLegaleHistorique::class, 'dateFin', ['Y-m-d']);
		$configuration->addDateTimeFormat(ApiFrInseeSireneEtablissement::class, 'dateCreationEtablissement', ['Y-m-d']);
		$configuration->addDateTimeFormat(ApiFrInseeSireneEtablissement::class, 'dateDebut', ['Y-m-d']);
		$configuration->addDateTimeFormat(ApiFrInseeSireneEtablissement::class, 'dateDernierTraitementEtablissement', ['Y-m-d\\TH:i:s']);
		$configuration->addDateTimeFormat(ApiFrInseeSireneEtablissementHistorique::class, 'dateDebut', ['Y-m-d']);
		$configuration->addDateTimeFormat(ApiFrInseeSireneEtablissementHistorique::class, 'dateFin', ['Y-m-d']);
		$configuration->addDateTimeFormat(ApiFrInseeSireneSuccession::class, 'dateDernierTraitementLienSuccession', ['Y-m-d\\TH:i:s']);
		
		$configuration->addFieldsEmptyAsNull(ApiFrInseeSireneUniteLegale::class, ['dateCreation', 'dateDernierTraitement', 'sexe', 'trancheEffectif', 'categorie', 'dateDebut', 'etatAdministratif', 'nomenclature']);
		$configuration->addFieldsEmptyAsNull(ApiFrInseeSireneUniteLegaleHistorique::class, ['dateFin', 'dateDebut', 'etatAdministratif', 'nomenclature']);
		$configuration->addFieldsEmptyAsNull(ApiFrInseeSireneEtablissement::class, ['dateCreation', 'dateDernierTraitement', 'trancheEffectifs', 'typeVoie', 'typeVoie2', 'dateDebut', 'etatAdministratif', 'nomenclature']);
		$configuration->addFieldsEmptyAsNull(ApiFrInseeSireneEtablissementHistorique::class, ['dateFin', 'dateDebut', 'etatAdministratif', 'nomenclature']);
		
		$configuration->addFieldsAllowedToFail(ApiFrInseeSireneUniteLegale::class, ['categorieEntreprise', 'dateCreationUniteLegale', 'dateDernierTraitementUniteLegale', 'nomenclatureActivitePrincipaleUniteLegale', 'sexeUniteLegale', 'trancheEffectifsUniteLegale']);
		$configuration->addFieldsAllowedToFail(ApiFrInseeSireneUniteLegaleHistorique::class, ['etatAdministratifUniteLegale', 'nomenclatureActivitePrincipaleUniteLegale']);
		$configuration->addFieldsAllowedToFail(ApiFrInseeSireneEtablissement::class, ['categorieEntreprise', 'dateCreationEtablissement', 'dateDernierTraitementEtablissement', 'nomenclatureActivitePrincipaleEtablissement', 'trancheEffectifsEtablissement', 'typeVoieEtablissement', 'typeVoie2Etablissement']);
		$configuration->addFieldsAllowedToFail(ApiFrInseeSireneEtablissementHistorique::class, ['etatAdministratifEtablissement', 'nomenclatureActivitePrincipaleEtablissement']);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface::getLatestUploadDate()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function getLatestUploadDate() : DateTimeInterface
	{
		$url = 'https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$response = $this->_httpClient->sendRequest($request);
			$data = $response->getBody()->__toString();
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		$needle = '<script id="json_ld" type="application/ld+json">';
		$pos = \mb_strpos($data, $needle);
		if(false === $pos)
		{
			$message = 'Failed to find json ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$needle2 = '</script>';
		$pos2 = \mb_strpos($data, $needle2, $pos + 1);
		if(false === $pos2)
		{
			$message = 'Failed to find end of json ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$jsonStr = \mb_substr($data, $pos + (int) \mb_strlen($needle), $pos2 - $pos - (int) \mb_strlen($needle));
		if(empty($jsonStr))
		{
			$message = 'Failed to extract json ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$json = \json_decode($jsonStr, true);
		if(empty($json) || !\is_array($json))
		{
			$message = 'Failed to decode json ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		if(!isset($json['dateModified']) || !\is_string($json['dateModified']))
		{
			$message = 'Failed to find dateModified field in json ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$date = DateTimeImmutable::createFromFormat('Y-m-d\\TH:i:s.u', $json['dateModified']);
		if(false === $date)
		{
			$message = 'Failed to parse date modified field in json ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		return $date;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface::getLatestStockUniteLegaleIterator()
	 */
	public function getLatestStockUniteLegaleIterator() : Iterator
	{
		$url = self::HOST.'insee-sirene/StockUniteLegale_utf8.zip';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$request = $request->withAddedHeader('X-Php-Download-File', $this->_tempDirectoryPath.\DIRECTORY_SEPARATOR.'StockUniteLegale_utf8.zip');
			$response = $this->_httpClient->sendRequest($request);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		try
		{
			$xDownloadedFile = $response->getHeaderLine('X-Php-Uncompressed-File');
			if(empty($xDownloadedFile))
			{
				return $this->_reifier->reifyIterator(
					ApiFrInseeSireneUniteLegale::class,
					(new CsvStringDataProvider(
						$response->getBody()->__toString(),
						true,
						',',
						'"',
						'\\',
					))->provideIterator(),
				);
			}
			
			$iterator = new CsvFileDataIterator($xDownloadedFile, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
			
			return $this->_reifier->reifyIterator(ApiFrInseeSireneUniteLegale::class, $iterator);
		}
		catch(ReificationThrowable|UnprovidableThrowable $exc)
		{
			$message = 'Failed to transform data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -3, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface::getLatestStockUniteLegaleHistoricIterator()
	 */
	public function getLatestStockUniteLegaleHistoricIterator() : Iterator
	{
		$url = self::HOST.'insee-sirene/StockUniteLegaleHistorique_utf8.zip';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$request = $request->withAddedHeader('X-Php-Download-File', $this->_tempDirectoryPath.\DIRECTORY_SEPARATOR.'StockUniteLegaleHistorique_utf8.zip');
			$response = $this->_httpClient->sendRequest($request);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		try
		{
			$xDownloadedFile = $response->getHeaderLine('X-Php-Uncompressed-File');
			if(empty($xDownloadedFile))
			{
				return $this->_reifier->reifyIterator(
					ApiFrInseeSireneUniteLegaleHistorique::class,
					(new CsvStringDataProvider(
						$response->getBody()->__toString(),
						true,
						',',
						'"',
						'\\',
					))->provideIterator(),
				);
			}
			
			$iterator = new CsvFileDataIterator($xDownloadedFile, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
			
			return $this->_reifier->reifyIterator(ApiFrInseeSireneUniteLegaleHistorique::class, $iterator);
		}
		catch(ReificationThrowable|UnprovidableThrowable $exc)
		{
			$message = 'Failed to transform data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -3, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface::getLatestStockEtablissementIterator()
	 */
	public function getLatestStockEtablissementIterator() : Iterator
	{
		$url = self::HOST.'insee-sirene/StockEtablissement_utf8.zip';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$request = $request->withAddedHeader('X-Php-Download-File', $this->_tempDirectoryPath.\DIRECTORY_SEPARATOR.'StockEtablissement_utf8.zip');
			$response = $this->_httpClient->sendRequest($request);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		try
		{
			$xDownloadedFile = $response->getHeaderLine('X-Php-Uncompressed-File');
			if(empty($xDownloadedFile))
			{
				return $this->_reifier->reifyIterator(
					ApiFrInseeSireneEtablissement::class,
					(new CsvStringDataProvider(
						$response->getBody()->__toString(),
						true,
						',',
						'"',
						'\\',
					))->provideIterator(),
				);
			}
			
			$iterator = new CsvFileDataIterator($xDownloadedFile, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
			
			return $this->_reifier->reifyIterator(ApiFrInseeSireneEtablissement::class, $iterator);
		}
		catch(ReificationThrowable|UnprovidableThrowable $exc)
		{
			$message = 'Failed to transform data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -3, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface::getLatestStockEtablissementHistoricIterator()
	 */
	public function getLatestStockEtablissementHistoricIterator() : Iterator
	{
		$url = self::HOST.'insee-sirene/StockEtablissementHistorique_utf8.zip';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$request = $request->withAddedHeader('X-Php-Download-File', $this->_tempDirectoryPath.\DIRECTORY_SEPARATOR.'StockEtablissementHistorique_utf8.zip');
			$response = $this->_httpClient->sendRequest($request);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		try
		{
			$xDownloadedFile = $response->getHeaderLine('X-Php-Uncompressed-File');
			if(empty($xDownloadedFile))
			{
				return $this->_reifier->reifyIterator(
					ApiFrInseeSireneEtablissementHistorique::class,
					(new CsvStringDataProvider(
						$response->getBody()->__toString(),
						true,
						',',
						'"',
						'\\',
					))->provideIterator(),
				);
			}
			
			$iterator = new CsvFileDataIterator($xDownloadedFile, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
			
			return $this->_reifier->reifyIterator(ApiFrInseeSireneEtablissementHistorique::class, $iterator);
		}
		catch(ReificationThrowable|UnprovidableThrowable $exc)
		{
			$message = 'Failed to transform data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -3, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface::getLatestStockSuccessionIterator()
	 */
	public function getLatestStockSuccessionIterator() : Iterator
	{
		$url = self::HOST.'insee-sirene/StockEtablissementLiensSuccession_utf8.zip';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$request = $request->withAddedHeader('X-Php-Download-File', $this->_tempDirectoryPath.\DIRECTORY_SEPARATOR.'StockEtablissementLiensSuccession_utf8.zip');
			$response = $this->_httpClient->sendRequest($request);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		try
		{
			$xDownloadedFile = $response->getHeaderLine('X-Php-Uncompressed-File');
			if(empty($xDownloadedFile))
			{
				return $this->_reifier->reifyIterator(
					ApiFrInseeSireneSuccession::class,
					(new CsvStringDataProvider(
						$response->getBody()->__toString(),
						true,
						',',
						'"',
						'\\',
					))->provideIterator(),
				);
			}
			
			$iterator = new CsvFileDataIterator($xDownloadedFile, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
			
			return $this->_reifier->reifyIterator(ApiFrInseeSireneSuccession::class, $iterator);
		}
		catch(ReificationThrowable|UnprovidableThrowable $exc)
		{
			$message = 'Failed to transform data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -3, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface::getCategorieEntrepriseIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCategorieEntrepriseIterator() : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/categorie_entreprise.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->_reifier->reifyIterator(ApiFrInseeSireneCategorieEntreprise::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface::getEtatAdministratifIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getEtatAdministratifIterator() : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/etat_administratif.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->_reifier->reifyIterator(ApiFrInseeSireneEtatAdministratif::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface::getGenderIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getGenderIterator() : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/gender.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->_reifier->reifyIterator(ApiFrInseeSireneGender::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface::getNomenclatureApeIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNomenclatureApeIterator() : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/nomenclature_ape.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->_reifier->reifyIterator(ApiFrInseeSireneNomenclatureApe::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeSirene\ApiFrInseeSireneEndpointInterface::getTrancheEffectifIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getTrancheEffectifIterator() : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/tranche_effectif.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->_reifier->reifyIterator(ApiFrInseeSireneTrancheEffectifs::class, $iterator);
	}
	
}
