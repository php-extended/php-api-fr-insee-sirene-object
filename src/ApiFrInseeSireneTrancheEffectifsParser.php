<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;

/**
 * InseeSireneTrancheEffectifsParser class file.
 * 
 * This class parses insee sirene tranche effectifs data into source objects.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiFrInseeSireneTrancheEffectifs>
 */
class ApiFrInseeSireneTrancheEffectifsParser extends AbstractParser
{
	
	/**
	 * The tranche effectifs.
	 * 
	 * @var array<string, ApiFrInseeSireneTrancheEffectifs>
	 */
	protected array $_trancheEffectifs = [];
	
	/**
	 * Builds a new InseeSireneTrancheEffectifsParser with this given data.
	 * 
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct()
	{
		$reifier = new Reifier();
		$filePath = \dirname(__DIR__).'/data/tranche_effectif.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		$iterator = $reifier->reifyIterator(ApiFrInseeSireneTrancheEffectifs::class, $iterator);
		
		/** @var ApiFrInseeSireneTrancheEffectifs $trancheEffectifs */
		foreach($iterator as $trancheEffectifs)
		{
			$this->_trancheEffectifs[(string) $trancheEffectifs->getCode()] = $trancheEffectifs;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiFrInseeSireneTrancheEffectifs
	{
		$data = (string) $data;
		if(isset($this->_trancheEffectifs[$data]))
		{
			return $this->_trancheEffectifs[$data];
		}
		
		throw new ParseException(ApiFrInseeSireneTrancheEffectifs::class, $data, 0);
	}
	
}
