<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

/**
 * ApiFrInseeSireneGender class file.
 * 
 * This is a simple implementation of the ApiFrInseeSireneGenderInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeSireneGender implements ApiFrInseeSireneGenderInterface
{
	
	/**
	 * The id of the gender.
	 * 
	 * @var int
	 */
	protected int $_id;
	
	/**
	 * The code of the gender.
	 * 
	 * @var string
	 */
	protected string $_code;
	
	/**
	 * The name of the gender.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Constructor for ApiFrInseeSireneGender with private members.
	 * 
	 * @param int $id
	 * @param string $code
	 * @param string $name
	 */
	public function __construct(int $id, string $code, string $name)
	{
		$this->setId($id);
		$this->setCode($code);
		$this->setName($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the gender.
	 * 
	 * @param int $id
	 * @return ApiFrInseeSireneGenderInterface
	 */
	public function setId(int $id) : ApiFrInseeSireneGenderInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the gender.
	 * 
	 * @return int
	 */
	public function getId() : int
	{
		return $this->_id;
	}
	
	/**
	 * Sets the code of the gender.
	 * 
	 * @param string $code
	 * @return ApiFrInseeSireneGenderInterface
	 */
	public function setCode(string $code) : ApiFrInseeSireneGenderInterface
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * Gets the code of the gender.
	 * 
	 * @return string
	 */
	public function getCode() : string
	{
		return $this->_code;
	}
	
	/**
	 * Sets the name of the gender.
	 * 
	 * @param string $name
	 * @return ApiFrInseeSireneGenderInterface
	 */
	public function setName(string $name) : ApiFrInseeSireneGenderInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the name of the gender.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
}
