<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

/**
 * ApiFrInseeSireneNomenclatureApe class file.
 * 
 * This is a simple implementation of the
 * ApiFrInseeSireneNomenclatureApeInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeSireneNomenclatureApe implements ApiFrInseeSireneNomenclatureApeInterface
{
	
	/**
	 * The identifier of the nomenclature.
	 * 
	 * @var int
	 */
	protected int $_id;
	
	/**
	 * The code of the nomenclature.
	 * 
	 * @var string
	 */
	protected string $_code;
	
	/**
	 * The name of the nomenclature.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Constructor for ApiFrInseeSireneNomenclatureApe with private members.
	 * 
	 * @param int $id
	 * @param string $code
	 * @param string $name
	 */
	public function __construct(int $id, string $code, string $name)
	{
		$this->setId($id);
		$this->setCode($code);
		$this->setName($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the identifier of the nomenclature.
	 * 
	 * @param int $id
	 * @return ApiFrInseeSireneNomenclatureApeInterface
	 */
	public function setId(int $id) : ApiFrInseeSireneNomenclatureApeInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the identifier of the nomenclature.
	 * 
	 * @return int
	 */
	public function getId() : int
	{
		return $this->_id;
	}
	
	/**
	 * Sets the code of the nomenclature.
	 * 
	 * @param string $code
	 * @return ApiFrInseeSireneNomenclatureApeInterface
	 */
	public function setCode(string $code) : ApiFrInseeSireneNomenclatureApeInterface
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * Gets the code of the nomenclature.
	 * 
	 * @return string
	 */
	public function getCode() : string
	{
		return $this->_code;
	}
	
	/**
	 * Sets the name of the nomenclature.
	 * 
	 * @param string $name
	 * @return ApiFrInseeSireneNomenclatureApeInterface
	 */
	public function setName(string $name) : ApiFrInseeSireneNomenclatureApeInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the name of the nomenclature.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
}
