<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;

/**
 * InseeSireneGenderParser class file.
 * 
 * This class parses insee sirene gender data into source objects.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiFrInseeSireneGender>
 */
class ApiFrInseeSireneGenderParser extends AbstractParser
{
	
	/**
	 * The genders.
	 * 
	 * @var array<string, ApiFrInseeSireneGender>
	 */
	protected array $_genders = [];
	
	/**
	 * Builds a new InseeSireneGenderParser with the given data.
	 * 
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct()
	{
		$reifier = new Reifier();
		$filePath = \dirname(__DIR__).'/data/gender.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		$iterator = $reifier->reifyIterator(ApiFrInseeSireneGender::class, $iterator);
		
		/** @var ApiFrInseeSireneGender $gender */
		foreach($iterator as $gender)
		{
			$this->_genders[(string) $gender->getCode()] = $gender;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiFrInseeSireneGender
	{
		$data = (string) $data;
		if(isset($this->_genders[$data]))
		{
			return $this->_genders[$data];
		}
		
		throw new ParseException(ApiFrInseeSireneGender::class, $data, 0);
	}
	
}
