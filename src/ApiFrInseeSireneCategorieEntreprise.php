<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

/**
 * ApiFrInseeSireneCategorieEntreprise class file.
 * 
 * This is a simple implementation of the
 * ApiFrInseeSireneCategorieEntrepriseInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeSireneCategorieEntreprise implements ApiFrInseeSireneCategorieEntrepriseInterface
{
	
	/**
	 * The id of the category.
	 * 
	 * @var int
	 */
	protected int $_id;
	
	/**
	 * The sigle of the category.
	 * 
	 * @var string
	 */
	protected string $_code;
	
	/**
	 * The name of the category.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Constructor for ApiFrInseeSireneCategorieEntreprise with private members.
	 * 
	 * @param int $id
	 * @param string $code
	 * @param string $name
	 */
	public function __construct(int $id, string $code, string $name)
	{
		$this->setId($id);
		$this->setCode($code);
		$this->setName($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the category.
	 * 
	 * @param int $id
	 * @return ApiFrInseeSireneCategorieEntrepriseInterface
	 */
	public function setId(int $id) : ApiFrInseeSireneCategorieEntrepriseInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the category.
	 * 
	 * @return int
	 */
	public function getId() : int
	{
		return $this->_id;
	}
	
	/**
	 * Sets the sigle of the category.
	 * 
	 * @param string $code
	 * @return ApiFrInseeSireneCategorieEntrepriseInterface
	 */
	public function setCode(string $code) : ApiFrInseeSireneCategorieEntrepriseInterface
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * Gets the sigle of the category.
	 * 
	 * @return string
	 */
	public function getCode() : string
	{
		return $this->_code;
	}
	
	/**
	 * Sets the name of the category.
	 * 
	 * @param string $name
	 * @return ApiFrInseeSireneCategorieEntrepriseInterface
	 */
	public function setName(string $name) : ApiFrInseeSireneCategorieEntrepriseInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the name of the category.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
}
