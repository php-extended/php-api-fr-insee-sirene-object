<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use DateTimeInterface;

/**
 * ApiFrInseeSireneEtablissementHistorique class file.
 * 
 * This is a simple implementation of the
 * ApiFrInseeSireneEtablissementHistoriqueInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassLength")
 * @SuppressWarnings("PHPMD.LongVariable")
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiFrInseeSireneEtablissementHistorique implements ApiFrInseeSireneEtablissementHistoriqueInterface
{
	
	/**
	 * Un numéro d’identité de l’unité légale est attribué par
	 * l’Insee à toutes les personnes physiques ou morales inscrites au
	 * répertoire ainsi qu’à leurs établissements : le numéro Siren. Ce
	 * numéro unique est « attribué soit à l’occasion des demandes
	 * d’immatriculation au registre du commerce et des sociétés ou des
	 * déclarations effectuées au répertoire des métiers, soit à la
	 * demande d’administrations » (article R123-224 du code de commerce).
	 * Lors de sa création, une unité légale se voit attribuer un numéro
	 * Siren de 9 chiffres.
	 * 
	 * *Règles de gestion*
	 * Les entrepreneurs individuels, ou personnes physiques, conservent le
	 * même numéro Siren jusqu’à leur décès. Les sociétés, ou
	 * personnes morales, perdent la personnalité juridique au moment de la
	 * cessation de l’activité de l’entreprise. Si l’activité devait
	 * reprendre ultérieurement, un nouveau numéro Siren sera attribué. Les
	 * numéros d’identification sont uniques : lorsqu’un numéro Siren a
	 * été attribué, il ne peut pas être réutilisé et attribué à une
	 * nouvelle unité légale, même lorsque l’activité a cessé.
	 * 
	 * *Historique*
	 * Même si la mise en place du répertoire Sirene remonte à 1973, toutes
	 * les unités légales, y compris celles créées avant cette date,
	 * disposent d’un numéro Siren pour le secteur privé non agricole.
	 * En 1983, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus aux institutions et services de
	 * l’État et aux collectivités territoriales.
	 * En 1993, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus au secteur privé agricole.
	 * 
	 * Longueur : 9
	 * 
	 * @var string
	 */
	protected string $_siren;
	
	/**
	 * Le numéro interne de classement permet de distinguer les
	 * établissements d'une même entreprise. Il est composé de 5 chiffres.
	 * Associé au Siren, il forme le Siret de l'établissement. Il est
	 * composé de quatre chiffres et d'un cinquième qui permet de contrôler
	 * la validité du numéro Siret.
	 * Le Nic est attribué une seule fois au sein de l'entreprise. Si
	 * l'établissement ferme, son Nic ne peut être réattribué à un nouvel
	 * établissement.
	 * 
	 * Longueur : 5
	 * 
	 * @var string
	 */
	protected string $_nic;
	
	/**
	 * Le numéro Siret est le numéro unique d’identification attribué à
	 * chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * Une entreprise est constituée d’autant d’établissements qu’il y
	 * a de lieux différents où elle exerce son activité. L’établissement
	 * est fermé quand l’activité cesse dans l’établissement concerné
	 * ou lorsque l’établissement change d’adresse.
	 * 
	 * Longueur : 14
	 * 
	 * @var string
	 */
	protected string $_siret;
	
	/**
	 * Cette variable désigne la date de fin de la période au cours de
	 * laquelle toutes les variables historisées de l'établissement restent
	 * inchangées.
	 * La date de fin est calculée et est égale à la veille de la date de
	 * début de la période suivante dans l'ordre chronologique.
	 * Si la date de fin de la période est null, la période correspond à la
	 * situation courante de l'établissement.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_dateFin = null;
	
	/**
	 * Date de début de la période au cours de laquelle toutes les variables
	 * historisées de l'établissement restent inchangées.
	 * La date 1900-01-01 signifie : date non déterminée. dateDebut
	 * peut-être vide uniquement pour les établissements des unités purgées
	 * (cf. variable unitePurgeeUniteLegale dans le descriptif des variables du
	 * fichier StockUniteLegale).
	 * La date de début de la période la plus ancienne ne correspond pas
	 * obligatoirement à la date de création de l'établissement, certaines
	 * variables historisées pouvant posséder des dates de début soit au
	 * 1900-01-01 soit antérieures à la date de création.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_dateDebut = null;
	
	/**
	 * Lors de son inscription au répertoire, un établissement est, sauf
	 * exception, à l’état « Actif ». Le passage à l’état « Fermé
	 * » découle de la prise en compte d’une déclaration de fermeture. À
	 * noter qu’un établissement fermé peut être rouvert.
	 * En règle générale, la première période d’historique d’un
	 * établissement correspond à un etatAdministratifUniteLegale égal à «
	 * Actif ». Toutefois, dans cette version, l'état administratif peut
	 * être à null (première date de début de l'état postérieure à la
	 * première date de début d'une autre variable historisée).
	 * 
	 * @var ?ApiFrInseeSireneEtatAdministratifInterface
	 */
	protected ?ApiFrInseeSireneEtatAdministratifInterface $_etatAdministratifEtablissement = null;
	
	/**
	 * C’est une variable booléenne qui indique si la variable
	 * EtatAdministratifEtablissement a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @var bool
	 */
	protected bool $_changementEtatAdministratifEtablissement;
	
	/**
	 * Les trois variables enseigne1Etablissement, enseigne2Etablissement et
	 * enseigne3Etablissement contiennent la ou les enseignes de
	 * l'établissement.
	 * L'enseigne identifie l'emplacement ou le local dans lequel est exercée
	 * l'activité. Un établissement peut posséder une enseigne, plusieurs
	 * enseignes ou aucune.
	 * L'analyse des enseignes et de son découpage en trois variables dans
	 * Sirene montre deux cas possibles : soit les 3 champs concernent 3
	 * enseignes bien distinctes, soit ces trois champs correspondent au
	 * découpage de l'enseigne qui est déclarée dans la liasse (sur un seul
	 * champ) avec une continuité des trois champs.
	 * 
	 * *Exemples*
	 * SIRET=53053581400178
	 * "enseigne1Etablissement": "LES SERRURIERS DES YVELINES LES VITRIERS DES
	 * YVELI",
	 * "enseigne2Etablissement": "NES LES CHAUFFAGISTES DES YVELINES LES
	 * PLATRIERS D",
	 * "enseigne3Etablissement": "ES YVELINES LES ELECTRICIENS DES
	 * YVELINES.…"
	 * 
	 * SIRET=05439181800033
	 * "enseigne1Etablissement": "HALTE OCCASIONS",
	 * "enseigne2Etablissement": "OUTRE-MER LOCATION",
	 * "enseigne3Etablissement": "OUTRE-MER TRANSIT".
	 * 
	 * Longueur : 50
	 * 
	 * @var ?string
	 */
	protected ?string $_enseigne1Etablissement = null;
	
	/**
	 * 2e enseigne.
	 * 
	 * @var ?string
	 */
	protected ?string $_enseigne2Etablissement = null;
	
	/**
	 * 3e enseigne.
	 * 
	 * @var ?string
	 */
	protected ?string $_enseigne3Etablissement = null;
	
	/**
	 * C’est une variable booléenne qui indique si une des variables
	 * Enseigne1Etablissement à Enseigne3Etablissement a été modifiée par
	 * rapport à la période précédente.
	 * 
	 * @var bool
	 */
	protected bool $_changementEnseigneEtablissement;
	
	/**
	 * Cette variable désigne le nom sous lequel l'établissement est connu du
	 * grand public. Cet élément d'identification de l'établissement a été
	 * enregistré au niveau établissement depuis l'application de la norme
	 * d'échanges CFE de 2008. Avant la norme 2008, la dénomination usuelle
	 * était enregistrée au niveau de l'unité légale sur trois champs (cf.
	 * variables denominationUsuelle1UniteLegale à
	 * denominationUsuelle3UniteLegale dans le descriptif des variables du
	 * fichier StockUniteLegale).
	 * 
	 * Longueur : 100
	 * 
	 * @var ?string
	 */
	protected ?string $_denominationUsuelleEtablissement = null;
	
	/**
	 * C’est une variable booléenne qui indique si la variable
	 * denominationUsuelleEtablissement a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @var bool
	 */
	protected bool $_changementDenominationUsuelleEtablissement;
	
	/**
	 * Lors de son inscription au répertoire, l’Insee attribue à tout
	 * établissement un code dit « APE » sur la base de la description de
	 * l’activité principale faite par le déclarant. Ce code est modifiable
	 * au cours de la vie de l’établissement en fonction des déclarations
	 * de l’exploitant.
	 * Pour chaque établissement, il existe à un instant donné un seul code
	 * « APE ». Il est attribué selon la nomenclature en vigueur. La
	 * nomenclature en vigueur est la Naf Rév2 et ce depuis le 1 er Janvier
	 * 2008.
	 * Chaque code comporte 2 chiffres, un point, 2 chiffres et une lettre.
	 * Tous les établissements actifs au 01/01/2008 ont eu leur code APE
	 * recodé dans la nouvelle nomenclature, ainsi de très nombreux
	 * établissements ont une période débutant à cette date.
	 * Au moment de la déclaration de l’entreprise, il peut arriver que
	 * l’Insee ne soit pas en mesure d’attribuer le bon code APE : la
	 * modalité 00.00Z peut alors être affectée provisoirement.
	 * 
	 * *Historique*
	 * Le code APE est historisé depuis le 01/01/2005.
	 * La règle d’historisation des données d’activité est la suivante :
	 *  - Pour les établissements fermés avant 31/12/2004, seul le dernier
	 * code activité connu figure, dans la nomenclature en vigueur à la date
	 * de fermeture.
	 *  - Pour les établissements ouverts après le 01/01/2005 et fermés
	 * avant le 31/12/2007, l’historique des codes attribués sur la période
	 * est disponible.
	 * - Pour les établissements ouverts après le 01/01/2005 et toujours
	 * ouverts le 01/01/2008, l’historique intègre le changement de
	 * nomenclature.
	 *  - Pour les établissements ouverts après le 01/01/2008, l’historique
	 * comprend les modifications apportées au cours de la vie de
	 * l’établissement.
	 * L'APE peut être à null (cas des unités purgées - pour des raisons de
	 * capacité de stockage des données, les données concernant les
	 * entreprises cessées avant le 31/12/2002 ont été purgées -, première
	 * date de début de l'APE postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * Longueur : 6
	 * 
	 * @var ?string
	 */
	protected ?string $_activitePrincipaleEtablissement = null;
	
	/**
	 * Cette variable indique la nomenclature d’activité correspondant à la
	 * variable activitePrincipaleEtablissement. (cf
	 * activitePrincipaleEtablissement).
	 * 
	 * @var ?ApiFrInseeSireneNomenclatureApeInterface
	 */
	protected ?ApiFrInseeSireneNomenclatureApeInterface $_nomenclatureActivitePrincipaleEtablissement = null;
	
	/**
	 * C’est une variable booléenne qui indique si la variable
	 * activitePrincipaleEtablissement a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @var bool
	 */
	protected bool $_changementActivitePrincipaleEtablissement;
	
	/**
	 * Lors de sa formalité d’ouverture, le déclarant indique si
	 * l’établissement aura ou non des employés. Par la suite, le
	 * déclarant peut également faire des déclarations de prise d’emploi
	 * et de fin d’emploi. La prise en compte d’une déclaration de prise
	 * d’emploi bascule immédiatement l’établissement en « employeur ».
	 * Inversement, lorsqu’une déclaration de fin d’emploi est traitée,
	 * l’établissement devient « non employeur ».
	 * Pour chaque établissement, il existe à un instant donné un seul code
	 * « employeur ». Cette variable est historisée pour les établissements
	 * qui étaient ouverts en 2005 et pour ceux ouverts ultérieurement.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_caractereEmployeurEtablissement = null;
	
	/**
	 * C’est une variable booléenne qui indique si la variable
	 * CaractereEmployeurEtablissement a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @var bool
	 */
	protected bool $_changementCaractereEmployeurEtablissement;
	
	/**
	 * Constructor for ApiFrInseeSireneEtablissementHistorique with private members.
	 * 
	 * @param string $siren
	 * @param string $nic
	 * @param string $siret
	 * @param bool $changementEtatAdministratifEtablissement
	 * @param bool $changementEnseigneEtablissement
	 * @param bool $changementDenominationUsuelleEtablissement
	 * @param bool $changementActivitePrincipaleEtablissement
	 * @param bool $changementCaractereEmployeurEtablissement
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(string $siren, string $nic, string $siret, bool $changementEtatAdministratifEtablissement, bool $changementEnseigneEtablissement, bool $changementDenominationUsuelleEtablissement, bool $changementActivitePrincipaleEtablissement, bool $changementCaractereEmployeurEtablissement)
	{
		$this->setSiren($siren);
		$this->setNic($nic);
		$this->setSiret($siret);
		$this->setChangementEtatAdministratifEtablissement($changementEtatAdministratifEtablissement);
		$this->setChangementEnseigneEtablissement($changementEnseigneEtablissement);
		$this->setChangementDenominationUsuelleEtablissement($changementDenominationUsuelleEtablissement);
		$this->setChangementActivitePrincipaleEtablissement($changementActivitePrincipaleEtablissement);
		$this->setChangementCaractereEmployeurEtablissement($changementCaractereEmployeurEtablissement);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets un numéro d’identité de l’unité légale est attribué par
	 * l’Insee à toutes les personnes physiques ou morales inscrites au
	 * répertoire ainsi qu’à leurs établissements : le numéro Siren. Ce
	 * numéro unique est « attribué soit à l’occasion des demandes
	 * d’immatriculation au registre du commerce et des sociétés ou des
	 * déclarations effectuées au répertoire des métiers, soit à la
	 * demande d’administrations » (article R123-224 du code de commerce).
	 * Lors de sa création, une unité légale se voit attribuer un numéro
	 * Siren de 9 chiffres.
	 * 
	 * *Règles de gestion*
	 * Les entrepreneurs individuels, ou personnes physiques, conservent le
	 * même numéro Siren jusqu’à leur décès. Les sociétés, ou
	 * personnes morales, perdent la personnalité juridique au moment de la
	 * cessation de l’activité de l’entreprise. Si l’activité devait
	 * reprendre ultérieurement, un nouveau numéro Siren sera attribué. Les
	 * numéros d’identification sont uniques : lorsqu’un numéro Siren a
	 * été attribué, il ne peut pas être réutilisé et attribué à une
	 * nouvelle unité légale, même lorsque l’activité a cessé.
	 * 
	 * *Historique*
	 * Même si la mise en place du répertoire Sirene remonte à 1973, toutes
	 * les unités légales, y compris celles créées avant cette date,
	 * disposent d’un numéro Siren pour le secteur privé non agricole.
	 * En 1983, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus aux institutions et services de
	 * l’État et aux collectivités territoriales.
	 * En 1993, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus au secteur privé agricole.
	 * 
	 * Longueur : 9
	 * 
	 * @param string $siren
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setSiren(string $siren) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_siren = $siren;
		
		return $this;
	}
	
	/**
	 * Gets un numéro d’identité de l’unité légale est attribué par
	 * l’Insee à toutes les personnes physiques ou morales inscrites au
	 * répertoire ainsi qu’à leurs établissements : le numéro Siren. Ce
	 * numéro unique est « attribué soit à l’occasion des demandes
	 * d’immatriculation au registre du commerce et des sociétés ou des
	 * déclarations effectuées au répertoire des métiers, soit à la
	 * demande d’administrations » (article R123-224 du code de commerce).
	 * Lors de sa création, une unité légale se voit attribuer un numéro
	 * Siren de 9 chiffres.
	 * 
	 * *Règles de gestion*
	 * Les entrepreneurs individuels, ou personnes physiques, conservent le
	 * même numéro Siren jusqu’à leur décès. Les sociétés, ou
	 * personnes morales, perdent la personnalité juridique au moment de la
	 * cessation de l’activité de l’entreprise. Si l’activité devait
	 * reprendre ultérieurement, un nouveau numéro Siren sera attribué. Les
	 * numéros d’identification sont uniques : lorsqu’un numéro Siren a
	 * été attribué, il ne peut pas être réutilisé et attribué à une
	 * nouvelle unité légale, même lorsque l’activité a cessé.
	 * 
	 * *Historique*
	 * Même si la mise en place du répertoire Sirene remonte à 1973, toutes
	 * les unités légales, y compris celles créées avant cette date,
	 * disposent d’un numéro Siren pour le secteur privé non agricole.
	 * En 1983, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus aux institutions et services de
	 * l’État et aux collectivités territoriales.
	 * En 1993, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus au secteur privé agricole.
	 * 
	 * Longueur : 9
	 * 
	 * @return string
	 */
	public function getSiren() : string
	{
		return $this->_siren;
	}
	
	/**
	 * Sets le numéro interne de classement permet de distinguer les
	 * établissements d'une même entreprise. Il est composé de 5 chiffres.
	 * Associé au Siren, il forme le Siret de l'établissement. Il est
	 * composé de quatre chiffres et d'un cinquième qui permet de contrôler
	 * la validité du numéro Siret.
	 * Le Nic est attribué une seule fois au sein de l'entreprise. Si
	 * l'établissement ferme, son Nic ne peut être réattribué à un nouvel
	 * établissement.
	 * 
	 * Longueur : 5
	 * 
	 * @param string $nic
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setNic(string $nic) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_nic = $nic;
		
		return $this;
	}
	
	/**
	 * Gets le numéro interne de classement permet de distinguer les
	 * établissements d'une même entreprise. Il est composé de 5 chiffres.
	 * Associé au Siren, il forme le Siret de l'établissement. Il est
	 * composé de quatre chiffres et d'un cinquième qui permet de contrôler
	 * la validité du numéro Siret.
	 * Le Nic est attribué une seule fois au sein de l'entreprise. Si
	 * l'établissement ferme, son Nic ne peut être réattribué à un nouvel
	 * établissement.
	 * 
	 * Longueur : 5
	 * 
	 * @return string
	 */
	public function getNic() : string
	{
		return $this->_nic;
	}
	
	/**
	 * Sets le numéro Siret est le numéro unique d’identification attribué
	 * à chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * Une entreprise est constituée d’autant d’établissements qu’il y
	 * a de lieux différents où elle exerce son activité. L’établissement
	 * est fermé quand l’activité cesse dans l’établissement concerné
	 * ou lorsque l’établissement change d’adresse.
	 * 
	 * Longueur : 14
	 * 
	 * @param string $siret
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setSiret(string $siret) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_siret = $siret;
		
		return $this;
	}
	
	/**
	 * Gets le numéro Siret est le numéro unique d’identification attribué
	 * à chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * Une entreprise est constituée d’autant d’établissements qu’il y
	 * a de lieux différents où elle exerce son activité. L’établissement
	 * est fermé quand l’activité cesse dans l’établissement concerné
	 * ou lorsque l’établissement change d’adresse.
	 * 
	 * Longueur : 14
	 * 
	 * @return string
	 */
	public function getSiret() : string
	{
		return $this->_siret;
	}
	
	/**
	 * Sets cette variable désigne la date de fin de la période au cours de
	 * laquelle toutes les variables historisées de l'établissement restent
	 * inchangées.
	 * La date de fin est calculée et est égale à la veille de la date de
	 * début de la période suivante dans l'ordre chronologique.
	 * Si la date de fin de la période est null, la période correspond à la
	 * situation courante de l'établissement.
	 * 
	 * @param ?DateTimeInterface $dateFin
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setDateFin(?DateTimeInterface $dateFin) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_dateFin = $dateFin;
		
		return $this;
	}
	
	/**
	 * Gets cette variable désigne la date de fin de la période au cours de
	 * laquelle toutes les variables historisées de l'établissement restent
	 * inchangées.
	 * La date de fin est calculée et est égale à la veille de la date de
	 * début de la période suivante dans l'ordre chronologique.
	 * Si la date de fin de la période est null, la période correspond à la
	 * situation courante de l'établissement.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateFin() : ?DateTimeInterface
	{
		return $this->_dateFin;
	}
	
	/**
	 * Sets date de début de la période au cours de laquelle toutes les
	 * variables historisées de l'établissement restent inchangées.
	 * La date 1900-01-01 signifie : date non déterminée. dateDebut
	 * peut-être vide uniquement pour les établissements des unités purgées
	 * (cf. variable unitePurgeeUniteLegale dans le descriptif des variables du
	 * fichier StockUniteLegale).
	 * La date de début de la période la plus ancienne ne correspond pas
	 * obligatoirement à la date de création de l'établissement, certaines
	 * variables historisées pouvant posséder des dates de début soit au
	 * 1900-01-01 soit antérieures à la date de création.
	 * 
	 * @param ?DateTimeInterface $dateDebut
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setDateDebut(?DateTimeInterface $dateDebut) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_dateDebut = $dateDebut;
		
		return $this;
	}
	
	/**
	 * Gets date de début de la période au cours de laquelle toutes les
	 * variables historisées de l'établissement restent inchangées.
	 * La date 1900-01-01 signifie : date non déterminée. dateDebut
	 * peut-être vide uniquement pour les établissements des unités purgées
	 * (cf. variable unitePurgeeUniteLegale dans le descriptif des variables du
	 * fichier StockUniteLegale).
	 * La date de début de la période la plus ancienne ne correspond pas
	 * obligatoirement à la date de création de l'établissement, certaines
	 * variables historisées pouvant posséder des dates de début soit au
	 * 1900-01-01 soit antérieures à la date de création.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateDebut() : ?DateTimeInterface
	{
		return $this->_dateDebut;
	}
	
	/**
	 * Sets lors de son inscription au répertoire, un établissement est, sauf
	 * exception, à l’état « Actif ». Le passage à l’état « Fermé
	 * » découle de la prise en compte d’une déclaration de fermeture. À
	 * noter qu’un établissement fermé peut être rouvert.
	 * En règle générale, la première période d’historique d’un
	 * établissement correspond à un etatAdministratifUniteLegale égal à «
	 * Actif ». Toutefois, dans cette version, l'état administratif peut
	 * être à null (première date de début de l'état postérieure à la
	 * première date de début d'une autre variable historisée).
	 * 
	 * @param ?ApiFrInseeSireneEtatAdministratifInterface $etatAdministratifEtablissement
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setEtatAdministratifEtablissement(?ApiFrInseeSireneEtatAdministratifInterface $etatAdministratifEtablissement) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_etatAdministratifEtablissement = $etatAdministratifEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets lors de son inscription au répertoire, un établissement est, sauf
	 * exception, à l’état « Actif ». Le passage à l’état « Fermé
	 * » découle de la prise en compte d’une déclaration de fermeture. À
	 * noter qu’un établissement fermé peut être rouvert.
	 * En règle générale, la première période d’historique d’un
	 * établissement correspond à un etatAdministratifUniteLegale égal à «
	 * Actif ». Toutefois, dans cette version, l'état administratif peut
	 * être à null (première date de début de l'état postérieure à la
	 * première date de début d'une autre variable historisée).
	 * 
	 * @return ?ApiFrInseeSireneEtatAdministratifInterface
	 */
	public function getEtatAdministratifEtablissement() : ?ApiFrInseeSireneEtatAdministratifInterface
	{
		return $this->_etatAdministratifEtablissement;
	}
	
	/**
	 * Sets c’est une variable booléenne qui indique si la variable
	 * EtatAdministratifEtablissement a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @param bool $changementEtatAdministratifEtablissement
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setChangementEtatAdministratifEtablissement(bool $changementEtatAdministratifEtablissement) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_changementEtatAdministratifEtablissement = $changementEtatAdministratifEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets c’est une variable booléenne qui indique si la variable
	 * EtatAdministratifEtablissement a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementEtatAdministratifEtablissement() : bool
	{
		return $this->_changementEtatAdministratifEtablissement;
	}
	
	/**
	 * Sets les trois variables enseigne1Etablissement, enseigne2Etablissement
	 * et enseigne3Etablissement contiennent la ou les enseignes de
	 * l'établissement.
	 * L'enseigne identifie l'emplacement ou le local dans lequel est exercée
	 * l'activité. Un établissement peut posséder une enseigne, plusieurs
	 * enseignes ou aucune.
	 * L'analyse des enseignes et de son découpage en trois variables dans
	 * Sirene montre deux cas possibles : soit les 3 champs concernent 3
	 * enseignes bien distinctes, soit ces trois champs correspondent au
	 * découpage de l'enseigne qui est déclarée dans la liasse (sur un seul
	 * champ) avec une continuité des trois champs.
	 * 
	 * *Exemples*
	 * SIRET=53053581400178
	 * "enseigne1Etablissement": "LES SERRURIERS DES YVELINES LES VITRIERS DES
	 * YVELI",
	 * "enseigne2Etablissement": "NES LES CHAUFFAGISTES DES YVELINES LES
	 * PLATRIERS D",
	 * "enseigne3Etablissement": "ES YVELINES LES ELECTRICIENS DES
	 * YVELINES.…"
	 * 
	 * SIRET=05439181800033
	 * "enseigne1Etablissement": "HALTE OCCASIONS",
	 * "enseigne2Etablissement": "OUTRE-MER LOCATION",
	 * "enseigne3Etablissement": "OUTRE-MER TRANSIT".
	 * 
	 * Longueur : 50
	 * 
	 * @param ?string $enseigne1Etablissement
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setEnseigne1Etablissement(?string $enseigne1Etablissement) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_enseigne1Etablissement = $enseigne1Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets les trois variables enseigne1Etablissement, enseigne2Etablissement
	 * et enseigne3Etablissement contiennent la ou les enseignes de
	 * l'établissement.
	 * L'enseigne identifie l'emplacement ou le local dans lequel est exercée
	 * l'activité. Un établissement peut posséder une enseigne, plusieurs
	 * enseignes ou aucune.
	 * L'analyse des enseignes et de son découpage en trois variables dans
	 * Sirene montre deux cas possibles : soit les 3 champs concernent 3
	 * enseignes bien distinctes, soit ces trois champs correspondent au
	 * découpage de l'enseigne qui est déclarée dans la liasse (sur un seul
	 * champ) avec une continuité des trois champs.
	 * 
	 * *Exemples*
	 * SIRET=53053581400178
	 * "enseigne1Etablissement": "LES SERRURIERS DES YVELINES LES VITRIERS DES
	 * YVELI",
	 * "enseigne2Etablissement": "NES LES CHAUFFAGISTES DES YVELINES LES
	 * PLATRIERS D",
	 * "enseigne3Etablissement": "ES YVELINES LES ELECTRICIENS DES
	 * YVELINES.…"
	 * 
	 * SIRET=05439181800033
	 * "enseigne1Etablissement": "HALTE OCCASIONS",
	 * "enseigne2Etablissement": "OUTRE-MER LOCATION",
	 * "enseigne3Etablissement": "OUTRE-MER TRANSIT".
	 * 
	 * Longueur : 50
	 * 
	 * @return ?string
	 */
	public function getEnseigne1Etablissement() : ?string
	{
		return $this->_enseigne1Etablissement;
	}
	
	/**
	 * Sets 2e enseigne.
	 * 
	 * @param ?string $enseigne2Etablissement
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setEnseigne2Etablissement(?string $enseigne2Etablissement) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_enseigne2Etablissement = $enseigne2Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets 2e enseigne.
	 * 
	 * @return ?string
	 */
	public function getEnseigne2Etablissement() : ?string
	{
		return $this->_enseigne2Etablissement;
	}
	
	/**
	 * Sets 3e enseigne.
	 * 
	 * @param ?string $enseigne3Etablissement
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setEnseigne3Etablissement(?string $enseigne3Etablissement) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_enseigne3Etablissement = $enseigne3Etablissement;
		
		return $this;
	}
	
	/**
	 * Gets 3e enseigne.
	 * 
	 * @return ?string
	 */
	public function getEnseigne3Etablissement() : ?string
	{
		return $this->_enseigne3Etablissement;
	}
	
	/**
	 * Sets c’est une variable booléenne qui indique si une des variables
	 * Enseigne1Etablissement à Enseigne3Etablissement a été modifiée par
	 * rapport à la période précédente.
	 * 
	 * @param bool $changementEnseigneEtablissement
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setChangementEnseigneEtablissement(bool $changementEnseigneEtablissement) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_changementEnseigneEtablissement = $changementEnseigneEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets c’est une variable booléenne qui indique si une des variables
	 * Enseigne1Etablissement à Enseigne3Etablissement a été modifiée par
	 * rapport à la période précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementEnseigneEtablissement() : bool
	{
		return $this->_changementEnseigneEtablissement;
	}
	
	/**
	 * Sets cette variable désigne le nom sous lequel l'établissement est
	 * connu du grand public. Cet élément d'identification de
	 * l'établissement a été enregistré au niveau établissement depuis
	 * l'application de la norme d'échanges CFE de 2008. Avant la norme 2008,
	 * la dénomination usuelle était enregistrée au niveau de l'unité
	 * légale sur trois champs (cf. variables denominationUsuelle1UniteLegale
	 * à denominationUsuelle3UniteLegale dans le descriptif des variables du
	 * fichier StockUniteLegale).
	 * 
	 * Longueur : 100
	 * 
	 * @param ?string $denominationUsuelleEtablissement
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setDenominationUsuelleEtablissement(?string $denominationUsuelleEtablissement) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_denominationUsuelleEtablissement = $denominationUsuelleEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable désigne le nom sous lequel l'établissement est
	 * connu du grand public. Cet élément d'identification de
	 * l'établissement a été enregistré au niveau établissement depuis
	 * l'application de la norme d'échanges CFE de 2008. Avant la norme 2008,
	 * la dénomination usuelle était enregistrée au niveau de l'unité
	 * légale sur trois champs (cf. variables denominationUsuelle1UniteLegale
	 * à denominationUsuelle3UniteLegale dans le descriptif des variables du
	 * fichier StockUniteLegale).
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getDenominationUsuelleEtablissement() : ?string
	{
		return $this->_denominationUsuelleEtablissement;
	}
	
	/**
	 * Sets c’est une variable booléenne qui indique si la variable
	 * denominationUsuelleEtablissement a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @param bool $changementDenominationUsuelleEtablissement
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setChangementDenominationUsuelleEtablissement(bool $changementDenominationUsuelleEtablissement) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_changementDenominationUsuelleEtablissement = $changementDenominationUsuelleEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets c’est une variable booléenne qui indique si la variable
	 * denominationUsuelleEtablissement a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementDenominationUsuelleEtablissement() : bool
	{
		return $this->_changementDenominationUsuelleEtablissement;
	}
	
	/**
	 * Sets lors de son inscription au répertoire, l’Insee attribue à tout
	 * établissement un code dit « APE » sur la base de la description de
	 * l’activité principale faite par le déclarant. Ce code est modifiable
	 * au cours de la vie de l’établissement en fonction des déclarations
	 * de l’exploitant.
	 * Pour chaque établissement, il existe à un instant donné un seul code
	 * « APE ». Il est attribué selon la nomenclature en vigueur. La
	 * nomenclature en vigueur est la Naf Rév2 et ce depuis le 1 er Janvier
	 * 2008.
	 * Chaque code comporte 2 chiffres, un point, 2 chiffres et une lettre.
	 * Tous les établissements actifs au 01/01/2008 ont eu leur code APE
	 * recodé dans la nouvelle nomenclature, ainsi de très nombreux
	 * établissements ont une période débutant à cette date.
	 * Au moment de la déclaration de l’entreprise, il peut arriver que
	 * l’Insee ne soit pas en mesure d’attribuer le bon code APE : la
	 * modalité 00.00Z peut alors être affectée provisoirement.
	 * 
	 * *Historique*
	 * Le code APE est historisé depuis le 01/01/2005.
	 * La règle d’historisation des données d’activité est la suivante :
	 *  - Pour les établissements fermés avant 31/12/2004, seul le dernier
	 * code activité connu figure, dans la nomenclature en vigueur à la date
	 * de fermeture.
	 *  - Pour les établissements ouverts après le 01/01/2005 et fermés
	 * avant le 31/12/2007, l’historique des codes attribués sur la période
	 * est disponible.
	 * - Pour les établissements ouverts après le 01/01/2005 et toujours
	 * ouverts le 01/01/2008, l’historique intègre le changement de
	 * nomenclature.
	 *  - Pour les établissements ouverts après le 01/01/2008, l’historique
	 * comprend les modifications apportées au cours de la vie de
	 * l’établissement.
	 * L'APE peut être à null (cas des unités purgées - pour des raisons de
	 * capacité de stockage des données, les données concernant les
	 * entreprises cessées avant le 31/12/2002 ont été purgées -, première
	 * date de début de l'APE postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * Longueur : 6
	 * 
	 * @param ?string $activitePrincipaleEtablissement
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setActivitePrincipaleEtablissement(?string $activitePrincipaleEtablissement) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_activitePrincipaleEtablissement = $activitePrincipaleEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets lors de son inscription au répertoire, l’Insee attribue à tout
	 * établissement un code dit « APE » sur la base de la description de
	 * l’activité principale faite par le déclarant. Ce code est modifiable
	 * au cours de la vie de l’établissement en fonction des déclarations
	 * de l’exploitant.
	 * Pour chaque établissement, il existe à un instant donné un seul code
	 * « APE ». Il est attribué selon la nomenclature en vigueur. La
	 * nomenclature en vigueur est la Naf Rév2 et ce depuis le 1 er Janvier
	 * 2008.
	 * Chaque code comporte 2 chiffres, un point, 2 chiffres et une lettre.
	 * Tous les établissements actifs au 01/01/2008 ont eu leur code APE
	 * recodé dans la nouvelle nomenclature, ainsi de très nombreux
	 * établissements ont une période débutant à cette date.
	 * Au moment de la déclaration de l’entreprise, il peut arriver que
	 * l’Insee ne soit pas en mesure d’attribuer le bon code APE : la
	 * modalité 00.00Z peut alors être affectée provisoirement.
	 * 
	 * *Historique*
	 * Le code APE est historisé depuis le 01/01/2005.
	 * La règle d’historisation des données d’activité est la suivante :
	 *  - Pour les établissements fermés avant 31/12/2004, seul le dernier
	 * code activité connu figure, dans la nomenclature en vigueur à la date
	 * de fermeture.
	 *  - Pour les établissements ouverts après le 01/01/2005 et fermés
	 * avant le 31/12/2007, l’historique des codes attribués sur la période
	 * est disponible.
	 * - Pour les établissements ouverts après le 01/01/2005 et toujours
	 * ouverts le 01/01/2008, l’historique intègre le changement de
	 * nomenclature.
	 *  - Pour les établissements ouverts après le 01/01/2008, l’historique
	 * comprend les modifications apportées au cours de la vie de
	 * l’établissement.
	 * L'APE peut être à null (cas des unités purgées - pour des raisons de
	 * capacité de stockage des données, les données concernant les
	 * entreprises cessées avant le 31/12/2002 ont été purgées -, première
	 * date de début de l'APE postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * Longueur : 6
	 * 
	 * @return ?string
	 */
	public function getActivitePrincipaleEtablissement() : ?string
	{
		return $this->_activitePrincipaleEtablissement;
	}
	
	/**
	 * Sets cette variable indique la nomenclature d’activité correspondant
	 * à la variable activitePrincipaleEtablissement. (cf
	 * activitePrincipaleEtablissement).
	 * 
	 * @param ?ApiFrInseeSireneNomenclatureApeInterface $nomenclatureActivitePrincipaleEtablissement
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setNomenclatureActivitePrincipaleEtablissement(?ApiFrInseeSireneNomenclatureApeInterface $nomenclatureActivitePrincipaleEtablissement) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_nomenclatureActivitePrincipaleEtablissement = $nomenclatureActivitePrincipaleEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique la nomenclature d’activité correspondant
	 * à la variable activitePrincipaleEtablissement. (cf
	 * activitePrincipaleEtablissement).
	 * 
	 * @return ?ApiFrInseeSireneNomenclatureApeInterface
	 */
	public function getNomenclatureActivitePrincipaleEtablissement() : ?ApiFrInseeSireneNomenclatureApeInterface
	{
		return $this->_nomenclatureActivitePrincipaleEtablissement;
	}
	
	/**
	 * Sets c’est une variable booléenne qui indique si la variable
	 * activitePrincipaleEtablissement a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @param bool $changementActivitePrincipaleEtablissement
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setChangementActivitePrincipaleEtablissement(bool $changementActivitePrincipaleEtablissement) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_changementActivitePrincipaleEtablissement = $changementActivitePrincipaleEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets c’est une variable booléenne qui indique si la variable
	 * activitePrincipaleEtablissement a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementActivitePrincipaleEtablissement() : bool
	{
		return $this->_changementActivitePrincipaleEtablissement;
	}
	
	/**
	 * Sets lors de sa formalité d’ouverture, le déclarant indique si
	 * l’établissement aura ou non des employés. Par la suite, le
	 * déclarant peut également faire des déclarations de prise d’emploi
	 * et de fin d’emploi. La prise en compte d’une déclaration de prise
	 * d’emploi bascule immédiatement l’établissement en « employeur ».
	 * Inversement, lorsqu’une déclaration de fin d’emploi est traitée,
	 * l’établissement devient « non employeur ».
	 * Pour chaque établissement, il existe à un instant donné un seul code
	 * « employeur ». Cette variable est historisée pour les établissements
	 * qui étaient ouverts en 2005 et pour ceux ouverts ultérieurement.
	 * 
	 * @param ?bool $caractereEmployeurEtablissement
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setCaractereEmployeurEtablissement(?bool $caractereEmployeurEtablissement) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_caractereEmployeurEtablissement = $caractereEmployeurEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets lors de sa formalité d’ouverture, le déclarant indique si
	 * l’établissement aura ou non des employés. Par la suite, le
	 * déclarant peut également faire des déclarations de prise d’emploi
	 * et de fin d’emploi. La prise en compte d’une déclaration de prise
	 * d’emploi bascule immédiatement l’établissement en « employeur ».
	 * Inversement, lorsqu’une déclaration de fin d’emploi est traitée,
	 * l’établissement devient « non employeur ».
	 * Pour chaque établissement, il existe à un instant donné un seul code
	 * « employeur ». Cette variable est historisée pour les établissements
	 * qui étaient ouverts en 2005 et pour ceux ouverts ultérieurement.
	 * 
	 * @return ?bool
	 */
	public function hasCaractereEmployeurEtablissement() : ?bool
	{
		return $this->_caractereEmployeurEtablissement;
	}
	
	/**
	 * Sets c’est une variable booléenne qui indique si la variable
	 * CaractereEmployeurEtablissement a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @param bool $changementCaractereEmployeurEtablissement
	 * @return ApiFrInseeSireneEtablissementHistoriqueInterface
	 */
	public function setChangementCaractereEmployeurEtablissement(bool $changementCaractereEmployeurEtablissement) : ApiFrInseeSireneEtablissementHistoriqueInterface
	{
		$this->_changementCaractereEmployeurEtablissement = $changementCaractereEmployeurEtablissement;
		
		return $this;
	}
	
	/**
	 * Gets c’est une variable booléenne qui indique si la variable
	 * CaractereEmployeurEtablissement a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementCaractereEmployeurEtablissement() : bool
	{
		return $this->_changementCaractereEmployeurEtablissement;
	}
	
}
