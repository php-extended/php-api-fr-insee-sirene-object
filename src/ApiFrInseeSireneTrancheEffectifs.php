<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

/**
 * ApiFrInseeSireneTrancheEffectifs class file.
 * 
 * This is a simple implementation of the
 * ApiFrInseeSireneTrancheEffectifsInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeSireneTrancheEffectifs implements ApiFrInseeSireneTrancheEffectifsInterface
{
	
	/**
	 * The identifier of the tranche.
	 * 
	 * @var int
	 */
	protected int $_id;
	
	/**
	 * The code of the tranche.
	 * 
	 * @var string
	 */
	protected string $_code;
	
	/**
	 * The name of the tranche.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The effectif min of the tranche.
	 * 
	 * @var int
	 */
	protected int $_effectifMin;
	
	/**
	 * The effectif max of the tranche.
	 * 
	 * @var int
	 */
	protected int $_effectifMax;
	
	/**
	 * Constructor for ApiFrInseeSireneTrancheEffectifs with private members.
	 * 
	 * @param int $id
	 * @param string $code
	 * @param string $name
	 * @param int $effectifMin
	 * @param int $effectifMax
	 */
	public function __construct(int $id, string $code, string $name, int $effectifMin, int $effectifMax)
	{
		$this->setId($id);
		$this->setCode($code);
		$this->setName($name);
		$this->setEffectifMin($effectifMin);
		$this->setEffectifMax($effectifMax);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the identifier of the tranche.
	 * 
	 * @param int $id
	 * @return ApiFrInseeSireneTrancheEffectifsInterface
	 */
	public function setId(int $id) : ApiFrInseeSireneTrancheEffectifsInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the identifier of the tranche.
	 * 
	 * @return int
	 */
	public function getId() : int
	{
		return $this->_id;
	}
	
	/**
	 * Sets the code of the tranche.
	 * 
	 * @param string $code
	 * @return ApiFrInseeSireneTrancheEffectifsInterface
	 */
	public function setCode(string $code) : ApiFrInseeSireneTrancheEffectifsInterface
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * Gets the code of the tranche.
	 * 
	 * @return string
	 */
	public function getCode() : string
	{
		return $this->_code;
	}
	
	/**
	 * Sets the name of the tranche.
	 * 
	 * @param string $name
	 * @return ApiFrInseeSireneTrancheEffectifsInterface
	 */
	public function setName(string $name) : ApiFrInseeSireneTrancheEffectifsInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the name of the tranche.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * Sets the effectif min of the tranche.
	 * 
	 * @param int $effectifMin
	 * @return ApiFrInseeSireneTrancheEffectifsInterface
	 */
	public function setEffectifMin(int $effectifMin) : ApiFrInseeSireneTrancheEffectifsInterface
	{
		$this->_effectifMin = $effectifMin;
		
		return $this;
	}
	
	/**
	 * Gets the effectif min of the tranche.
	 * 
	 * @return int
	 */
	public function getEffectifMin() : int
	{
		return $this->_effectifMin;
	}
	
	/**
	 * Sets the effectif max of the tranche.
	 * 
	 * @param int $effectifMax
	 * @return ApiFrInseeSireneTrancheEffectifsInterface
	 */
	public function setEffectifMax(int $effectifMax) : ApiFrInseeSireneTrancheEffectifsInterface
	{
		$this->_effectifMax = $effectifMax;
		
		return $this;
	}
	
	/**
	 * Gets the effectif max of the tranche.
	 * 
	 * @return int
	 */
	public function getEffectifMax() : int
	{
		return $this->_effectifMax;
	}
	
}
