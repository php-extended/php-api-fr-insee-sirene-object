<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;

/**
 * InseeSireneNomenclatureApeParser class file.
 * 
 * This class parses insee sirene nomenclature ape data into source objects.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiFrInseeSireneNomenclatureApe>
 */
class ApiFrInseeSireneNomenclatureApeParser extends AbstractParser
{
	
	/**
	 * The nomenclatures.
	 * 
	 * @var array<string, ApiFrInseeSireneNomenclatureApe>
	 */
	protected array $_nomenclaturesApe = [];
	
	/**
	 * Builds a new InseeSireneNomenclatureApeParser with the given data.
	 * 
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct()
	{
		$reifier = new Reifier();
		$filePath = \dirname(__DIR__).'/data/nomenclature_ape.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		$iterator = $reifier->reifyIterator(ApiFrInseeSireneNomenclatureApe::class, $iterator);
		
		/** @var ApiFrInseeSireneNomenclatureApe $nomenclatureApe */
		foreach($iterator as $nomenclatureApe)
		{
			$this->_nomenclaturesApe[(string) $nomenclatureApe->getCode()] = $nomenclatureApe;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiFrInseeSireneNomenclatureApe
	{
		$data = (string) $data;
		if(isset($this->_nomenclaturesApe[$data]))
		{
			return $this->_nomenclaturesApe[$data];
		}
		
		throw new ParseException(ApiFrInseeSireneNomenclatureApe::class, $data, 0);
	}
	
}
