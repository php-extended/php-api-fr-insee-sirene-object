<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use DateTimeInterface;

/**
 * ApiFrInseeSireneSuccession class file.
 * 
 * This is a simple implementation of the ApiFrInseeSireneSuccessionInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeSireneSuccession implements ApiFrInseeSireneSuccessionInterface
{
	
	/**
	 * Cette variable désigne le numéro Siret de l’établissement
	 * prédécesseur.
	 * Le numéro Siret est le numéro unique d’identification attribué à
	 * chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * 
	 * Longueur : 14
	 * 
	 * @var string
	 */
	protected string $_siretEtablissementPredecesseur;
	
	/**
	 * Cette variable désigne le numéro Siret de l’établissement
	 * successeur.
	 * Le numéro Siret est le numéro unique d’identification attribué à
	 * chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * 
	 * Longueur : 14
	 * 
	 * @var string
	 */
	protected string $_siretEtablissementSuccesseur;
	
	/**
	 * Cette variable indique la date à laquelle la succession a lieu.
	 * 
	 * Longueur : 10
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_dateLienSuccession;
	
	/**
	 * C’est une variable booléenne qui indique s’il s’agit d’un
	 * transfert de siège.
	 * 
	 * Il peut s’agir d’un transfert de l’établissement ayant la
	 * qualité de siège, ou d’un transfert de la qualité de siège d’un
	 * établissement à un autre.
	 * La variable prend la valeur true si le lien de succession concerne
	 * l’établissement siège, false si seulement des établissements
	 * secondaires sont concernés.
	 * 
	 * Longueur : 5
	 * 
	 * @var bool
	 */
	protected bool $_transfertSiege;
	
	/**
	 * C’est une variable booléenne qui indique s’il y a continuité
	 * économique entre les deux établissements ou non :
	 *  - true s’il y a continuité économique,
	 *  - false s’il n’y a pas continuité économique,.
	 * 
	 * Notion de continuité économique :
	 * Il y a continuité économique entre deux établissements qui se
	 * succèdent dès lors que deux des trois critères suivants sont
	 * vérifiés :
	 *  - les deux établissements appartiennent à la même unité légale
	 * (même Siren) ;
	 *  - les deux établissements exercent la même activité (même code APE)
	 * ;
	 *  - les deux établissements sont situés dans un même lieu (numéro et
	 * libellé de voie, code commune).
	 * À noter : en cas de transfert de siège, la variable
	 * continuiteEconomique est toujours à true.
	 * 
	 * Longueur : 5
	 * 
	 * @var bool
	 */
	protected bool $_continuiteEconomique;
	
	/**
	 * Cette variable indique la date à laquelle le lien de succession a été
	 * enregistré dans le répertoire Sirene.
	 * 
	 * Longueur : 19
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_dateDernierTraitementLienSuccession;
	
	/**
	 * Constructor for ApiFrInseeSireneSuccession with private members.
	 * 
	 * @param string $siretEtablissementPredecesseur
	 * @param string $siretEtablissementSuccesseur
	 * @param DateTimeInterface $dateLienSuccession
	 * @param bool $transfertSiege
	 * @param bool $continuiteEconomique
	 * @param DateTimeInterface $dateDernierTraitementLienSuccession
	 */
	public function __construct(string $siretEtablissementPredecesseur, string $siretEtablissementSuccesseur, DateTimeInterface $dateLienSuccession, bool $transfertSiege, bool $continuiteEconomique, DateTimeInterface $dateDernierTraitementLienSuccession)
	{
		$this->setSiretEtablissementPredecesseur($siretEtablissementPredecesseur);
		$this->setSiretEtablissementSuccesseur($siretEtablissementSuccesseur);
		$this->setDateLienSuccession($dateLienSuccession);
		$this->setTransfertSiege($transfertSiege);
		$this->setContinuiteEconomique($continuiteEconomique);
		$this->setDateDernierTraitementLienSuccession($dateDernierTraitementLienSuccession);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets cette variable désigne le numéro Siret de l’établissement
	 * prédécesseur.
	 * Le numéro Siret est le numéro unique d’identification attribué à
	 * chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * 
	 * Longueur : 14
	 * 
	 * @param string $siretEtablissementPredecesseur
	 * @return ApiFrInseeSireneSuccessionInterface
	 */
	public function setSiretEtablissementPredecesseur(string $siretEtablissementPredecesseur) : ApiFrInseeSireneSuccessionInterface
	{
		$this->_siretEtablissementPredecesseur = $siretEtablissementPredecesseur;
		
		return $this;
	}
	
	/**
	 * Gets cette variable désigne le numéro Siret de l’établissement
	 * prédécesseur.
	 * Le numéro Siret est le numéro unique d’identification attribué à
	 * chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * 
	 * Longueur : 14
	 * 
	 * @return string
	 */
	public function getSiretEtablissementPredecesseur() : string
	{
		return $this->_siretEtablissementPredecesseur;
	}
	
	/**
	 * Sets cette variable désigne le numéro Siret de l’établissement
	 * successeur.
	 * Le numéro Siret est le numéro unique d’identification attribué à
	 * chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * 
	 * Longueur : 14
	 * 
	 * @param string $siretEtablissementSuccesseur
	 * @return ApiFrInseeSireneSuccessionInterface
	 */
	public function setSiretEtablissementSuccesseur(string $siretEtablissementSuccesseur) : ApiFrInseeSireneSuccessionInterface
	{
		$this->_siretEtablissementSuccesseur = $siretEtablissementSuccesseur;
		
		return $this;
	}
	
	/**
	 * Gets cette variable désigne le numéro Siret de l’établissement
	 * successeur.
	 * Le numéro Siret est le numéro unique d’identification attribué à
	 * chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * 
	 * Longueur : 14
	 * 
	 * @return string
	 */
	public function getSiretEtablissementSuccesseur() : string
	{
		return $this->_siretEtablissementSuccesseur;
	}
	
	/**
	 * Sets cette variable indique la date à laquelle la succession a lieu.
	 * 
	 * Longueur : 10
	 * 
	 * @param DateTimeInterface $dateLienSuccession
	 * @return ApiFrInseeSireneSuccessionInterface
	 */
	public function setDateLienSuccession(DateTimeInterface $dateLienSuccession) : ApiFrInseeSireneSuccessionInterface
	{
		$this->_dateLienSuccession = $dateLienSuccession;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique la date à laquelle la succession a lieu.
	 * 
	 * Longueur : 10
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateLienSuccession() : DateTimeInterface
	{
		return $this->_dateLienSuccession;
	}
	
	/**
	 * Sets c’est une variable booléenne qui indique s’il s’agit d’un
	 * transfert de siège.
	 * 
	 * Il peut s’agir d’un transfert de l’établissement ayant la
	 * qualité de siège, ou d’un transfert de la qualité de siège d’un
	 * établissement à un autre.
	 * La variable prend la valeur true si le lien de succession concerne
	 * l’établissement siège, false si seulement des établissements
	 * secondaires sont concernés.
	 * 
	 * Longueur : 5
	 * 
	 * @param bool $transfertSiege
	 * @return ApiFrInseeSireneSuccessionInterface
	 */
	public function setTransfertSiege(bool $transfertSiege) : ApiFrInseeSireneSuccessionInterface
	{
		$this->_transfertSiege = $transfertSiege;
		
		return $this;
	}
	
	/**
	 * Gets c’est une variable booléenne qui indique s’il s’agit d’un
	 * transfert de siège.
	 * 
	 * Il peut s’agir d’un transfert de l’établissement ayant la
	 * qualité de siège, ou d’un transfert de la qualité de siège d’un
	 * établissement à un autre.
	 * La variable prend la valeur true si le lien de succession concerne
	 * l’établissement siège, false si seulement des établissements
	 * secondaires sont concernés.
	 * 
	 * Longueur : 5
	 * 
	 * @return bool
	 */
	public function hasTransfertSiege() : bool
	{
		return $this->_transfertSiege;
	}
	
	/**
	 * Sets c’est une variable booléenne qui indique s’il y a continuité
	 * économique entre les deux établissements ou non :
	 *  - true s’il y a continuité économique,
	 *  - false s’il n’y a pas continuité économique,.
	 * 
	 * Notion de continuité économique :
	 * Il y a continuité économique entre deux établissements qui se
	 * succèdent dès lors que deux des trois critères suivants sont
	 * vérifiés :
	 *  - les deux établissements appartiennent à la même unité légale
	 * (même Siren) ;
	 *  - les deux établissements exercent la même activité (même code APE)
	 * ;
	 *  - les deux établissements sont situés dans un même lieu (numéro et
	 * libellé de voie, code commune).
	 * À noter : en cas de transfert de siège, la variable
	 * continuiteEconomique est toujours à true.
	 * 
	 * Longueur : 5
	 * 
	 * @param bool $continuiteEconomique
	 * @return ApiFrInseeSireneSuccessionInterface
	 */
	public function setContinuiteEconomique(bool $continuiteEconomique) : ApiFrInseeSireneSuccessionInterface
	{
		$this->_continuiteEconomique = $continuiteEconomique;
		
		return $this;
	}
	
	/**
	 * Gets c’est une variable booléenne qui indique s’il y a continuité
	 * économique entre les deux établissements ou non :
	 *  - true s’il y a continuité économique,
	 *  - false s’il n’y a pas continuité économique,.
	 * 
	 * Notion de continuité économique :
	 * Il y a continuité économique entre deux établissements qui se
	 * succèdent dès lors que deux des trois critères suivants sont
	 * vérifiés :
	 *  - les deux établissements appartiennent à la même unité légale
	 * (même Siren) ;
	 *  - les deux établissements exercent la même activité (même code APE)
	 * ;
	 *  - les deux établissements sont situés dans un même lieu (numéro et
	 * libellé de voie, code commune).
	 * À noter : en cas de transfert de siège, la variable
	 * continuiteEconomique est toujours à true.
	 * 
	 * Longueur : 5
	 * 
	 * @return bool
	 */
	public function hasContinuiteEconomique() : bool
	{
		return $this->_continuiteEconomique;
	}
	
	/**
	 * Sets cette variable indique la date à laquelle le lien de succession a
	 * été enregistré dans le répertoire Sirene.
	 * 
	 * Longueur : 19
	 * 
	 * @param DateTimeInterface $dateDernierTraitementLienSuccession
	 * @return ApiFrInseeSireneSuccessionInterface
	 */
	public function setDateDernierTraitementLienSuccession(DateTimeInterface $dateDernierTraitementLienSuccession) : ApiFrInseeSireneSuccessionInterface
	{
		$this->_dateDernierTraitementLienSuccession = $dateDernierTraitementLienSuccession;
		
		return $this;
	}
	
	/**
	 * Gets cette variable indique la date à laquelle le lien de succession a
	 * été enregistré dans le répertoire Sirene.
	 * 
	 * Longueur : 19
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateDernierTraitementLienSuccession() : DateTimeInterface
	{
		return $this->_dateDernierTraitementLienSuccession;
	}
	
}
